/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SchedulePreference {

    private int order;
    private LocalDateTime dateHourBegin;

    public SchedulePreference(int order, LocalDateTime dateHourBegin) {
        this.order = order;
        this.dateHourBegin = dateHourBegin;
    }

    public int getOrder() {
        return this.order;
    }

    public LocalDateTime getDateHourBegin() {
        return dateHourBegin;
    }

    public String getDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return this.dateHourBegin.format(formatter);
    }

    public String getTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        return this.dateHourBegin.format(formatter);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SchedulePreference other = (SchedulePreference) obj;
        if (!Objects.equals(this.dateHourBegin, other.dateHourBegin)) {
            return false;
        }
        return true;
    }

}
