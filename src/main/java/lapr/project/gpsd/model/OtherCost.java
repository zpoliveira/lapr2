/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class OtherCost {
    
    private String designation;
    private double value;

    public OtherCost(String designation, double value) {
        this.designation = designation;
        this.value = value;
    }

    public String getDesignation() {
        return this.designation;
    }

    public double getValue() {
        return this.value;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void setValue(int value) {
        this.value = value;
    }
    
    
}
