/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class Application {
    
    
    private String SPName;   
    private String SPVATNumber;   
    private String SPPhoneNumber;   
    private String SPEmail;   
    private String basePostalCode;
    private List<PostalAddress> postalAddressList;
    private List<AcademicQualification> academicQualificationList;
    private List<ProfessionalQualification> professionalQualificationList;
    private List<Document> documentList;
    private List<Category> categoryList;

    public Application(String SPName, String SPVATNumber, String SPPhoneNumber, String SPEmail, PostalAddress postalA) {
        this.SPName = SPName;
        this.SPVATNumber = SPVATNumber;
        this.SPPhoneNumber = SPPhoneNumber;
        this.SPEmail = SPEmail;
        this.basePostalCode = postalA.getPostalCode();
        this.postalAddressList = new ArrayList<PostalAddress>();
        this.academicQualificationList = new ArrayList<AcademicQualification>();
        this.professionalQualificationList = new ArrayList<ProfessionalQualification>();
        this.documentList = new ArrayList<Document>();
        this.categoryList = new ArrayList<Category>();
        this.postalAddressList.add(postalA);
    }

    public String getSPName() {
        return SPName;
    }

    public String getSPVATNumber() {
        return SPVATNumber;
    }

    public String getSPPhoneNumber() {
        return SPPhoneNumber;
    }

    public String getSPEmail() {
        return SPEmail;
    }

    public String getBasePostalCode() {
        return basePostalCode;
    }

    public List<PostalAddress> getPostalAddressList() {
        return postalAddressList;
    }

    public List<AcademicQualification> getAcademicQualificationList() {
        return academicQualificationList;
    }

    public List<ProfessionalQualification> getProfessionalQualificationList() {
        return professionalQualificationList;
    } 

    public List<Document> getDocumentList() {
        return documentList;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setSPName(String SPName) {
        this.SPName = SPName;
    }

    public void setSPVATNumber(String SPVATNumber) {
        this.SPVATNumber = SPVATNumber;
    }

    public void setSPPhoneNumber(String SPPhoneNumber) {
        this.SPPhoneNumber = SPPhoneNumber;
    }

    public void setSPEmail(String SPEmail) {
        this.SPEmail = SPEmail;
    }

    public void setPostalAddressList(List<PostalAddress> postalAddressList) {
        this.postalAddressList = postalAddressList;
    }

    public void setAcademicQualificationList(List<AcademicQualification> academicQualificationList) {
        this.academicQualificationList = academicQualificationList;
    }

    public void setProfessionalQualificationList(List<ProfessionalQualification> professionalQualificationList) {
        this.professionalQualificationList = professionalQualificationList;
    }

    public void setDocumentList(List<Document> documentList) {
        this.documentList = documentList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.SPVATNumber);
        hash = 29 * hash + Objects.hashCode(this.SPEmail);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Application other = (Application) obj;
        if (!Objects.equals(this.SPVATNumber, other.SPVATNumber)) {
            return false;
        }
        if (!Objects.equals(this.SPEmail, other.SPEmail)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Application: Name: " + SPName + "\n" +
               "VAT Number: " + SPVATNumber + "\n" +
               "Phone Number: " + SPPhoneNumber + "\n" +
               "E-mail: " + SPEmail + "\n" +
               "Postal address: " + this.postalAddressList.toString() + "\n" +
               "Academic qualification: " + this.academicQualificationList.toString() + "\n" +
               "Professional qualification: " + this.professionalQualificationList.toString() + "\n" +
               "Categories: " + this.categoryList.toString();
    }
    
    public static PostalAddress newAdress (String strLocal, String strCodPostal, String strLocalidade){
        
     return new PostalAddress(strLocal, strCodPostal, strLocalidade);
     
    }
    
    public void addPostalAddress(PostalAddress postalA){
        
        if(!this.postalAddressList.contains(postalA)){
            this.postalAddressList.add(postalA);
        }
        
    }
    
    public void addAcademicQualification(String designation, String degree, String classification){
        
        AcademicQualification academicQualif = new AcademicQualification(designation, degree, classification);
        
        if(!this.academicQualificationList.contains(academicQualif)){
            this.academicQualificationList.add(academicQualif);
        }
    }
    
    public void addProfessionalQualification(String designation){
        
        ProfessionalQualification professionalQualif = new ProfessionalQualification(designation);
        
        if(!this.professionalQualificationList.contains(professionalQualif)){
            this.professionalQualificationList.add(professionalQualif);
        }
    }
    
    public void addSuportDocument (String document){
        
        Document doc = new Document(document);
        
        if(!this.documentList.contains(doc)){
            this.documentList.add(doc);
        }
        
    }
    
    public void addCategory(Category cat){
        
        if(!this.categoryList.contains(cat)){
            this.categoryList.add(cat);
        }
        
    }
    
}
