/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ServiceProvider {
    
    private int mechanographicNumber;
    private String completName;
    private String shortName;
    private String email;
    private List<GeographicalArea> lstGeoAreas;
    private List<Category> lstCategory;
    private List<Disponibility> lstDisponibility;
    private String postalCode;
    private List<Rating> ratings;
    private String vatNumber;
    private String label;
    
    public ServiceProvider(int mechanographicNumber, String completName, String shortName, String email, String vatNumber, String postalCode) throws IllegalArgumentException {
        if(mechanographicNumber == 0 || completName == null || completName.isEmpty() || shortName == null 
                || shortName.isEmpty() || email == null || email.isEmpty() || vatNumber == null || 
                vatNumber.isEmpty() || postalCode == null || postalCode.isEmpty())
        {
            throw new IllegalArgumentException("No argument can be null or empty.");
        }
        this.mechanographicNumber = mechanographicNumber;
        this.completName = completName;
        this.shortName = shortName;
        this.email = email;
        this.vatNumber = vatNumber;
        this.lstGeoAreas = new ArrayList<>();
        this.lstCategory = new ArrayList<>();
        this.ratings = new ArrayList<>();
        this.lstDisponibility = new ArrayList<>();

        this.label = null;
        this.postalCode = postalCode;

    }

    public String getCompletName() {
        return this.completName;
    }

    public String getShortName() {
        return shortName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public int getMechanographicNumber() {
        return this.mechanographicNumber;
    }

    public String getEmail() {
        return this.email;
    }

    public String getVatNumber() {
        return this.vatNumber;
    }
    
    public List<Rating> getRatings() {
        
        return this.ratings;
    }

    public boolean addRating(Rating r) {
        if (r != null) {
            return this.ratings.add(r);
        }
        return false;
    }
    
    public void setLabel(String label){
        this.label = label;
    }
    
    public String getLabel(){
        return this.label;
    }
    
    public boolean addGeographicalArea(GeographicalArea geoArea){
        boolean added = false;
         if(geoArea == null)
             return added;
        if(!this.hasGeoArea(geoArea)){
            this.lstGeoAreas.add(geoArea);
            added = true;
        }
        return added;
    }
    
    public boolean addCategory(Category cat){
          boolean added = false;
        if(cat == null)
             return added;
      
        if(!this.hasCategory(cat)){
            this.lstCategory.add(cat);
            added = true;
        }
        return added;
    }
    
    public boolean addDisponibility(Disponibility disp){
        boolean added = false;
        if(disp == null){
            return added;
        }
        if(this.getDisponibility(disp.getDateTimeInit(), disp.getDateTimeEnd()) == null){
            this.lstDisponibility.add(disp);
            return true;
        }
        return added;
    }
    
    /***
     * Validate if has Disponibility between Dates.
     * @param dateIni
     * @param dateEnd
     * @return the Disponibility if has or null if don't
     */
    public Disponibility getDisponibility(LocalDateTime dateIni, LocalDateTime dateEnd){
         return this.lstDisponibility.stream().filter(x-> ((x.getDateTimeInit().isAfter(dateIni) || x.getDateTimeInit().equals(dateIni)) )&& (x.getDateTimeEnd().isBefore(dateEnd) || x.getDateTimeEnd().equals(dateEnd))).findFirst().orElse(null);
    }
    
    /***
     * Validate if Service Provider has the Geographical Area
     * @param geoArea
     * @return 
     */
    public boolean hasGeoArea(GeographicalArea geoArea){
        return this.lstGeoAreas.contains(geoArea);
    }
    
    /***
     * Validate if Service Provider has the Category
     * @param cat
     * @return 
     */
    public boolean hasCategory(Category cat){
        return this.lstCategory.contains(cat);
    }
    
    /***
     * calculate mean of Rating
     * @return the mean
     */
    public Double getRatingMean(){
        Double total = Double.valueOf(this.ratings.size());
        if(total > 0){
          return  this.ratings.stream().mapToDouble(x-> Double.valueOf(x.getRate())).sum()/total;
        }else{
            total = 3.0;
        }
        return total;
    }
    
    public Double getDistance(String postalCode){
       PostalCode cp1 =  this.lstGeoAreas.get(0).getExternalService().getPostalCode(this.postalCode);
       PostalCode cp2 = this.lstGeoAreas.get(0).getExternalService().getPostalCode(postalCode);
      return  ActsIn.distance(cp1,cp2);
    }
    
    @Override
    public boolean equals(Object otherObject) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == otherObject)
            return true;
        // null check
        if (otherObject == null)
            return false;
        // type check and cast
        if (getClass() != otherObject.getClass())
            return false;
        // field comparison
        ServiceProvider obj = (ServiceProvider) otherObject;
        return (Objects.equals(this.getEmail(), obj.getEmail()) || 
                Objects.equals(this.getMechanographicNumber(), obj.getMechanographicNumber()) ||
                Objects.equals(this.getVatNumber(), obj.getVatNumber()));
    }
}
