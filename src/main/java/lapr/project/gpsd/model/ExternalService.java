/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ExternalService {

    private HashMap<String, Double[]> postalCodes;

    public ExternalService() {
        postalCodes = new HashMap<>();
    }

    public ExternalService(HashMap<String, Double[]> postalCodes) {
        this.postalCodes = postalCodes;
    }

    public void setPostalCodes(HashMap<String, Double[]> postalCodes) {
        this.postalCodes = postalCodes;
    }

    public PostalCode getPostalCode(String postalCode) {
        if (postalCodes.containsKey(postalCode)) {
            Double[] psValues = postalCodes.get(postalCode);
            return new PostalCode(postalCode, psValues[0], psValues[1]);
        } else {
            return null;
        }
    }
    
    
   public boolean isValidPostalCode(String postalCode){
       return postalCodes.containsKey(postalCode);
   }

    public boolean actsIn(String pc1, String pc2, Double radius) {
        if (postalCodes.containsKey(pc1) && postalCodes.containsKey(pc2)) {
            Double[] ps1Values = postalCodes.get(pc1);
            Double[] ps2Values = postalCodes.get(pc2);

            Double distance = ActsIn.distance(ps1Values[0], ps1Values[1], ps2Values[0], ps2Values[1]);
            return distance <= radius;
        }
        return false;
    }
    

    
    /**
     * get PostalCode by radius from another one
     * @param pc
     * @param radius
     * @return list of Postal Codes
     */
    public List<PostalCode> getActuation(String pc, double radius) {
        ArrayList<PostalCode> result = new ArrayList<>();
        PostalCode basePC = getPostalCode(pc);
        if (basePC != null) {
            for (String key : postalCodes.keySet()) {
                PostalCode newPC = getPostalCode(key);
                if (ActsIn.distance(basePC.getLatitude(), basePC.getLongitude(), newPC.getLatitude(), newPC.getLongitude()) <= radius) {
                    result.add(newPC);
                }
            }
        }

        return result;
    }
    
     public List<PostalCode> getListPC() {
        ArrayList<PostalCode> result = new ArrayList<>();
            for (String key : postalCodes.keySet()) {
                PostalCode newPC = getPostalCode(key);               
                    result.add(newPC);
            }
        return result;
    }

}
