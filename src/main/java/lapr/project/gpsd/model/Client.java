/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * @author paulomaio
 */
public class Client {

    private String name;
    private String vatNumber;
    private String phone;
    private String email;
    private List<PostalAddress> addresses = new ArrayList<PostalAddress>();

    public Client(String name, String vatNumber, String phone, String email) {
        if ((name == null) || (vatNumber == null) || (phone == null)
                || (email == null) || (name.isEmpty()) || (vatNumber.isEmpty()) 
                || (phone.isEmpty()) || (email.isEmpty())) {
            throw new IllegalArgumentException(
                    "No argument can be null or empty.");
        }

        this.name = name;
        this.email = email;
        this.vatNumber = vatNumber;
        this.phone = phone;
        this.addresses = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    public String getEmail() {
        return this.email;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public String getPhone() {
        return phone;
    }

    public List<PostalAddress> getAddresses() {
        return addresses;
    }

    public boolean hasEmail(String strEmail) {
        return this.email.equalsIgnoreCase(strEmail);
    }
    
    public boolean hasVAT(String vat) {
        return this.vatNumber.equalsIgnoreCase(vat);
    }

    public boolean addAddress(PostalAddress address) {
        if (!addresses.contains(address)) {
            return this.addresses.add(address);
        }
        return false;
    }

    public boolean removeAddress(PostalAddress address) {
        return this.addresses.remove(address);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/

        // self check
        if (this == o) {
            return true;
        }
        // null check
        if (o == null) {
            return false;
        }
        // type check and cast
        if (getClass() != o.getClass()) {
            return false;
        }
        // field comparison
        Client obj = (Client) o;
        return (Objects.equals(this.email, obj.email) || Objects.equals(this.vatNumber, obj.vatNumber));
    }

    @Override
    public String toString() {
        String str = String.format("%s - %s - %s - %s", this.name, this.vatNumber, this.phone, this.email);
        for (PostalAddress adr : this.addresses) {
            str += "\nAddress:\n" + adr.toString();
        }
        return str;
    }

    public static PostalAddress newPostalAddress(String address, String postalCode, String place) {
        return new PostalAddress(address, postalCode, place);
    }

}
