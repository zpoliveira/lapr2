/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.ServiceType;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class ServiceTypeRegister {

    private List<ServiceType> serviceTypesList = new ArrayList<>();
    
    public ServiceType newServiceType(String className, String descriptor) {
        return new ServiceType(className, descriptor);
    }
   
    public List<String> getServiceTypesNames(){
        List<String> actualListTypeNames = new ArrayList<>();
        for(ServiceType serviceType : this.serviceTypesList ){
            actualListTypeNames.add(serviceType.getDescriptor());
        }
        return actualListTypeNames;
    }

    public boolean registServiceType(ServiceType newService) {
        if (this.validateService(newService)) {
            return addServiceType(newService);
        }
        return false;
    }

    private boolean addServiceType(ServiceType newServiceType) {
        return this.serviceTypesList.add(newServiceType);
    }
    
    public boolean validateService(ServiceType newService) {
        boolean validService = true;
        for (ServiceType savedServiceType : this.serviceTypesList) {
            if (savedServiceType.equals(newService)) {
                validService = false;
            }
        }
        return validService;
    }
    
    public List<ServiceType> getServiceTypes(){
        List<ServiceType> actualServiceType = new ArrayList<>();
        for(ServiceType serviceType : this.serviceTypesList ){
            actualServiceType.add(serviceType);
        }
        return actualServiceType;
    } 

}
