/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.PostalAddress;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class ClientRegister {

    private final List<Client> clients;

    public ClientRegister() {
        clients = new ArrayList<>();
    }

    public Client getClientByEmail(String email) {
        for (Client client : this.clients) {
            if (client.hasEmail(email)) {
                return client;
            }
        }

        return null;
    }
    
    public Client getClientByVAT(String vat) {
        for (Client client : this.clients) {
            if (client.hasVAT(vat)) {
                return client;
            }
        }

        return null;
    }

    public boolean existsClient(Client cli) {
        return this.clients.stream().anyMatch(x -> x.equals(cli));
    }

    public Client newClient(String name, String vatNumber, String phone, String email, List<PostalAddress> lstAddress) {
        Client cli = new Client(name, vatNumber, phone, email);
        for (PostalAddress address : lstAddress) {
            cli.addAddress(address);
        }
        return cli;
    }
    
     public Client newClient(String name, String vatNumber, String phone, String email) {
        Client cli = new Client(name, vatNumber, phone, email);      
        return cli;
    }

    public boolean addClient(Client client) {
        return clients.add(client);
    }

}
