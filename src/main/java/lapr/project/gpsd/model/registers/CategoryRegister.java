/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.Category;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class CategoryRegister {

    private List<Category> categoriesList = new ArrayList<>();

    public CategoryRegister() {
        this.categoriesList = new ArrayList<>();
    }

    public Category newCategory(String code, String description) {
        return new Category(code, description);
    }

    public boolean registerCategory(Category category) {
        if (this.categoryValidator(category)) {
            return addCategory(category);
        }
        return false;
    }

    public Category getCategorieById(String strId) {
        for (Category cat : this.categoriesList) {
            if (cat.hasId(strId)) {
                return cat;
            }
        }
        return null;
    }
    
    public Category getCategorieByDescription(String strDesc) {
        for (Category cat : this.categoriesList) {
            if (cat.getDescription().equalsIgnoreCase(strDesc.trim())) {
                return cat;
            }
        }
        return null;
    }

    private boolean addCategory(Category category) {
        return this.categoriesList.add(category);
    }

    public boolean categoryValidator(Category category) {
        boolean ret = true;

        for (Category c : categoriesList) {
            if (c.equals(category)) {
                ret = false;
            }
        }
        return ret;
    }

    public List<Category> getCategories() {
        List< Category> lc = new ArrayList<>();
        lc.addAll(this.categoriesList);
        return lc;
    }
}
