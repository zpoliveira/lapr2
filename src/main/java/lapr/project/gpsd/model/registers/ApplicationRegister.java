/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.PostalAddress;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class ApplicationRegister {
    
    private List<Application> applicationRegister;

    public ApplicationRegister() {
        this.applicationRegister = new ArrayList<Application>();
    }

    public List<Application> getApplicationRegister() {
        return applicationRegister;
    }

    public void setApplicationRegister(List<Application> applicationRegister) {
        this.applicationRegister = applicationRegister;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.applicationRegister);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApplicationRegister other = (ApplicationRegister) obj;
        if (!Objects.equals(this.applicationRegister, other.applicationRegister)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ApplicationRegister: " + applicationRegister;
    }
    
    public boolean applicationValidator (Application app){        
        
        return !this.applicationRegister.contains(app);
    }
    
    public boolean registerApplication (Application app){
        
       return this.applicationRegister.add(app);
        
    }
    public Application newApplication(String name, String VATNumber, String phoneNumber, String email, String address, String postalCode, String local){

        PostalAddress postalA = new PostalAddress(address, postalCode, local);
        
        return new Application(name, VATNumber, phoneNumber, email, postalA);
        
    }
    
}
