/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.GeographicalArea;
import lapr.project.gpsd.model.ServiceProvider;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class ServiceProviderRegister {
    
    private List<ServiceProvider> serviceProviderList;

    public ServiceProviderRegister() {
        this.serviceProviderList = new ArrayList<>();              
    }
    
    public ServiceProvider newServiceProvider(int numMec, String completName, String shortName, String email, String vatNumber, String spPCode)
    {
        return new ServiceProvider(numMec, completName, shortName, email, vatNumber, spPCode);
    }
     
    public List<ServiceProvider> getServiceProviders() {
        List < ServiceProvider > sp = new ArrayList<>();
        sp.addAll(this.serviceProviderList);
        return sp;
    }

     public boolean registServiceProvider(ServiceProvider newServiceProvider) {
        if (this.validateServiceProvider(newServiceProvider)) {
            return addServiceProvider(newServiceProvider);
        }
        return false;
    }

    private boolean addServiceProvider(ServiceProvider newServiceProvider) {
        return this.serviceProviderList.add(newServiceProvider);
    }

    public boolean validateServiceProvider(ServiceProvider newServiceProvider) {
        boolean validServiceProvider = true;
        for (ServiceProvider savedServiceProvider : this.serviceProviderList) {
            if (savedServiceProvider.equals(newServiceProvider)) {
                validServiceProvider = false;
            }
        }
        return validServiceProvider;
    }
    
    public ServiceProvider getServiceProviderByEmailPartial(String email){
        ServiceProvider serviceProvider = null;
        for(ServiceProvider sp : this.serviceProviderList){
            if(sp.getEmail().toLowerCase().startsWith(email.toLowerCase())){
                serviceProvider = sp;
                break;
            }
        }
        return serviceProvider;
    }
    
    public ServiceProvider getServiceProviderByEmail(String email){
        ServiceProvider serviceProvider = null;
        for(ServiceProvider sp : this.serviceProviderList){
            if(sp.getEmail().equalsIgnoreCase(email)){
                serviceProvider = sp;
                break;
            }
        }
        return serviceProvider;
    }
    
    /****
     * Filter list of Service Provider by Geographical Area and Category and ordered by Rating, Distance, Name
     * @param geo Geographical Area to filter
     * @param cat Category to filter
     * @param postalCode PostalCode of Client
     * @return list of Service Providers filtered and ordered
     */
    public List<ServiceProvider> getServiceProviderByRatingDistanceName(GeographicalArea geo, Category cat,String postalCode) {
        List<ServiceProvider> sProvidersFiltered = filterByAGCat(geo,cat, postalCode);
        Collections.sort(sProvidersFiltered,comparatorByRating
                                        .thenComparingDouble(x->x.getDistance(postalCode))
                                        .thenComparing(x->x.getCompletName())
                                        );
        return sProvidersFiltered;
    }
    
    /****
     * Filter list of Service Provider by Geographical Area and Category and Postal Code
     * @param geo Geographical Area to filter
     * @param cat Category to filter
     * @return list of Service Providers filtered
     */
    private List<ServiceProvider> filterByAGCat(GeographicalArea geo, Category cat, String postalCode){
        return this.serviceProviderList.stream().filter(x-> x.hasCategory(cat) && x.hasGeoArea(geo) && x.getDistance(postalCode)>=0).collect(Collectors.toList());     
    }
    
    /***
     * variable to sort Service Providers by Rating
     */
    private final Comparator<ServiceProvider> comparatorByRating = (ServiceProvider r1, ServiceProvider r2) -> Double.compare(r1.getRatingMean(),r2.getRatingMean());
    
    
    public ServiceProvider getServiceProviderByMeca(int meca){
       for (ServiceProvider sp : this.serviceProviderList) {
            if (sp.getMechanographicNumber() == meca) {
                return sp;
            }
        }

        return null;
    }
}
