/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.util.ArrayList;
import lapr.project.gpsd.model.ActsIn;
import lapr.project.gpsd.model.ExternalService;
import lapr.project.gpsd.model.GeographicalArea;
import lapr.project.gpsd.model.PostalCode;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class GeographicalAreaRegister {

    private ArrayList<GeographicalArea> listGeoAreas = new ArrayList<>();

    public GeographicalArea newGeographicalArea(String designation, float transitCost,
            float workingRadius, String postalCode,
            ExternalService externalService) {

        return new GeographicalArea(designation, transitCost,
                workingRadius, postalCode, externalService);

    }

    public boolean registerGeographicalArea(GeographicalArea ga) {
        if (this.validateGeographicalArea(ga)) {
            return addGeographicalArea(ga);
        }
        return false;
    }

    private boolean addGeographicalArea(GeographicalArea ga) {
        return this.listGeoAreas.add(ga);
    }

    public boolean validateGeographicalArea(GeographicalArea ga) {
        for (GeographicalArea lstGa : listGeoAreas) {
            if (lstGa.equals(ga)) {
                return false;
            }
        }
        return true;
    }

    public ArrayList<GeographicalArea> getListGeoAreas() {
        return this.listGeoAreas;
    }

    public GeographicalArea getGeographicalAreaByPostalCode(String postalCode) {
        for (GeographicalArea geoArea : listGeoAreas) {
            if (geoArea.hasPostalCode(postalCode)) {
                return geoArea;
            }
        }
        return null;
    }
    
    public GeographicalArea getGeographicalAreaByDesignation(String designation) {
        for (GeographicalArea geoArea : listGeoAreas) {
            if (geoArea.getDesignation().equals(designation)) {
                return geoArea;
            }
        }
        return null;
    }

    public GeographicalArea getClosestGeoArea(PostalCode pc) {
        double distance = -1;
        GeographicalArea result = null;
        for (GeographicalArea ga : listGeoAreas) {
            PostalCode newPc = ga.getExternalService().getPostalCode(ga.getPostalCode());
            double temp = ActsIn.distance(pc, newPc);
            if (temp < distance || distance < 0) {
                distance = temp;
                result = ga;
            }
        }
        return result;
    }
}
