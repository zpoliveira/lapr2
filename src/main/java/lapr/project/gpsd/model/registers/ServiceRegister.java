/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Service;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class ServiceRegister {

    private List<Service> servicesList = new ArrayList<>();    
  
    public Service getServiceById(String strId) 
    {
        for (Service serv : this.servicesList) {
            if (serv.hasId(strId)) {
                return serv;
            }
        }

        return null;
    }
    

    public Service newService(
            String strId, 
            String littleDescription, 
            String largeDescription, 
            double costHour, 
            Category cat, 
            String serviceType) throws Exception 
    {
            Class serviceTypeClass = Class.forName("lapr.project.gpsd.model." + serviceType.replaceAll(" ", ""));
            Constructor construtor = serviceTypeClass.getConstructor(
                    String.class, 
                    String.class, 
                    String.class, 
                    double.class, 
                    Category.class
            );
            Service newService = (Service) construtor.newInstance(
                    strId, 
                    littleDescription, 
                    largeDescription, 
                    costHour, 
                    cat
            );
            return newService;
    }
    

    public ArrayList<Service> getServiceListByCategory(Category cat){
        ArrayList<Service> result = new  ArrayList<>();
        for (Service serv : servicesList){
            if (cat.equals(serv.getCategory())){
                result.add(serv);
            }
        }
        return result;
    }


    public boolean registService(Service newService) 
    {
        if (this.validateService(newService)) {
            return addService(newService);
        }
        return false;
    }

    private boolean addService(Service newService) {
        return this.servicesList.add(newService);
    }

    public boolean validateService(Service newService) {
        boolean validService = true;
            for (Service savedService : this.servicesList) {
                if(savedService !=null){
                    if (savedService.equals(newService)) {
                    validService = false;
                    }
                }
            }

        return validService;
    }

}
