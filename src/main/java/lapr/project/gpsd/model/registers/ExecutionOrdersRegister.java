/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.ExecutionOrder;
import lapr.project.gpsd.model.ServiceProvider;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class ExecutionOrdersRegister {

    private List<ExecutionOrder> executionOrdersregister;

    public ExecutionOrdersRegister() {
        this.executionOrdersregister = new ArrayList<>();
    }

    public List<ExecutionOrder> getExecutionOrdersregister() {
        return executionOrdersregister;
    }

    public List<ExecutionOrder> getExecutionOrdersBySp(ServiceProvider sp) {
        List<ExecutionOrder> newList = new ArrayList<>();
        for (ExecutionOrder exec : this.executionOrdersregister) {
            if (exec != null && sp!=null) {
                if (sp.equals(exec.getServiceProvider())) {
                    newList.add(exec);
                }
            }
        }
        return newList;
    }

    public ExecutionOrder getExecutionOrderById(int id) {
        ExecutionOrder findedExec=null;
        for (ExecutionOrder exec : this.executionOrdersregister) {
            if (exec != null) {
                if (exec.getId() == id) {
                    findedExec = exec;
                }
            }
        }
        return findedExec;
    }

    public List<ExecutionOrder> getExecutionOrdersByClient(Client client) {
        List<ExecutionOrder> newList = new ArrayList<>();
        for (ExecutionOrder exec : this.executionOrdersregister) {
            if (exec != null) {
                if (client.getEmail().equalsIgnoreCase(exec.getClientEmail())) {
                    newList.add(exec);
                }
            }
        }
        return newList;
    }

    public boolean addExecutionOrder(ExecutionOrder execOrder) {

        if (validateExecutionOrder(execOrder)) {
            return this.executionOrdersregister.add(execOrder);
        }

        return false;
    }

    private boolean validateExecutionOrder(ExecutionOrder execOrder) {
        return !this.executionOrdersregister.contains(execOrder);
    }
    
    public ExecutionOrder getexecutionOrderByID(int id){
    for(ExecutionOrder exec : this.executionOrdersregister){
            if(exec.getId() == id){
                return exec;
            }
        }   
        return null;
    }
    
    
    
}
