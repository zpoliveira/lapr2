/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.RequestStatus;
import lapr.project.gpsd.model.ServiceRequest;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class ServiceRequestRegister {

    private List<ServiceRequest> serviceRequests;

    public ServiceRequestRegister() {
        this.serviceRequests = new ArrayList<>();
    }

    public List<ServiceRequest> getClientAssignedServiceRequests(String email) {
        List<ServiceRequest> userReqs = new ArrayList<>();

        for (ServiceRequest sr : serviceRequests) {
            if (sr.getClientEmail().equalsIgnoreCase(email) && sr.getStatus().equals(RequestStatus.ASSIGNED)) {
                userReqs.add(sr);
            }
        }
        return userReqs;
    }

    public ServiceRequest newServiceRequest(
            String clientEmail,
            PostalAddress postalAddress) {
        ServiceRequest servReq = new ServiceRequest(clientEmail, postalAddress);
        return servReq;
    }

    public boolean registerServiceRequest(ServiceRequest sr) {
        if (this.validateServiceRquest(sr)) {
            return this.addServiceRequest(sr);
        }
        return false;
    }

    private boolean addServiceRequest(ServiceRequest sr) {
        return this.serviceRequests.add(sr);
    }

    public ServiceRequest getServiceRequestById(int id) {
        for (ServiceRequest sr : serviceRequests) {
            if (sr.getID() == id) {
                return sr;
            }
        }
        return null;
    }

    public boolean validateServiceRquest(ServiceRequest newServiceRequest) {
        boolean validServReq = true;
        for (ServiceRequest savedServReq : this.serviceRequests) {
            if (savedServReq != null) {
                if (savedServReq.equals(newServiceRequest)) {
                    validServReq = false;
                }
            }
        }
        return validServReq;
    }

    /**
     * *
     * Sort the list by the service request ID and filter by status NEW
     *
     * @return List of ServiceResquest
     */
    public List<ServiceRequest> getResquestByNumber() {
        Collections.sort(serviceRequests, comparatorByNumber);

        return this.serviceRequests.stream().filter(x -> x.getStatus() == RequestStatus.NEW).collect(Collectors.toList());
    }

    /**
     * *
     * Shuffle and filter by status NEW
     *
     * @return List of ServiceResquest
     */
    public List<ServiceRequest> getResquestRandom() {
        Collections.shuffle(serviceRequests);
        return this.serviceRequests.stream().filter(x -> x.getStatus() == RequestStatus.NEW).collect(Collectors.toList());
    }

    /**
     * *
     * variable to sort Service Request by ID
     */
    private final Comparator<ServiceRequest> comparatorByNumber = new Comparator<ServiceRequest>() {
        @Override
        public int compare(ServiceRequest r1, ServiceRequest r2) {
            return Integer.compare(r1.getID(), r2.getID());
        }
    };

}
