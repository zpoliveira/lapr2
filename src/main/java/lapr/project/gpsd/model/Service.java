/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.Objects;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public interface Service
{
    public int DEFAULTDURATION = 30;
    public boolean hasId(String strId);
    
    public String getId();
    public String getLittleDescription();
    public double getCostHour();
    public String getTypeName();

    /**
     *
     * @param otherObject
     * @return
     */
    default boolean ourEquals(Object otherObject) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == otherObject)
            return true;
        // null check
        if (otherObject == null)
            return false;
        // type check and cast
        if (getClass() != otherObject.getClass())
            return false;
        // field comparison
        Service obj = (Service) otherObject;
        return (Objects.equals(this.getId(), obj.getId()));
    }
    
    public Category getCategory();
        
    public double getCost4Duration(int duration);
}
