/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ServiceRequest {

    private final int ID;
    private static int numServReqTotal;
    private final String clientEmail;
    private double totalCost;
    private final PostalAddress postalAddress;
    private final Calendar creationDate;
    private List<ServiceRequestDescription> serviceDescriptions;
    private List<OtherCost> listOtherCost;
    private List<SchedulePreference> lstSchedulePreference;
    private RequestStatus status;

    public ServiceRequest(String clientEmail, PostalAddress postalAddress) {
        if (clientEmail == null || clientEmail.isEmpty() || postalAddress == null) {
            throw new IllegalArgumentException("No argument can be null or empty.");
        }
        this.clientEmail = clientEmail;
        this.postalAddress = postalAddress;

        this.creationDate = Calendar.getInstance();
        this.serviceDescriptions = new ArrayList<>();
        this.listOtherCost = new ArrayList<>();
        this.totalCost = 0;
        this.status = RequestStatus.NEW;

        ServiceRequest.numServReqTotal++;
        this.lstSchedulePreference = new ArrayList<>();

        this.ID = ServiceRequest.numServReqTotal;

    }

    public int getID() {
        return this.ID;
    }

    public RequestStatus getStatus() {
        return this.status;

    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public String getClientEmail() {
        return this.clientEmail;
    }

    public PostalAddress getPostalAddress() {
        return new PostalAddress(this.postalAddress);
    }

    public Calendar getCreationDate() {
        return this.creationDate;
    }

    public double getTotalCost() {
        return this.totalCost;
    }

    private double getServicesCost() {
        int cost = 0;
        for (ServiceRequestDescription desc : serviceDescriptions) {
            if (desc.getService() instanceof FixedService) {
                FixedService fixedService = (FixedService) desc.getService();
                cost += fixedService.getCost4Duration(fixedService.getDuration());
            } else {
                cost += desc.getService().getCost4Duration(desc.getDuration());
            }

        }
        return cost;
    }
    
    private double getOtherCosts(){
        int cost = 0;
        for(OtherCost oc : listOtherCost){
            cost += oc.getValue();
        }
        return cost;
    }

    public List<ServiceRequestDescription> getServiceDescriptions() {
        return this.serviceDescriptions;
    }

    public List<SchedulePreference> getLstSchedulePreference() {
        return this.lstSchedulePreference;
    }

    public List<OtherCost> getListOtherCost() {
        return this.listOtherCost;
    }

    public boolean calculateCost(Company comp) {

        try {
        double otherCost = 0;

        PostalCode pc
                = comp.getExternalService().getPostalCode(postalAddress.getPostalCode());

        GeographicalArea ga
                = comp.getGeographicalAreaRegister().getClosestGeoArea(pc);

        PostalCode gaPc
                = comp.getExternalService().getPostalCode(ga.getPostalCode());

        otherCost = ga.getTransitCost() * ActsIn.distance(pc, gaPc);

        OtherCost oc = new OtherCost("Deslocation", otherCost);

        this.listOtherCost.add(oc);
        
        double servCost = getServicesCost();
        double otherCosts = getOtherCosts();
        this.totalCost = servCost + otherCosts;
        
        return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public List<ServiceRequestDescription> getServiceRequestDescriptions() {
        return this.serviceDescriptions;
    }

    public boolean addDescription(Service serv, String description, int duration) {
        ServiceRequestDescription desc = new ServiceRequestDescription(serv, description, duration);
        return this.serviceDescriptions.add(desc);
    }

    public boolean addSchedulePreference(LocalDateTime schedule) {
        SchedulePreference newSp = new SchedulePreference(
                lstSchedulePreference.size() + 1,
                schedule);
        if (validateSchedule(newSp)) {
            return lstSchedulePreference.add(newSp);
        }
        return false;
    }

    private boolean validateSchedule(SchedulePreference sp) {
        for (SchedulePreference spCheck : lstSchedulePreference) {
            if (sp.equals(spCheck)) {
                return false;
            }
        }
        return true;
    }

    /**
     * *
     * Filter the Schedule Preference by Today and order by order
     *
     * @return List with Schedule preference of client
     */
    public List<SchedulePreference> getListSchedulePreferenceByOrderFilterByNow() {
        List<SchedulePreference> lstFiltered = this.lstSchedulePreference.stream().filter(x -> x.getDateHourBegin().isAfter(LocalDateTime.now())).collect(Collectors.toList());
        if (lstFiltered.isEmpty()) {
            this.status = RequestStatus.DUE;
        } else {
            Collections.sort(lstFiltered, (SchedulePreference r1, SchedulePreference r2) -> Integer.compare(r1.getOrder(), r2.getOrder()));
        }
        return lstFiltered;
    }
    
    
    public ServiceRequestDescription getServiceRequestDescriptionByService(Service service){
        ServiceRequestDescription servReqDescr= null;
        for(ServiceRequestDescription servDesc : this.serviceDescriptions){
            if(servDesc.getService().equals(service)){
                servReqDescr = servDesc;
            }
        }
        return servReqDescr;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/

        // self check
        if (this == o) {
            return true;
        }
        // null check
        if (o == null) {
            return false;
        }
        // type check and cast
        if (getClass() != o.getClass()) {
            return false;
        }
        // field comparison
        ServiceRequest obj = (ServiceRequest) o;
        return (Objects.equals(this.clientEmail, obj.clientEmail)
                && Objects.equals(this.creationDate, obj.creationDate)
                && Objects.equals(this.listOtherCost, obj.listOtherCost)
                && Objects.equals(this.postalAddress, obj.postalAddress)
                && Objects.equals(this.serviceDescriptions, obj.serviceDescriptions)
                && Objects.equals(this.status, obj.status));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hash(this.clientEmail, this.creationDate, this.listOtherCost, this.postalAddress, this.serviceDescriptions, this.status);
        return hash;
    }

}
