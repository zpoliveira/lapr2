/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.time.LocalDateTime;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ExecutionExportData {

    private int id;
    private String clientName;
    private PostalAddress postalAddr;
    private double distance;
    private Category cat;
    private String serviceType;
    private LocalDateTime date;
    private BooleanProperty isEnded;
    private StringProperty hasIssue;

    public ExecutionExportData(int id, String clientName, PostalAddress postalAddr, double distance, Category cat, String serviceType, LocalDateTime date, boolean isEnded, String hasIssue) {
        this.id = id;
        this.clientName = clientName;
        this.postalAddr = postalAddr;
        this.distance = distance;
        this.cat = cat;
        this.serviceType = serviceType;
        this.date = date;
        this.isEnded = new SimpleBooleanProperty(isEnded);
        this.hasIssue = new SimpleStringProperty(hasIssue);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public PostalAddress getPostalAddr() {
        return postalAddr;
    }

    public void setPostalAddr(PostalAddress postalAddr) {
        this.postalAddr = postalAddr;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Category getCat() {
        return cat;
    }

    public void setCat(Category cat) {
        this.cat = cat;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public boolean isIsEnded() {
        return isEnded.get();
    }

    public void setIsEnded(boolean isEnded) {
        this.isEnded.set(isEnded);
    }

    public BooleanProperty isEndedProperty() {
        return isEnded;
    }

    public String getHasIssue() {
        return hasIssue.get();
    }

    public void setHasIssue(String hasIssue) {
        this.hasIssue.set(hasIssue);
    }

    public StringProperty hasIssue() {
        return hasIssue;
    }

}
