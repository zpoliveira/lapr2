/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.Objects;
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * @author paulomaio
 */
public class Category
{
    private String code;
    private String description;
            
    
    public Category(String code, String description)
    {
        if ( (code == null) || (description == null) ||
                (code.isEmpty())|| (description.isEmpty()))
            throw new IllegalArgumentException("No argument can be empty.");
        
        this.code = code;
        this.description = description;
    }
    
    public boolean hasId(String strId)
    {
        return this.code.equalsIgnoreCase(strId);
    }
    

    public String getCatId()
    {
        return this.code;
    }

    public String getDescription() {
        return description;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.code);
        return hash;
    }
    
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        Category obj = (Category) o;
        return (Objects.equals(code, obj.code));
    }
    
    @Override
    public String toString()
    {
        return String.format("%s - %s ", this.code, this.description);
    }
}