/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class GeographicalArea {

    private String designation;
    private double transitCost; //in .2f €
    private double workingRadius; //in KM
    private String postalCode;
    private ExternalService externalService;
    private List<PostalCode> listActionArea;

    public GeographicalArea(String designation, double transitCost,
            double workingRadius, String postalCode,
            ExternalService externalService) {

        if (designation == null || transitCost <= 0 || workingRadius <= 0
                || postalCode == null || externalService == null) {
            throw new IllegalArgumentException("Arguments not valid.");
        }

        this.designation = designation;
        this.transitCost = transitCost;
        this.workingRadius = workingRadius;
        this.postalCode = postalCode;
        this.externalService = externalService;
        this.listActionArea = this.externalService.getActuation(postalCode, workingRadius);
    }

    public GeographicalArea(String designation, double transitCost, double workingRadius, String postalCode) {
        if (designation == null || transitCost <= 0 || workingRadius <= 0
                || postalCode == null) {
            throw new IllegalArgumentException("Arguments not valid.");
        }
        this.designation = designation;
        this.transitCost = transitCost;
        this.workingRadius = workingRadius;
        this.postalCode = postalCode;
    }
    
    

    //<editor-fold defaultstate="collapsed" desc=" GetSet Region ">
    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public double getTransitCost() {
        return transitCost;
    }

    public void setTransitCost(float transitCost) {
        this.transitCost = transitCost;
    }

    public double getWorkingRadius() {
        return this.workingRadius;
    }

    public void setWorkingRadius(int workingRadius) {
        this.workingRadius = workingRadius;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public List<PostalCode> getListActionArea() {
        return this.listActionArea;
    }

    public ExternalService getExternalService() {
        return this.externalService;
    }

    public boolean hasPostalCode(String postalCode) {
        for (PostalCode ps : listActionArea) {
            if(ps.getPostalCode().equals(postalCode)){
                return true;
            }        
        }
        return false;
    }
    // </editor-fold>

   
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GeographicalArea other = (GeographicalArea) obj;
        if (!Objects.equals(this.postalCode, other.postalCode)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + (int) (Double.doubleToLongBits(this.transitCost) ^ (Double.doubleToLongBits(this.transitCost) >>> 32));
        hash = 13 * hash + Objects.hashCode(this.postalCode);
        return hash;
    }

}
