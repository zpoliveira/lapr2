/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.time.LocalDateTime;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class Disponibility {
    private LocalDateTime dateTimeInit;
    private LocalDateTime dateTimeEnd;
    private boolean available;

    public Disponibility(LocalDateTime dateTimeInit, LocalDateTime dateTimeEnd, boolean available) {
        this.dateTimeInit = dateTimeInit;
        this.dateTimeEnd = dateTimeEnd;
        this.available = available;
    }

    public Disponibility(LocalDateTime dateTimeInit, LocalDateTime dateTimeEnd) {
        this.dateTimeInit = dateTimeInit;
        this.dateTimeEnd = dateTimeEnd;
        this.available = true;
    }

    public LocalDateTime getDateTimeInit() {
        return dateTimeInit;
    }

    public LocalDateTime getDateTimeEnd() {
        return dateTimeEnd;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
    
    
    
}
