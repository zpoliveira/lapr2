/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.Objects;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class PostalAddress
{
    private String address;
    private String postalCode;
    private String place;
    
            
    
    public PostalAddress(String address, String postalCode, String place)
    {
        if ( (place == null) || (postalCode == null) || (address == null) ||
                (place.isEmpty())|| (postalCode.isEmpty()) || (address.isEmpty()))
            throw new IllegalArgumentException("Parameter can't be null or empty.");
        
        this.address = address;
        this.postalCode = postalCode;
        this.place = place;
    }
    
    
    public PostalAddress(PostalAddress postalAddress)
    {
        this.address = postalAddress.address;
        this.place = postalAddress.place;
        this.postalCode = postalAddress.postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hash(this.address,this.postalCode, this.place);
        return hash;
    }
    
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        PostalAddress obj = (PostalAddress) o;
        return (Objects.equals(this.address, obj.address) && 
                Objects.equals(this.place, obj.place) &&
                Objects.equals(this.postalCode, obj.postalCode));
    }
    
    @Override
    public String toString()
    {
        return String.format("%s - %s - %s", this.address, this.postalCode, this.place);
    }
    
}
