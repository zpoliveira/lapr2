/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.time.LocalDateTime;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ServiceRequestDescription {

    private ServiceProvider serviceProvider;
    private Disponibility disponibility;

    private final Service service;
    private final String description;
    private int duration;
    private double cost;
    private LocalDateTime startDate;

    public ServiceRequestDescription(
            Service service,
            String description,
            int duration) {
        this.service = service;
        this.description = description;
        this.duration = duration;
        this.cost = service.getCost4Duration(duration);
    }

    public Service getService() {
        return service;
    }

    public Disponibility getDisponibility() {
        return disponibility;
    }

    public void setDisponibility(Disponibility disponibility) {
        this.disponibility = disponibility;
    }

    public ServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getDescription() {
        return this.description;
    }

    public int getDuration() {
        return this.duration<30?30:this.duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }


    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime date) {
        this.startDate = date;
    }

    public double getCost() {
        return this.cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return true;          
    }

}
