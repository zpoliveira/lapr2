/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.Objects;
import java.util.Timer;
import lapr.project.auth.AuthFacade;
import lapr.project.gpsd.controller.AssociateServiceProviderTask;
import lapr.project.gpsd.model.registers.ApplicationRegister;
import lapr.project.gpsd.model.registers.CategoryRegister;
import lapr.project.gpsd.model.registers.ClientRegister;
import lapr.project.gpsd.model.registers.ExecutionOrdersRegister;
import lapr.project.gpsd.model.registers.GeographicalAreaRegister;
import lapr.project.gpsd.model.registers.ServiceProviderRegister;
import lapr.project.gpsd.model.registers.ServiceRegister;
import lapr.project.gpsd.model.registers.ServiceRequestRegister;
import lapr.project.gpsd.model.registers.ServiceTypeRegister;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * @author paulomaio
 */
public class Company {

    private String companyDescription;
    private String companyVatNum;
    private long taskDelay;
    private long taskInterval;
    private TaskSchedule taskSchedule;
    private final AuthFacade auth;
    private final ClientRegister clientRegister;
    private final CategoryRegister categoryRegister;
    private final ServiceRegister serviceRegister;
    private final ApplicationRegister applicationRegister;
    private final GeographicalAreaRegister geographicalAreaRegister;
    private final ServiceProviderRegister serviceProviderRegister;
    private final ServiceTypeRegister serviceTypeRegister;
    private final ServiceRequestRegister serviceRequestRegister;
    private final ExecutionOrdersRegister executionOrdersRegister;
    private final ExternalService externalService;

    
    private static final long DEFAULT_TASKDELAY = 1000;
    private static final long DEFAULT_TASKINTERVAL = 600000;
    private static final TaskSchedule DEFAULT_TASKSCHEDULE = TaskSchedule.FIFO;

    public Company(String compDescription, String compVat) {

        if ((compDescription == null) || (compVat == null)
                || (compDescription.isEmpty()) || (compVat.isEmpty())) {
            throw new IllegalArgumentException("None of the fields can't be empty!");
        }

        this.companyDescription = compDescription;
        this.companyVatNum = compVat;
        this.taskDelay = DEFAULT_TASKDELAY;
        this.taskInterval = DEFAULT_TASKINTERVAL;
        this.taskSchedule = DEFAULT_TASKSCHEDULE;
        this.auth = new AuthFacade();
        this.clientRegister = new ClientRegister();
        this.categoryRegister = new CategoryRegister();
        this.serviceRegister = new ServiceRegister();
        this.applicationRegister = new ApplicationRegister();
        this.geographicalAreaRegister = new GeographicalAreaRegister();
        this.serviceProviderRegister = new ServiceProviderRegister();
        this.serviceTypeRegister = new ServiceTypeRegister();
        this.serviceRequestRegister = new ServiceRequestRegister();
        this.executionOrdersRegister = new ExecutionOrdersRegister();
        this.externalService = new ExternalService();
    }
    
    
    
    public Company(String compDescription, String compVat, Long delay, Long interval, TaskSchedule schedule) {

        if ((compDescription == null) || (compVat == null)
                || (compDescription.isEmpty()) || (compVat.isEmpty()) || delay == null || interval == null || schedule == null) {
            throw new IllegalArgumentException("None of the fields can't be empty!");
        }

        this.companyDescription = compDescription;
        this.companyVatNum = compVat;
        setTaskDelay(delay);
        setTaskInterval(interval);
        setTaskSchedule(schedule);
        this.auth = new AuthFacade();
        this.clientRegister = new ClientRegister();
        this.categoryRegister = new CategoryRegister();
        this.serviceRegister = new ServiceRegister();
        this.applicationRegister = new ApplicationRegister();
        this.geographicalAreaRegister = new GeographicalAreaRegister();
        this.serviceProviderRegister = new ServiceProviderRegister();
        this.serviceTypeRegister = new ServiceTypeRegister();
        this.serviceRequestRegister = new ServiceRequestRegister();
        this.executionOrdersRegister = new ExecutionOrdersRegister();
        this.externalService = new ExternalService();
        
    }
    
    

    public AuthFacade getAuthFacade() {
        return this.auth;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public String getCompanyVatNum() {
        return companyVatNum;
    }

    public long getTaskDelay() {
        return taskDelay;
    }

    private void setTaskDelay(long taskDelay) {
        if (taskDelay < 0){
            this.taskDelay = DEFAULT_TASKDELAY;
        }else{
            this.taskDelay = taskDelay;
        }
        
    }

    public long getTaskInterval() {
        return taskInterval;
    }

    private void setTaskInterval(long taskInterval) {
        if (taskInterval < 0){
            this.taskInterval = DEFAULT_TASKDELAY;
        }else{
            this.taskInterval = taskInterval;
        }
    }

    public TaskSchedule getTaskSchedule() {
        return taskSchedule;
    }

    private void setTaskSchedule(TaskSchedule taskSchedule) {
        switch(taskSchedule){
            case FIFO:
            case RANDOM:
                this.taskSchedule = taskSchedule;
                break;
            default:
                this.taskSchedule = DEFAULT_TASKSCHEDULE;
                break;
                
        }
    }

    
    
    public ClientRegister getClientRegister() {
        return clientRegister;
    }

    public CategoryRegister getCategoryRegister() {
        return categoryRegister;
    }

    public ServiceRegister getServiceRegister() {
        return serviceRegister;
    }

    public ApplicationRegister getApplicationRegister() {
        return applicationRegister;
    }

    public GeographicalAreaRegister getGeographicalAreaRegister() {
        return geographicalAreaRegister;
    }

    public ServiceProviderRegister getServiceProviderRegister() {
        return serviceProviderRegister;
    }

    public ServiceTypeRegister getServiceTypeRegister() {
        return serviceTypeRegister;
    }

    public ServiceRequestRegister getServiceRequestRegister() {
        return serviceRequestRegister;
    }

    public ExternalService getExternalService() {
        return externalService;
    }

    public ExecutionOrdersRegister getExecutionOrdersRegister() {
        return executionOrdersRegister;
    }
    
    public void initTask(){
        AssociateServiceProviderTask task = new AssociateServiceProviderTask(this);
        
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(task, this.getTaskDelay(), this.getTaskInterval());
        
    }

    @Override
    public boolean equals(Object obj) {
         // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/

        // self check
        if (this == obj) {
            return true;
        }
        // null check
        if (obj == null) {
            return false;
        }
        // type check and cast
        if (getClass() != obj.getClass()) {
            return false;
        }
        // field comparison
        Company objt = (Company) obj;
        return (Objects.equals(this.companyVatNum, objt.getCompanyVatNum()));
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.companyDescription);
        hash = 89 * hash + Objects.hashCode(this.companyVatNum);
        return hash;
    }
    
    

}
