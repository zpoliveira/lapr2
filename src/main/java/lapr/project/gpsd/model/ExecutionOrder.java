/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.Objects;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ExecutionOrder {
    
    private int id;
    
    private Service service;
    
    private int servicerequestID;
    
    private ServiceProvider serviceProvider;
    
    private int duration;
    
    private String clientEmail;
    
    private boolean rated;
    
    private boolean isEnded;
    
    private String hasIssue;
    
    public ExecutionOrder(int id, int servicerequestID, ServiceProvider serviceProvider, Service service, int duration, String clientEmail){
        this.id = id;
        this.servicerequestID = servicerequestID;
        this.serviceProvider = serviceProvider;
        this.service = service;
        this.duration = duration;
        this.clientEmail = clientEmail;
        this.rated = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public int getServicerequestID() {
        return servicerequestID;
    }

    public void setServicerequestID(int servicerequestID) {
        this.servicerequestID = servicerequestID;
    }

    public ServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public boolean isRated() {
        return rated;
    }

    public void setRated(boolean rated) {
        this.rated = rated;
    }

    public boolean isIsEnded() {
        return isEnded;
    }

    public void setIsEnded(boolean isEnded) {
        this.isEnded = isEnded;
    }

    public String getHasIssue() {
        return hasIssue;
    }

    public void setHasIssue(String hasIssue) {
        this.hasIssue = hasIssue;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExecutionOrder other = (ExecutionOrder) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Execution Order: " + 
                "ID: " + id + 
                "Service: " + service + 
                "srID:" + servicerequestID + 
                "Service Provider:" + serviceProvider +
                "Duration: " + duration + 
                "Client Email: " + clientEmail;
    }
    
    
    
}