/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.Objects;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ExpansibleService implements Service {

    private String strId;
    private String littleDescription;
    private String largeDescription;
    private double costHour;
    private Category cat;

    public ExpansibleService(String strId, String littleDescription, String largeDescription, double costHout, Category cat) {
        if ((strId == null) || (littleDescription == null) || (largeDescription == null)
                || (costHout < 0) || (cat == null)
                || (strId.isEmpty()) || (littleDescription.isEmpty()) || (largeDescription.isEmpty())) {
            throw new IllegalArgumentException("None of the arguments should be empty.");
        }

        this.strId = strId;
        this.littleDescription = littleDescription;
        this.largeDescription = largeDescription;
        this.costHour = costHout;
        this.cat = cat;
    }

    public String getStrId() {
        return this.strId;
    }

    public String getLittleDescription() {
        return this.littleDescription;
    }

    public double getCostHour() {
        return this.costHour;
    }

    @Override
    public String getTypeName() {
        return this.getClass().getSimpleName();
    }

    
    @Override
    public String getId() {
        return this.strId;
    }

    @Override
    public Category getCategory() {
        return this.cat;
    }

    @Override
    public double getCost4Duration(int duration) {
        return (duration/30) * this.costHour;
    }

    @Override
    public boolean hasId(String strId) {
        return this.strId.equalsIgnoreCase(strId);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.strId);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        return this.ourEquals(o);
    }

    @Override
    public String toString() {
        return String.format("%s - %s - %s - %.2f - Expansible Service: %s", this.strId, this.littleDescription, this.largeDescription, this.costHour, this.cat.toString());
    }

}
