/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.Objects;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ServiceType {
    private String className;
    private String descriptor;

    public ServiceType(String className, String descriptor) {
        this.className = className;
        this.descriptor = descriptor;
    }

    public String getClassName() {
        return className;
    }

    public String getDescriptor() {
        return descriptor;
    }
    
    
     @Override
    public boolean equals(Object otherObject) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == otherObject)
            return true;
        // null check
        if (otherObject == null)
            return false;
        // type check and cast
        if (getClass() != otherObject.getClass())
            return false;
        // field comparison
        ServiceType obj = (ServiceType) otherObject;
        return (Objects.equals(this.getClassName(), obj.getClassName()));
    }  
}
