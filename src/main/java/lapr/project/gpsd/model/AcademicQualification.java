/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.Objects;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class AcademicQualification {

    private String designation;
    private String degree;
    private String classification;

    public AcademicQualification(String designation, String degree, String classification) {
        this.designation = designation;
        this.degree = degree;
        this.classification = classification;
    }

    public String getDesignation() {
        return designation;
    }

    public String getDegree() {
        return degree;
    }

    public String getClassification() {
        return classification;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.designation);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AcademicQualification other = (AcademicQualification) obj;
        if (!Objects.equals(this.designation, other.designation)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AcademicQualification: Designation: " + designation + " | Degree: " + degree + " | Classification: " + classification;
    }
    
    

}
