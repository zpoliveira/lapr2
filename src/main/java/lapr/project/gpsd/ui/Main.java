/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lapr.project.gpsd.ui;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 /**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class Main extends Application
{  
     public static void main(String[] args) throws Exception {
        launch(args);
    }

    public void start(Stage stage) throws Exception {
        //String fxmlFile = "/fxml/mainFXML.fxml";
        String fxmlFile = "/fxml/startFXML.fxml";
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlFile));
        Parent rootNode = (Parent) loader.load();
        Scene scene = new Scene(rootNode);
        scene.getStylesheets().add("/styles/startfxml.css");

        stage.setTitle("GPSD");
        stage.setScene(scene);
        
        //Call controlller to start read files
        AuthController auth = new AuthController();
        
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
                public void handle(WindowEvent event) {
                    Alert alert = FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, 
                            "Please Confirm your Action", 
                            "Really want to exit?");
                    if (alert.showAndWait().get() == ButtonType.CANCEL) {
                        event.consume();
                    }
                }
            });
        
        stage.show();
}
}
