/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import lapr.project.gpsd.model.ExecutionExportData;
import static lapr.project.gpsd.utils.Utils.bfwrite;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class FxmlUtils {

    public static void newWindow(String title, Parent content) {
        Scene newScene = new Scene(content);
        Stage newWindow = new Stage();
        newWindow.setTitle(title);
        newWindow.setScene(newScene);
        newWindow.initModality(Modality.APPLICATION_MODAL);
        newWindow.show();
        newWindow.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Alert alert = FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION,
                        "Please Confirm your Action",
                        "Really want to exit all data of this window will be lost?");
                if (alert.showAndWait().get() == ButtonType.CANCEL) {
                    event.consume();
                }
            }
        });
    }

    public static Alert createAlert(Alert.AlertType alertType,
            String header,
            String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(alertType.toString());
        alert.setHeaderText(header);
        alert.setContentText(message);
        return alert;
    }

    public static Pane createNewBorderPane(String fxmlPath) {
        Pane pane = new Pane();
        try {
            FXMLLoader loader = new FXMLLoader();
            pane = loader.load(FxmlUtils.class.getResource(fxmlPath));
        } catch (IOException ex) {
            Logger.getLogger(FxmlUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pane;
    }

    public static void doLogout(Pane internalPane) {
        Pane pane = FxmlUtils.createNewBorderPane("/fxml/mainFXML.fxml");
        internalPane.getScene().setRoot(pane);
    }

    /**
     * *
     * validates if email is valid
     *
     * @param email
     * @return boolean
     */
    public static boolean isEmail(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    /**
     * *
     * validates if vat number is valid
     *
     * @param vat
     * @return boolean
     */
    public static boolean isVatNumber(String vat) {
        String exp = "^[1-9][0-9]{8}$";

        return vat.matches(exp);
    }

    /**
     * *
     * validates if phone number is valid
     *
     * @param phone
     * @return boolean
     */
    public static boolean isPhoneNumber(String phone) {
        String exp = "^(2|9)[0-9]{8}$";

        return phone.matches(exp);
    }

    /**
     * *
     * validates if postal code is valid
     *
     * @param postal code
     * @return boolean
     */
    public static boolean isPostalCode(String postalCode) {
        String exp = "^[1-9][0-9]{3}-[0-9]{3}$";

        return postalCode.matches(exp);
    }

    public static boolean isInt(String intValue) {
        boolean isInt = false;
        try {
            Integer.parseInt(intValue);
            isInt = true;
        } catch (Exception e) {
            isInt = false;
        }
        return isInt;
    }

    public static void makeToast(Stage actualStage, String toastMsg, boolean success) {
        int fadeInDelay = 100;
        int fadeOutDelay = 300;
        int toastDelay = 2000;
        Stage toastStage = new Stage();
        toastStage.initOwner(actualStage);
        toastStage.setResizable(false);
        toastStage.initStyle(StageStyle.TRANSPARENT);
        Text text = new Text(toastMsg);
        if (success) {
            text.setFill(Color.GREEN);
        } else {
            text.setFill(Color.RED);
        }

        StackPane root = new StackPane(text);
        root.setStyle("-fx-background-radius: 30; -fx-background-color: rgba(0, 0, 0, 0.1); -fx-padding: 20px;");
        root.setOpacity(0);

        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        toastStage.setScene(scene);
        toastStage.show();

        Timeline fadeInTimeline = new Timeline();
        KeyFrame fadeInKey = new KeyFrame(Duration.millis(fadeInDelay), new KeyValue(toastStage.getScene().getRoot().opacityProperty(), 1));
        fadeInTimeline.getKeyFrames().add(fadeInKey);
        fadeInTimeline.setOnFinished((ae)
                -> {
            new Thread(() -> {
                try {
                    Thread.sleep(toastDelay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Timeline fadeOutTimeline = new Timeline();
                KeyFrame fadeOutKey = new KeyFrame(Duration.millis(fadeOutDelay), new KeyValue(toastStage.getScene().getRoot().opacityProperty(), 0));
                fadeOutTimeline.getKeyFrames().add(fadeOutKey);
                fadeOutTimeline.setOnFinished((a) -> toastStage.close());
                fadeOutTimeline.play();
            }).start();
        });
        fadeInTimeline.play();
    }

    public static void exportExecutionOrdersToCsv(List<ExecutionExportData> executionsDataToExport, String userName, Stage actualStage) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV", "*.csv"));
        fileChooser.setInitialFileName("ExecutionOrders_" + userName);
        File file = fileChooser.showSaveDialog(actualStage);
        try {
            bfwrite = new BufferedWriter(
                    new FileWriter(file.getAbsolutePath()));
            bfwrite.append("id;Client;Distance;Category;Type;Date;Postal Address");
            bfwrite.newLine();
            for (ExecutionExportData exportDate : executionsDataToExport) {
                bfwrite.append(exportDate.getId() + ";")
                        .append(exportDate.getClientName() + ";")
                        .append(exportDate.getDistance() + ";")
                        .append(exportDate.getCat() + ";")
                        .append(exportDate.getServiceType() + ";")
                        .append(exportDate.getDate() + ";")
                        .append(exportDate.getPostalAddr().toString());
                bfwrite.newLine();
            }

            FxmlUtils.makeToast(actualStage, "The file was saved in " + file.getAbsolutePath() + "successfully", true);
        } catch (IOException ioe) {
            ioe.getMessage();
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bfwrite.close();
        }

    }

    public static void exportExecutionOrdersToXls(List<ExecutionExportData> executionsDataToExport, String userName, Stage actualStage) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Xls", "*.xlsx"));
        fileChooser.setInitialFileName("ExecutionOrders_" + userName);
        File file = fileChooser.showSaveDialog(actualStage);
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("ExecutionOrders_" + userName);
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        String[] titles = {"id", "Client", "Distance", "Category", "Type", "Date", "Postal Address"};
        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < titles.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(titles[i]);
            cell.setCellStyle(headerCellStyle);
        }
        int rowNum = 1;
        for (ExecutionExportData exportData : executionsDataToExport) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(exportData.getId());
            row.createCell(1).setCellValue(exportData.getClientName());
            row.createCell(2).setCellValue(exportData.getDistance());
            row.createCell(3).setCellValue(exportData.getCat().toString());
            row.createCell(4).setCellValue(exportData.getServiceType());
            row.createCell(5).setCellValue(exportData.getDate().toString());
            row.createCell(6).setCellValue(exportData.getPostalAddr().toString());
        }
        for (int i = 0; i < titles.length; i++) {
            sheet.autoSizeColumn(i);
        }

        FileOutputStream fileToSave = new FileOutputStream(file.getAbsolutePath());
        workbook.write(fileToSave);
        fileToSave.close();
        FxmlUtils.makeToast(actualStage, "The file was saved in " + file.getAbsolutePath() + "successfully", true);
    }

    public static void exportExecutionOrdersToXml(List<ExecutionExportData> executionsDataToExport, String userName, Stage actualStage) throws IOException {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save File");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML", "*.xml"));
            fileChooser.setInitialFileName("ExecutionOrders_" + userName);
            File file = fileChooser.showSaveDialog(actualStage);

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element root = document.createElement("ExecutionOrders");
            document.appendChild(root);
            for (ExecutionExportData exportData : executionsDataToExport) {
                Element id = document.createElement("id");
                Element client = document.createElement("Client");
                Element distance = document.createElement("Distance");
                Element cat = document.createElement("Category");
                Element type = document.createElement("Type");
                Element date = document.createElement("Date");
                Element postalAdd = document.createElement("PostalAddress");
                root.appendChild(id);
                id.setAttribute("id",String.valueOf(exportData.getId()));
                client.appendChild(document.createTextNode(exportData.getClientName()));
                id.appendChild(client);
                distance.appendChild(document.createTextNode(String.valueOf(exportData.getDistance())));
                id.appendChild(distance);
                cat.appendChild(document.createTextNode(exportData.getCat().toString()));
                id.appendChild(cat);
                type.appendChild(document.createTextNode(exportData.getServiceType()));
                id.appendChild(type);
                date.appendChild(document.createTextNode(exportData.getDate().toString()));
                id.appendChild(date);
                postalAdd.appendChild(document.createTextNode(exportData.getPostalAddr().toString()));
                id.appendChild(postalAdd);

            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(file.getAbsolutePath()));
            transformer.transform(domSource, streamResult);
            FxmlUtils.makeToast(actualStage, "The file was saved in " + file.getAbsolutePath() + "successfully", true);
        } catch (ParserConfigurationException | TransformerException ex) {
            Logger.getLogger(FxmlUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
