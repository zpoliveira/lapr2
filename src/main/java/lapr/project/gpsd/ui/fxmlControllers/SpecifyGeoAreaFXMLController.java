/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.ApplicationGPSD;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.controller.SpecifyGeographicalAreaController;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.GeographicalArea;
import lapr.project.gpsd.model.PostalCode;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SpecifyGeoAreaFXMLController implements Initializable {

    @FXML
    private Button btCreateGeoA;
    @FXML
    private Button btShowAllGA;
    @FXML
    private TextField txtDescription;
    @FXML
    private TextField txtRadius;
    @FXML
    private TextField txtTransitCost;
    @FXML
    private ListView<String> listPostalCode;
    @FXML
    private TextField txtPostalCode;
    @FXML
    private Button btSearchPC;

    private AuthController authController;
    private SpecifyGeographicalAreaController geoAreaCTRL;
    private Company company;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.geoAreaCTRL = new SpecifyGeographicalAreaController();
        this.company = ApplicationGPSD.getInstance().getCompany();
        for (PostalCode pc : company.getExternalService().getListPC()) {
            listPostalCode.getItems().add(pc.toString());
        }
    }

    @FXML
    private void actionCreateGA(ActionEvent event) {
        if (txtTransitCost.getText().matches("\\d*")
                && txtRadius.getText().matches("\\d*")) {
            if (this.geoAreaCTRL.newGeographicalArea(txtDescription.getText(),
                    Integer.valueOf(txtTransitCost.getText()),
                    Integer.valueOf(txtRadius.getText()), getPCfromList())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING,
                        "SUCCESS!!", "NEW GEOGRAPHICAL AREA SAVED").
                        showAndWait();
            Stage st = (Stage) listPostalCode.getScene().getWindow();
            st.close();
            } else {
                FxmlUtils.createAlert(Alert.AlertType.WARNING,
                        "ALERT!!", "That Geographical Area already exists!").
                        showAndWait();
            }
        }else {
            FxmlUtils.createAlert(Alert.AlertType.WARNING,
                        "ALERT!!", "Invalid values!").
                        showAndWait();
        }
    }

    @FXML
    private void actionShowAllGA(ActionEvent event) {
        StringBuilder stb = new StringBuilder();
        for (GeographicalArea ga : geoAreaCTRL.getAllGeoAreas()) {
            stb.append(ga.toString()).append("\n");
        }
        Alert al = FxmlUtils.createAlert(Alert.AlertType.INFORMATION, "GeoAreas", stb.toString());
        al.showAndWait();
    }

    @FXML
    private void actionSearchPC(ActionEvent event) {
        // TODO create a filter for the list
    }

    public void associateAuthController(AuthController authController) {
        this.authController = authController;
    }

    private String getPCfromList() {

        String str = listPostalCode.getSelectionModel().getSelectedItem();
        String id = "";
        try {
            id = str.split(",")[0];
        } catch (Exception e) {
            Alert al = FxmlUtils.createAlert(Alert.AlertType.ERROR,
                    "ERROR", e.getMessage());
            al.showAndWait();
        }
        return id;
    }

}
