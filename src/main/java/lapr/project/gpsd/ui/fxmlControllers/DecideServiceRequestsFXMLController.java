/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.ApplicationGPSD;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.controller.DecideServiceRequestsController;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.OtherCost;
import lapr.project.gpsd.model.ServiceRequest;
import lapr.project.gpsd.model.ServiceRequestDescription;
import lapr.project.gpsd.model.registers.ServiceRequestRegister;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class DecideServiceRequestsFXMLController implements Initializable {

    @FXML
    private AnchorPane anchor;

    @FXML
    private Button rejectButton;

    @FXML
    private Button backButton;

    @FXML
    private Button acceptButton;

    @FXML
    private TreeView serviceReqList;

    private DecideServiceRequestsController decideReqController;

    private AuthController authController;

    private Company company;

    private ServiceRequestRegister srr;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.decideReqController = new DecideServiceRequestsController();
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.srr = company.getServiceRequestRegister();

        createTree();
    }

    public void associateAuthController(AuthController authController) {
        this.authController = authController;
    }

    @FXML
    private void cancelServiceRequestDecision(ActionEvent event) {
        Stage stage = (Stage) this.backButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void acceptServiceRequest(ActionEvent event) {
        Stage stage = (Stage) this.backButton.getScene().getWindow();
        try {
            TreeItem<String> idd = (TreeItem<String>) this.serviceReqList.getSelectionModel().getSelectedItem();
            int id = Integer.parseInt(idd.getValue());
            if (this.decideReqController.acceptServiceRequest(srr.getServiceRequestById(id))) {
                FxmlUtils.makeToast(
                        stage,
                        "Service accepted!",
                        true);
            } else {
                FxmlUtils.makeToast(
                        stage,
                        "Service doesn't have service requests associated.",
                        false);
            }
        } catch (Exception e) {
            FxmlUtils.makeToast(stage, "Select a valid service request!",
                    false);
        }

        createTree();
    }

    @FXML
    private void rejectServiceRequest(ActionEvent event) {
        Stage stage = (Stage) this.backButton.getScene().getWindow();
        try {
            TreeItem<String> idd = (TreeItem<String>) this.serviceReqList.getSelectionModel().getSelectedItem();
            int id = Integer.parseInt(idd.getValue());
            this.decideReqController.rejectServiceRequest(srr.getServiceRequestById(id));
        } catch (Exception e) {
            FxmlUtils.makeToast(stage, "Select a valid service request!",
                    false);
        }

        createTree();
    }

    private void createTree() {
        List<ServiceRequest> srs = decideReqController.getUserServiceRequests();
        List<String> ids = new ArrayList<>();
        for (ServiceRequest sr : srs) {
            ids.add(String.valueOf(sr.getID()));
        }

        ObservableList<String> listServices = FXCollections.observableArrayList(ids);

        TreeItem<String> str = new TreeItem<>("Service Request List");

        serviceReqList.setRoot(str);

        for (int i = 0; i < listServices.size(); i++) {
            TreeItem<String> id = new TreeItem<>(listServices.get(i));

            TreeItem<String> date = new TreeItem<>(srs.get(i).getCreationDate().getTime().toString().trim());
            id.getChildren().add(date);

            TreeItem<String> postalAddress = new TreeItem<>(srs.get(i).getPostalAddress().toString().trim());
            id.getChildren().add(postalAddress);

            if (srs.get(i).getServiceRequestDescriptions().isEmpty()) {
                id.getChildren().add(new TreeItem<>("No Service Descriptions"));
            } else {
                TreeItem<String> descs = new TreeItem<>("Service Request Descriptions");
                for (ServiceRequestDescription srd : srs.get(i).getServiceRequestDescriptions()) {
                    TreeItem<String> desc = new TreeItem<>(srd.getDescription());

                    descs.getChildren().add(desc);
                }
                id.getChildren().add(descs);
            }

            if (srs.get(i).getListOtherCost().isEmpty()) {
                id.getChildren().add(new TreeItem<>("No Other costs"));
            } else {
                TreeItem<String> costs = new TreeItem<>("Other Costs");
                for (OtherCost ot : srs.get(i).getListOtherCost()) {
                    TreeItem<String> cost = new TreeItem<>(ot.getDesignation() + " " + ot.getValue());

                    costs.getChildren().add(cost);
                }
                id.getChildren().add(costs);
            }

            serviceReqList.getRoot().getChildren().add(id);

            serviceReqList.getRoot().setExpanded(true);
        }
    }
}
