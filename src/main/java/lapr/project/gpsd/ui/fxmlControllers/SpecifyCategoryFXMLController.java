/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.controller.SpecifyCategoryController;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SpecifyCategoryFXMLController implements Initializable {

    private AuthController authController;
    private SpecifyCategoryController categoryController;

    @FXML
    private Button createCategoryButton;

    @FXML
    private TextField codeTextField;

    @FXML
    private TextArea categoryDescriptionTextArea;

    @FXML
    private Button backButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.categoryController = new SpecifyCategoryController();
    }

    @FXML
    private void createCategory(ActionEvent event) {
        Stage stage = (Stage) this.createCategoryButton.getScene().getWindow();
        if (verifyInputs()) {
            Category c = this.categoryController.newCategory(this.codeTextField.getText(),
                    this.categoryDescriptionTextArea.getText());
            if (!c.equals(null) && FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION,
                    "You really want to create the category?",
                    "It will be saved in our database.").showAndWait().get() == ButtonType.OK) {
                stage.close();
                this.categoryController.registerCategory(c);
                FxmlUtils.makeToast(
                        stage, 
                        "The category " + this.codeTextField.getText() + " was created succefully", 
                        true);
            }else{
                FxmlUtils.makeToast(
                        stage, 
                        "The category " + this.codeTextField.getText() + " was not Created", 
                        false);
                return;
            }
        } else {
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Please Fill all", "All fileds must be filled").show();
        }
    }

    public void associateAuthController(AuthController authController) {
        this.authController = authController;
    }

    @FXML
    private void cancelCategoryCreation(ActionEvent event) {
        if (FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION,
                "Realy want to cancel?",
                "All inserted data will be lost.").showAndWait().get() == ButtonType.OK) {
            Stage stage = (Stage) this.backButton.getScene().getWindow();
            stage.close();
        } else {
            event.consume();
        }
    }

    private boolean verifyInputs() {
        boolean valid = true;
        if (this.codeTextField.getText().isEmpty()
                || this.categoryDescriptionTextArea.getText().isEmpty()) {
            valid = false;
        }
        return valid;
    }
}
