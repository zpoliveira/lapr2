/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.controller.SubmitApplicationController;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;
import lapr.project.gpsd.utils.Utils;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SubmitApplicationConfirmatioFXMLController implements Initializable {

    @FXML
    private BorderPane mainBorderPane;
    @FXML
    private Button submitApplication;
    @FXML
    private Button cancelApplication;
    @FXML
    private TextField SPAddress;
    @FXML
    private TextField SPPostalCode;
    @FXML
    private TextField SPLocal;
    @FXML
    private TextField SPAcademicQualificationDesignation;
    @FXML
    private TextField SPAcademicQualificationTitle;
    @FXML
    private TextField SPAcademicQualificationClassification;
    @FXML
    private TextArea SPProfessionalQualificationDescription;
    @FXML
    private ComboBox<Category> categoriesBox;
    
    private String address, postalCode, local;
    private String academicQualificationDesignation, academicQualificationTitle, academicQualificationClassification;
    private String professionalQualification;
    
    private SubmitApplicationController submitApplicationController;
    private SubmitApplicationConfirmatioFXMLController submitApplicationConfirmatioFXMLController;
    private Company company;
    private Application app;
    private Category cat;
    private AuthController authController;
    private List<Category> catList;
    private boolean academic, professional, categories;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.submitApplicationConfirmatioFXMLController = new SubmitApplicationConfirmatioFXMLController();
        authController = new AuthController();
        catList = this.authController.getApplicationGPSD().getCompany().getCategoryRegister().getCategories();
        for(Category cat : catList){
            categoriesBox.getItems().add(cat);
        }
        academic = false;
        professional = false;
        categories = false;
    }
           
    public void associateController(SubmitApplicationController submitApplicationController){
        this.submitApplicationController = submitApplicationController;
    }
    
    @FXML
    private void addApplication(ActionEvent event) {
        if(validateAllFields()){
        this.submitApplicationController.registerApplication(this.submitApplicationController.getApp());
        FxmlUtils.createAlert(Alert.AlertType.INFORMATION, "Submited to Service Provider", this.submitApplicationController.getApp().toString()).show();
        Stage stage = (Stage) this.submitApplication.getScene().getWindow();
        stage.close();
        }
    }
    
    private boolean validateAllFields(){
        boolean flag = true;
        
        if(!academic){
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Mandatory field not submited", "Please insert Academic Qualification").show();
            flag = false;
        }
        if(!professional){
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Mandatory field not submited", "Please insert Professional Qualification").show();
            flag = false;
        }
        if(!categories){
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Mandatory field not submited", "Please insert Service Category").show();
            flag = false;
        }
        
        return flag;
    }

    @FXML
    private void exit(ActionEvent event) {
        if(FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, 
                "Realy want to cancel?", 
                "All inserted data will be lost.").showAndWait().get() == ButtonType.OK){
            Stage stage = (Stage) this.cancelApplication.getScene().getWindow();
            stage.close();
            
        
        
        }else{
            event.consume();
        };
    }

    @FXML
    private void addAddress(ActionEvent event) {
        address = SPAddress.getText();
    }

    @FXML
    private void addPostalCode(ActionEvent event) {
        
        postalCode = SPPostalCode.getText();
    }

    @FXML
    private void addSPLocal(ActionEvent event) {
        
        local = SPLocal.getText();   
    }

    @FXML
    private void addSPPostalAdress(ActionEvent event) {
        if (this.verifyPostalAddressInputs() && this.verifyPostalCode()) {
            address = SPAddress.getText();
            postalCode = SPPostalCode.getText();
            local = SPLocal.getText();
            this.submitApplicationController.addPostalAddress(address, postalCode, local);
            
            if (FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, "Add new Postal Address?", "Please choose").showAndWait().get() == ButtonType.OK) {
            SPAddress.clear();
            SPPostalCode.clear();
            SPLocal.clear();
                

            }
        } else if (!this.verifyPostalAddressInputs()) {
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Please Fill all", "all fileds must be filled").show();
        }
    }

    private boolean verifyPostalAddressInputs(){
         boolean valid = true;
         if(this.SPAddress.getText().isEmpty() ||
            this.SPPostalCode.getText().isEmpty() ||
            this.SPLocal.getText().isEmpty()){
             valid = false;
         }
         return valid;
    }
    
    private boolean verifyPostalCode(){
         boolean valid = true;          
            
            if (!Utils.postalCodes.containsKey(SPPostalCode.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Postal code field is not valid!").show();
            valid = false;
            }
         
            return valid;

            }

    @FXML
    private void addSPAcademicQualificationDesignation(ActionEvent event) {
        academicQualificationDesignation = SPAcademicQualificationDesignation.getText();
    }

    @FXML
    private void addSPAcademicQualificationTitle(ActionEvent event) {
        academicQualificationTitle = SPAcademicQualificationTitle.getText();
    }

    @FXML
    private void addSPAcademicQualificationClassification(ActionEvent event) {
        academicQualificationClassification = SPAcademicQualificationClassification.getText();
    }

    @FXML
    private void addSPAcademicQualification(ActionEvent event) {
        if(this.verifyAcademicQualificationInputs()){
        academicQualificationDesignation = SPAcademicQualificationDesignation.getText();
        academicQualificationTitle = SPAcademicQualificationTitle.getText();
        academicQualificationClassification = SPAcademicQualificationClassification.getText();
        this.submitApplicationController.addAcademicQual(academicQualificationDesignation, academicQualificationTitle, academicQualificationClassification);
        academic = true;
        if (FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, "Add new Academic Qualification?", "Please choose").showAndWait().get() == ButtonType.OK) {
        SPAcademicQualificationDesignation.clear();
        SPAcademicQualificationTitle.clear();
        SPAcademicQualificationClassification.clear();     
            }
        }else{
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Please Fill all", "all fileds must be filled").show();
        }
    }
    
    private boolean verifyAcademicQualificationInputs(){
         boolean valid = true;
         if(this.SPAcademicQualificationDesignation.getText().isEmpty() ||
            this.SPAcademicQualificationTitle.getText().isEmpty() ||
            this.SPAcademicQualificationClassification.getText().isEmpty()){
             valid = false;
         }
         return valid;
    }

    @FXML
    private void addSPProfessionalQualification(ActionEvent event) {

        if (this.verifyProfessionalQualificationInputs()) {
            professionalQualification = SPProfessionalQualificationDescription.getText();
            professional = true;
            this.submitApplicationController.addProfessionalQual(professionalQualification);
            if (FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, "Add new Professional Qualification?", "Please choose").showAndWait().get() == ButtonType.OK) {
                SPProfessionalQualificationDescription.clear();
            }

        } else {
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Please Fill all", "all fileds must be filled").show();
        }
    }
    
    private boolean verifyProfessionalQualificationInputs(){
         boolean valid = true;
         if(this.SPProfessionalQualificationDescription.getText().isEmpty()){
             valid = false;
         }
         return valid;
    }

    @FXML
    private void addCategory(ActionEvent event) {
        
        if(this.verifyCategoryInputs()){
        cat = categoriesBox.getValue();
        String catID = cat.getCatId();
        this.submitApplicationController.addCategory(catID);
        categories = true;
        FxmlUtils.createAlert(Alert.AlertType.INFORMATION, "Category added", cat.toString()).show();
        }else{
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Please Select one", "At least one category has to be selected").show();
        }
    }

    @FXML
    private void selectCategory(ActionEvent event) {
        
        cat = categoriesBox.getValue();
    }
    
    private boolean verifyCategoryInputs(){
         boolean valid = true;
         if(this.categoriesBox.getValue().toString().isEmpty()){
             valid = false;
         }
         return valid;
    }
    
}
