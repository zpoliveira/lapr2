package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.ApplicationGPSD;
import lapr.project.gpsd.controller.ServiceRequestController;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.FixedService;
import lapr.project.gpsd.model.OtherCost;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.SchedulePreference;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceRequestDescription;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class RequestServiceFXMLController implements Initializable {

    private ServiceRequestController servReqController;
    private Company company;
    private final ToggleGroup groupMinutes = new ToggleGroup();
    private ObservableList<Service> lstServices;
    private ObservableList<ServiceRequestDescription> lstServReqDescription;
    private ObservableList<SchedulePreference> lstSchedules;
    private ObservableList<OtherCost> lstOtherCost;
    private String userEmail;

    @FXML
    private GridPane areaAfterAddress;
    @FXML
    private AnchorPane areaCatServ;
    @FXML
    private VBox areaAfterServ;
    @FXML
    private AnchorPane areaRequestDetail;
    @FXML
    private DatePicker dateSelection;
    @FXML
    private VBox radio30Minute;
    @FXML
    private RadioButton radioDurZero;
    @FXML
    private RadioButton radioDur30;
    @FXML
    private ComboBox<PostalAddress> cbAddressList;
    @FXML
    private ComboBox<Category> cbCategory;
    @FXML
    private ComboBox<String> cbDurationHour;
    @FXML
    private ComboBox<String> cbTimeHour;
    @FXML
    private ComboBox<String> cbTimeMinute;
    @FXML
    private Button btNewReq;
    @FXML
    private Button btAddSchedule;
    @FXML
    private Button btnAddDescription;
    @FXML
    private Button btSaveClose;
    @FXML
    private TextArea txtRequestDescription;
    @FXML
    private TableView<Service> tableService;
    @FXML
    private TableColumn<Service, String> colServID;
    @FXML
    private TableColumn<Service, String> colServName;
    @FXML
    private TableColumn<Service, String> colServType;
    @FXML
    private TableColumn<Service, String> colCostHout;
    @FXML
    private TableView<OtherCost> tableOtherCost;
    @FXML
    private TableColumn<OtherCost, String> colOtherDesig;
    @FXML
    private TableColumn<OtherCost, String> colOtherValue;
    @FXML
    private TableView<SchedulePreference> tableSchedPref;
    @FXML
    private TableColumn<SchedulePreference, String> colSchedDate;
    @FXML
    private TableColumn<SchedulePreference, String> colSchedTime;
    @FXML
    private TableView<ServiceRequestDescription> tableServReqDescription;
    @FXML
    private TableColumn<ServiceRequestDescription, String> colSerReqDuration;
    @FXML
    private TableColumn<ServiceRequestDescription, String> colSerReqServ;
    @FXML
    private TableColumn<ServiceRequestDescription, String> colSerReqDesc;
    @FXML
    private TableColumn<ServiceRequestDescription, String> colSerReqCost;
    @FXML
    private HBox areaDuration;


    @Override
    public void initialize(URL url, ResourceBundle rb) {

        this.servReqController = new ServiceRequestController();
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.userEmail = company.getAuthFacade().getCurrentSession().getUserEmail();

        radioDur30.setToggleGroup(groupMinutes);
        radioDurZero.setToggleGroup(groupMinutes);

        //POPULATES COMBOBOXES
        cbAddressList.getItems().addAll(
                company.getClientRegister().getClientByEmail(userEmail)
                        .getAddresses()
        );

        for (int i = 0; i < 17; i++) {
            cbDurationHour.getItems().add(String.valueOf(i));
        }
        cbDurationHour.setValue("0");
        for (int i = 6; i < 24; i++) {
            cbTimeHour.getItems().add(String.format("%02d", i));
        }
        cbTimeHour.setValue("06");
        for (int i = 0; i < 60; i++) {
            cbTimeMinute.getItems().add(String.format("%02d", i));
        }
        cbTimeMinute.setValue("00");

        cbCategory.getItems().clear();
        cbCategory.getItems().addAll(company.getCategoryRegister().getCategories());

        tableSetup();
    }

    @FXML
    private void clickNewRequest(ActionEvent event) {

        if (cbAddressList.getValue() instanceof PostalAddress) {
            this.servReqController.newServiceRequest(
                    this.userEmail,
                    this.cbAddressList.getValue());
            areaAfterAddress.setDisable(false);
        }
    }

    @FXML
    private void clickAddDescription(ActionEvent event) {

        if (tableService.getSelectionModel().isEmpty()
                || tableService.getSelectionModel().getSelectedItem() == null) {
            Alert al = FxmlUtils.createAlert(Alert.AlertType.ERROR,
                    "No Service", "Please select a service to add!");
            al.showAndWait();
        } else {
            Service serv = tableService.getSelectionModel().getSelectedItem();
            String description = txtRequestDescription.getText();
            int duration = getDuration();
            servReqController.addDescription(serv, description, duration);
            this.servReqController.calculateCost();
            
            this.lstServReqDescription = FXCollections.observableArrayList(servReqController.getDescriptionList());
            this.tableServReqDescription.setItems(lstServReqDescription);
            this.lstOtherCost = FXCollections.observableArrayList(servReqController.getOtherCostList());
            this.tableOtherCost.setItems(lstOtherCost);
        }
    }

    @FXML
    private void clickAddSchedule(ActionEvent event) {

        if (dateSelection.getValue() == null) {
            Alert al = FxmlUtils.createAlert(Alert.AlertType.ERROR,
                    "No Date", "Please select a date of your preference!");
            al.showAndWait();
        } else {
            LocalDate selDate = dateSelection.getValue();
            LocalDateTime dateTime = LocalDateTime.of(
                    selDate,
                    LocalTime.of(
                            Integer.valueOf(cbTimeHour.getValue()),
                            Integer.valueOf(cbTimeMinute.getValue())));

            System.out.println(dateTime.toString());

            servReqController.addSchedulePreference(dateTime);
            this.lstSchedules = FXCollections.observableArrayList(servReqController.getListSchedules());
            this.tableSchedPref.setItems(lstSchedules);
        }
    }

    @FXML
    private void clickSaveAndClose(ActionEvent event) {
        if (FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, "Saving...",
                "This will Save this request and close."
                + "\n Do you wish to continue?")
                .showAndWait().get() == ButtonType.OK) {
            if (this.servReqController.registerServiceRequest()) {
                Stage st = (Stage) areaAfterAddress.getScene().getWindow();
                st.close();
            } else {
                Alert al = FxmlUtils.createAlert(Alert.AlertType.ERROR,
                        "Not a valid request", "Please fill all the data!");
                al.showAndWait();
            }
        }
    }

    @FXML
    private void onTableClick(MouseEvent event) {
        if (tableService.getSelectionModel().getSelectedItem() instanceof FixedService) {
            areaDuration.setDisable(true);
        } else {
            areaDuration.setDisable(false);
        }
    }

    @FXML
    private void onDurationSelect(ActionEvent event) {
        switch (Integer.valueOf(cbDurationHour.getValue())) {
            case 0:
                radioDurZero.disableProperty().set(true);
                radioDur30.setSelected(true);
                radioDur30.disableProperty().set(false);
                break;
            case 16:
                radioDurZero.disableProperty().set(false);
                radioDur30.disableProperty().set(true);
                radioDurZero.setSelected(true);
                break;
            default:
                radioDurZero.disableProperty().set(false);
                radioDur30.disableProperty().set(false);
        }
    }

    @FXML
    private void onCategorySelected(ActionEvent event) {

        Category cat = cbCategory.getValue();
        this.lstServices = FXCollections.observableArrayList(company.getServiceRegister().getServiceListByCategory(cat));
        this.tableService.setItems(lstServices);
    }

    private int getDuration() {
        int durRes = 0;
        Service testServ = tableService.getSelectionModel().getSelectedItem();
        if (testServ instanceof FixedService) {
            durRes = ((FixedService) testServ).getDuration();
        } else {
            durRes = Integer.valueOf(cbDurationHour.getValue()) * 60
                    + (groupMinutes.getSelectedToggle() == radioDurZero ? 0 : 30);
        }
        return durRes;
    }

    private static Calendar localDateTimeToDate(LocalDateTime localDateTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(localDateTime.getYear(), localDateTime.getMonthValue() - 1, localDateTime.getDayOfMonth(),
                localDateTime.getHour(), localDateTime.getMinute(), localDateTime.getSecond());
        return calendar;
    }

    private void tableSetup() {
        //Create columns to Services tableview
        this.colServID.setCellValueFactory(cellData
                -> new SimpleStringProperty(cellData.getValue().getId()));
        this.colServName.setCellValueFactory(cellData
                -> new SimpleStringProperty(cellData.getValue().getLittleDescription()));
        this.colServType.setCellValueFactory(cellData
                -> new SimpleStringProperty(cellData.getValue().getTypeName()));
        this.colCostHout.setCellValueFactory(cellData
                -> new SimpleStringProperty(String.valueOf(cellData.getValue().getCostHour())));
        this.tableService.setItems(lstServices);

        //Create columns to Schedules tableview
        this.colSchedDate.setCellValueFactory(cellData
                -> new SimpleStringProperty(cellData.getValue().getDate()));
        this.colSchedTime.setCellValueFactory(cellData
                -> new SimpleStringProperty(cellData.getValue().getTime()));
        this.tableSchedPref.setItems(lstSchedules);

        //Create columns to OtherCost tableview
        this.colOtherDesig.setCellValueFactory(cellData
                -> new SimpleStringProperty(cellData.getValue().getDesignation()));
        this.colOtherValue.setCellValueFactory(cellData
                -> new SimpleStringProperty(String.valueOf(cellData.getValue().getValue())));
        this.tableOtherCost.setItems(lstOtherCost);

        //Create columns to ServiceRequestDescription tableview
        this.colSerReqServ.setCellValueFactory(cellData
                -> new SimpleStringProperty(cellData.getValue().getService().getLittleDescription()));
        this.colSerReqDesc.setCellValueFactory(cellData
                -> new SimpleStringProperty(cellData.getValue().getDescription()));
        this.colSerReqDuration.setCellValueFactory(cellData
                -> new SimpleStringProperty(String.valueOf(cellData.getValue().getDuration())));
        this.colSerReqCost.setCellValueFactory(cellData
                -> new SimpleStringProperty(String.valueOf(cellData.getValue().getCost())));
        this.tableServReqDescription.setItems(lstServReqDescription);

        this.tableService.setItems(lstServices);
        this.tableOtherCost.setItems(lstOtherCost);
        this.tableSchedPref.setItems(lstSchedules);
        this.tableServReqDescription.setItems(lstServReqDescription);
    }

}
