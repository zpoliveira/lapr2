/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.ApplicationGPSD;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.controller.EvaluateServiceProviderController;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.Rating;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class EvaluateServiceProviderFXMLController implements Initializable {

    @FXML
    private Button backButton;

    @FXML
    private ListView spList;

    @FXML
    private Slider slider;

    @FXML
    private Button evaluateButton;

    @FXML
    private Label spLabel;

    @FXML
    private Label spMean;

    @FXML
    private Label spStDev;

    @FXML
    private Label spMeanDev;

    @FXML
    private Label stDevAll;

    @FXML
    private Label generalMeanRating;

    @FXML
    private RadioButton out;

    @FXML
    private RadioButton worst;

    @FXML
    private Label meanDevAll;

    @FXML
    private RadioButton reg;

    @FXML
    private Label absDev;

    @FXML
    private BarChart histRats;

    private EvaluateServiceProviderController evaluateServiceProviderController;

    private AuthController authController;

    private Company company;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.evaluateServiceProviderController = new EvaluateServiceProviderController();
        this.company = ApplicationGPSD.getInstance().getCompany();
        List<String> sps = new ArrayList<>();
        double meanSum = 0;
        int nSP = 0;

        for (ServiceProvider sp : this.evaluateServiceProviderController.getServiceProviderList()) {
            sps.add(sp.getShortName() + " " + sp.getEmail());
            meanSum += sp.getRatingMean();
            nSP++;
        }
        int totRats = 0;
        for (int i = 0; i < sps.size(); i++) {
            List<Rating> rats = this.evaluateServiceProviderController.getServiceProviderList().get(i).getRatings();
            for (int j = 0; j < rats.size(); j++) {
                totRats++;
            }
        }

        double[] allRats = new double[totRats];
        int k = 0;
        for (int i = 0; i < sps.size(); i++) {
            List<Rating> rats = this.evaluateServiceProviderController.getServiceProviderList().get(i).getRatings();
            if (rats.size() > 0) {
                for (int j = 0; j < rats.size(); j++) {
                    Rating r = rats.get(j);
                    allRats[k] = Double.valueOf(r.getRate());
                    k++;
                }
            }
        }

        //mean all
        double meanAll = round(meanSum / nSP, 3);
        generalMeanRating.setText(String.valueOf(meanAll));

        //standard deviation all
        double stDev = round(calculateSD(allRats), 3);
        stDevAll.setText(String.valueOf(stDev));

        //mean deviation all
        double meanDev = round(calculateMeanDeviation(allRats), 3);
        meanDevAll.setText(String.valueOf(meanDev));

        ObservableList<String> listServices = FXCollections.observableArrayList(sps);

        spList.setItems(listServices);

        spList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                Stage stage = (Stage) backButton.getScene().getWindow();
                String[] select = spList.getSelectionModel().getSelectedItem().toString().trim().split(" ");
                String email = select[select.length - 1];
                ServiceProvider sp = evaluateServiceProviderController.getServiceProvider(email);

                int[] histRatsDist = {0, 0, 0, 0, 0};

                //mean SP
                double meanSP = round(sp.getRatingMean(), 3);
                spMean.setText(String.valueOf(meanSP));

                //absolute deviation
                double absDeve = round(Math.abs(meanSP - meanAll), 3);
                absDev.setText(String.valueOf(absDeve));

                //Standard deviation SP
                int totRats = 0;

                List<Rating> rats = sp.getRatings();
                for (int j = 0; j < rats.size(); j++) {
                    totRats++;
                }

                double[] allRats2 = new double[totRats];
                int k = 0;

                List<Rating> rats2 = sp.getRatings();

                for (int j = 0; j < rats2.size(); j++) {
                    Rating r = rats2.get(j);
                    allRats2[k] = Double.valueOf(r.getRate());
                    k++;
                }

                double stDev = round(calculateSD(allRats2), 3);
                spStDev.setText(String.valueOf(stDev));

                //mean deviation SP
                double meanDev = round(calculateMeanDeviation(allRats2), 3);
                spMeanDev.setText(String.valueOf(meanDev));

                //label SP
                double worst = meanAll - stDev;
                double best = meanAll + stDev;
                if (sp.getLabel() == null) {
                    if (meanSP < worst) {
                        sp.setLabel("Worst Providers");
                        spLabel.setText(sp.getLabel());
                    } else if (meanSP > worst && meanSP < best) {
                        sp.setLabel("Regular Providers");
                        spLabel.setText(sp.getLabel());
                    } else if (meanSP > best) {
                        sp.setLabel("Outstanding Providers");
                        spLabel.setText(sp.getLabel());
                    }
                }else{
                    spLabel.setText(sp.getLabel());
                }

                //histogram of ratings
                NumberAxis number = new NumberAxis();
                CategoryAxis cat = new CategoryAxis();
                BarChart<String, Number> hist = new BarChart<>(cat, number);
                cat.setLabel("Ratings");
                number.setLabel("Occurence");

                for (Rating r : sp.getRatings()) {
                    switch (r.getRate()) {
                        case 1:
                            histRatsDist[0]++;
                            break;
                        case 2:
                            histRatsDist[1]++;
                            break;
                        case 3:
                            histRatsDist[2]++;
                            break;
                        case 4:
                            histRatsDist[3]++;
                            break;
                        case 5:
                            histRatsDist[4]++;
                            break;
                    }
                }

                XYChart.Series series = new XYChart.Series();
                series.getData().add(new XYChart.Data("1", histRatsDist[0]));
                series.getData().add(new XYChart.Data("2", histRatsDist[1]));
                series.getData().add(new XYChart.Data("3", histRatsDist[2]));
                series.getData().add(new XYChart.Data("4", histRatsDist[3]));
                series.getData().add(new XYChart.Data("5", histRatsDist[4]));

                hist.getData().setAll(series);
                histRats.getData().clear();
                histRats.getData().setAll(hist.getData());

            }
        });
    }

    public void associateAuthController(AuthController authController) {
        this.authController = authController;
    }

    @FXML
    private void goBack(ActionEvent event) {
        if (FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION,
                "Realy want to cancel?",
                "All inserted data will be lost.").showAndWait().get() == ButtonType.OK) {
            Stage stage = (Stage) this.backButton.getScene().getWindow();
            stage.close();
        } else {
            event.consume();
        }
    }

    @FXML
    private void evaluate(ActionEvent event) {
        Stage stage = (Stage) this.backButton.getScene().getWindow();
        String[] select = spList.getSelectionModel().getSelectedItem().toString().trim().split(" ");
        String email = select[select.length - 1];
        ServiceProvider sp = evaluateServiceProviderController.getServiceProvider(email);

        if (!worst.isSelected() && !reg.isSelected() && !out.isSelected()) {
            FxmlUtils.makeToast(stage, "If you want to evaluate you need to select a label!",
                    false);
        } else if (worst.isSelected()) {
            sp.setLabel("Worst Providers");
            FxmlUtils.makeToast(stage, "Evaluated with success.",
                    true);
        } else if (reg.isSelected()) {
            sp.setLabel("Regular Providers");
            FxmlUtils.makeToast(stage, "Evaluated with success.",
                    true);
        } else if (out.isSelected()) {
            sp.setLabel("Outstanding Providers");
            FxmlUtils.makeToast(stage, "Evaluated with success.",
                    true);
        } else {
            FxmlUtils.makeToast(stage, "Error on evaluation.",
                    false);
        }

    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static double calculateSD(double numArray[]) {
        double sum = 0.0, standardDeviation = 0.0;
        int length = numArray.length;

        for (double num : numArray) {
            sum += num;
        }
        double mean = sum / length;
        for (double num : numArray) {
            standardDeviation += Math.pow(num - mean, 2);
        }
        return Math.sqrt(standardDeviation / (length - 1));
    }

    public static double calculateMeanDeviation(double[] nums) {
        int sum = 0;
        int length = nums.length;
        for (double num : nums) {
            sum += num;
        }
        if (length > 0) {
            double mean = sum / length;

            double sumMeanDifs = 0;
            for (double num : nums) {
                double dif = Math.abs(num - mean);
                sumMeanDifs += dif;
            }
            return sumMeanDifs / length;
        } else {
            return 0;
        }
    }

}
