/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.controller.SubmitApplicationController;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;
import lapr.project.gpsd.utils.Utils;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SubmitApplicationFXMLController implements Initializable {

    @FXML
    private AnchorPane basePane;
    @FXML
    private TextField sPFullName;
    @FXML
    private TextField sPVATNumber;
    @FXML
    private TextField sPPhoneNumber;
    @FXML
    private TextField sPeMail;
    @FXML
    private TextField sPAddress;
    @FXML
    private TextField sPPostalCode;
    @FXML
    private TextField sPLocal;
    @FXML
    private Button submitDetails;
    @FXML
    private Button cancelApplication;

    private String name, email, vat, phoneNumber;

    private String address, postalCode, local;

    private AuthController authController;
    private SubmitApplicationController submitApplicationController;
    private Company company;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.submitApplicationController = new SubmitApplicationController();
    }

    public void associateAuthController(AuthController authController) {
        this.authController = authController;
    }

    @FXML
    private void exit(ActionEvent event) {

        if (FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION,
                "Realy want to cancel?",
                "All inserted data will be lost.").showAndWait().get() == ButtonType.OK) {
            Pane pane = FxmlUtils.createNewBorderPane("/fxml/startFXML.fxml");
            basePane.getScene().setRoot(pane);
        } else {
            event.consume();
        };
    }

    @FXML
    private void next(ActionEvent event) {

        if (this.verifyInputs() && this.verifyValidInputs()) {
            name = sPFullName.getText();
            vat = sPVATNumber.getText();
            phoneNumber = sPPhoneNumber.getText();
            email = sPeMail.getText();
            address = sPAddress.getText();
            postalCode = sPPostalCode.getText();
            local = sPLocal.getText();

            this.submitApplicationController.newApplication(name, phoneNumber, phoneNumber, email, address, postalCode, local);

            FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getResource("/fxml/submitApplicationConfirmatioFXML.fxml"));
            try {
                Pane pane = loader.load();
                SubmitApplicationConfirmatioFXMLController submitApplicationConfirmatioFXMLController = loader.getController();
                submitApplicationConfirmatioFXMLController.associateController(submitApplicationController);
                FxmlUtils.newWindow("Insert Details", pane);

            } catch (IOException ex) {
                Logger.getLogger(AfterLoginController.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else if (!this.verifyInputs()) {
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Please Fill all", "all fileds must be filled").show();
        }
    }

    private boolean verifyInputs() {
        boolean valid = true;
        if (this.sPFullName.getText().isEmpty()
                || this.sPVATNumber.getText().isEmpty()
                || this.sPPhoneNumber.getText().isEmpty()
                || this.sPeMail.getText().isEmpty()
                || this.sPAddress.getText().isEmpty()
                || this.sPPostalCode.getText().isEmpty()
                || this.sPLocal.getText().isEmpty()) {
            valid = false;
        }

        return valid;

    }

    private boolean verifyValidInputs() {
        boolean valid = true;

        if (!FxmlUtils.isEmail(this.sPeMail.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Email field is not valid!").show();
            valid = false;
        }

        if (!FxmlUtils.isVatNumber(this.sPVATNumber.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "VAT Number field is not valid!").show();
            valid = false;
        }

        if (!FxmlUtils.isPhoneNumber(this.sPPhoneNumber.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Phone Number field is not valid!").show();
            valid = false;
        }

        if (!Utils.postalCodes.containsKey(sPPostalCode.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Postal code field is not valid!").show();
            valid = false;
        }

        return valid;

    }
}
