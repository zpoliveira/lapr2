/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.controller.ClientRegisterController;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ClientRegisterControllerFXML implements Initializable {

    private ClientRegisterController controller;
    
    private AuthController auth;

    @FXML
    private TextField txtName;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtVATNumber;
    @FXML
    private TextField txtPhone;
    @FXML
    private Button btnDeleteAddress;
    @FXML
    private Button btnAddAddress;
    @FXML
    private TableView<PostalAddress> tblAddresses;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private TextField txtAddress;
    @FXML
    private TextField txtPostalCode;
    @FXML
    private TextField txtLocal;
    @FXML
    private TableColumn<PostalAddress, String> colAddress;
    @FXML
    private TableColumn<PostalAddress, String> colLocal;

    private ObservableList<PostalAddress> lstAddresses;
    @FXML
    private TableColumn<PostalAddress, String> colPostalCode;
    @FXML
    private AnchorPane basePane;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private Button btnClearAddress;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        //Create collumns to tableview
        colAddress.setCellValueFactory(new PropertyValueFactory<>("address"));
        colPostalCode.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
        colLocal.setCellValueFactory(new PropertyValueFactory<>("place"));
         
        lstAddresses = FXCollections.observableArrayList();
        tblAddresses.setItems(lstAddresses);
        controller = new ClientRegisterController();
    }

    /**
     * *
     * validates the form data required to create Client
     *
     * @return
     */
    private boolean validateDataForm() {
        if (txtName.getText().isEmpty()) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Name field is empty!").show();
            return false;
        }
        if (!FxmlUtils.isEmail(txtEmail.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Email field is not valid!").show();
            return false;
        }
        if(txtPassword.getText().isEmpty()){
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Password field is empty!").show();
            return false;
        }
        if (!FxmlUtils.isVatNumber(txtVATNumber.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "VAT Number field is not valid!").show();
            return false;
        }
        if (!FxmlUtils.isPhoneNumber(txtPhone.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Phone Number field is not valid!").show();
            return false;
        }
        if (tblAddresses.getItems().size() < 1) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Add at list one address!").show();
            return false;
        }

        return true;
    }

    private boolean validateAddress() {
        if (txtAddress.getText().isEmpty()) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Address field is empty!").show();
            return false;
        }
        if (!FxmlUtils.isPostalCode(txtPostalCode.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Postal Code field is not valid!").show();
            return false;
        }
        if (txtLocal.getText().isEmpty()) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Local field is empty!").show();
            return false;
        }

        return true;
    }

    @FXML
    private void onDeleteAddress(ActionEvent event) {
        ObservableList<PostalAddress> postalAddressSelected, allPostalAddresses;
        allPostalAddresses = tblAddresses.getItems();
        postalAddressSelected = tblAddresses.getSelectionModel().getSelectedItems();

        postalAddressSelected.forEach(allPostalAddresses::remove);
    }

    @FXML
    private void onAddAddress(ActionEvent event) {
        if (validateAddress()) {
            PostalAddress pa = new PostalAddress(txtAddress.getText(), txtPostalCode.getText(), txtLocal.getText());
           if(!tblAddresses.getItems().stream().anyMatch(x -> x.equals(pa))){
               tblAddresses.getItems().add(pa);
            clearPostalAddress();
           }else{
               FxmlUtils.createAlert(Alert.AlertType.INFORMATION, "Information", "Repeated Postal Address.");
           }
            
        }

    }

    @FXML
    private void onSave(ActionEvent event) throws Exception {
        if(validateDataForm()){
            try {
               if( this.controller.newClient(txtName.getText(), txtVATNumber.getText(), txtPhone.getText(), txtEmail.getText(), txtPassword.getText(), tblAddresses.getItems())){

                  Alert alr = FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, "Information", "Save data?");
                                    if( alr.showAndWait().get() == ButtonType.OK){
                                        if(this.controller.createCliente()){
                                           Stage stage = (Stage) this.btnCancel.getScene().getWindow();
                                            FxmlUtils.makeToast(stage, "Client Registed!", true);
                                            stage.close();
                                        }else{
                                             FxmlUtils.makeToast((Stage) this.btnCancel.getScene().getWindow(), "Error on Client Regist!", false);
                                        }
                                    }                           
                
               }else{
                   FxmlUtils.createAlert(Alert.AlertType.ERROR, "Information", "The data could not be saved.").show();
               }
            } catch (Exception e) {
                FxmlUtils.createAlert(Alert.AlertType.ERROR, "Information", e.getMessage()).show();
            }
           
        }
    }

    @FXML
    private void onCancel(ActionEvent event) {
        if (FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION,
                "Realy want to cancel?",
                "All inserted data will be lost.").showAndWait().get() == ButtonType.OK) {
            Stage stage = (Stage) this.btnCancel.getScene().getWindow();
            stage.close();
        } else {
            event.consume();
        }
    }

    private void clearPostalAddress() {
        txtAddress.clear();
        txtLocal.clear();
        txtPostalCode.clear();
    }

    @FXML
    private void onClearAddress(ActionEvent event) {
        clearPostalAddress();
    }
}
