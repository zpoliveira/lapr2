/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.ConsultExecutionOrdersController;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ExecutionExportData;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ConsultExecutionOrdersControllerFxml implements Initializable {

    @FXML
    private TableView<ExecutionExportData> tableExecutionOrders;
    @FXML
    private TableColumn<ExecutionExportData, Integer> colid;
    @FXML
    private TableColumn<ExecutionExportData, String> colClient;
    @FXML
    private TableColumn<ExecutionExportData, PostalAddress> colPostalAddr;
    @FXML
    private TableColumn<ExecutionExportData, Double> colDistance;
    @FXML
    private TableColumn<ExecutionExportData, Category> colCateg;
    @FXML
    private TableColumn<ExecutionExportData, String> colType;
    @FXML
    private TableColumn<ExecutionExportData, LocalDateTime> colDate;
    @FXML
    private ComboBox<String> dropFormat;

    private ConsultExecutionOrdersController consultExecOrdersContr;
    private ObservableList<ExecutionExportData> execOrdersData;

    private String spName;
    Company company;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.consultExecOrdersContr = new ConsultExecutionOrdersController();
        this.execOrdersData = FXCollections.observableArrayList(this.consultExecOrdersContr.executionsDataToExport);
        this.colid.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.colClient.setCellValueFactory(new PropertyValueFactory<>("clientName"));
        this.colPostalAddr.setCellValueFactory(new PropertyValueFactory<>("postalAddr"));
        this.colDistance.setCellValueFactory(new PropertyValueFactory<>("distance"));
        this.colCateg.setCellValueFactory(new PropertyValueFactory<>("cat"));
        this.colType.setCellValueFactory(new PropertyValueFactory<>("serviceType"));
        this.colDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        this.tableExecutionOrders.setItems(this.execOrdersData);
        String[] fileFormat = {"CSV", "XML", "XLS"};
        this.dropFormat.getItems().setAll(fileFormat);
        this.dropFormat.getSelectionModel().selectFirst();
        this.spName = this.consultExecOrdersContr.getCompany().getAuthFacade().getCurrentSession().getUserName();
    }

    @FXML
    private void cancel(ActionEvent event) {
        Stage stage = (Stage) this.dropFormat.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void exportDataToFile(ActionEvent event) {
        Stage actualStage = (Stage) this.dropFormat.getScene().getWindow();
        String fileFormat = this.dropFormat.getValue();
        switch (fileFormat) {
            case "CSV":
                try {
                    FxmlUtils.exportExecutionOrdersToCsv(this.consultExecOrdersContr.executionsDataToExport, this.spName, actualStage);
                } catch (IOException ex) {
                    Logger.getLogger(ConsultExecutionOrdersControllerFxml.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "XLS":
                try {
                    FxmlUtils.exportExecutionOrdersToXls(this.consultExecOrdersContr.executionsDataToExport, this.spName, actualStage);
                } catch (IOException ex) {
                    Logger.getLogger(ConsultExecutionOrdersControllerFxml.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "XML":
                try {
                    FxmlUtils.exportExecutionOrdersToXml(this.consultExecOrdersContr.executionsDataToExport, this.spName, actualStage);
                } catch (IOException ex) {
                    Logger.getLogger(ConsultExecutionOrdersControllerFxml.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }
    }
}
