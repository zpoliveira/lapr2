/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.ConsultExecutionOrdersController;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.ExecutionExportData;
import lapr.project.gpsd.model.ExecutionOrder;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.registers.ExecutionOrdersRegister;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ReportEndWorkFxmlController implements Initializable {

    @FXML
    private TableView<ExecutionExportData> tableReportEnd;
    @FXML
    private TableColumn<ExecutionExportData, Integer> colid;
    @FXML
    private TableColumn<ExecutionExportData, String> colClient;
    @FXML
    private TableColumn<ExecutionExportData, PostalAddress> colPostalAddr;
    @FXML
    private TableColumn<ExecutionExportData, Double> colDistance;
    @FXML
    private TableColumn<ExecutionExportData, Category> colCateg;
    @FXML
    private TableColumn<ExecutionExportData, String> colType;
    @FXML
    private TableColumn<ExecutionExportData, LocalDateTime> colDate;
    @FXML
    private TableColumn<ExecutionExportData, Boolean> colSetEnd;
    @FXML
    private TableColumn<ExecutionExportData, String> colSetIssue;
    
    private ConsultExecutionOrdersController consultExecOrdersContr;
    private ObservableList<ExecutionExportData> execOrdersData;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.consultExecOrdersContr = new ConsultExecutionOrdersController();
        this.execOrdersData = FXCollections.observableArrayList(this.consultExecOrdersContr.executionsDataToExport);
        this.colid.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.colClient.setCellValueFactory(new PropertyValueFactory<>("clientName"));
        this.colPostalAddr.setCellValueFactory(new PropertyValueFactory<>("postalAddr"));
        this.colDistance.setCellValueFactory(new PropertyValueFactory<>("distance"));
        this.colCateg.setCellValueFactory(new PropertyValueFactory<>("cat"));
        this.colType.setCellValueFactory(new PropertyValueFactory<>("serviceType"));
        this.colDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        this.colSetEnd.setCellValueFactory(new PropertyValueFactory<>("isEnded"));
        this.colSetEnd.setCellFactory(CheckBoxTableCell.forTableColumn(this.colSetEnd));
        this.colSetIssue.setCellValueFactory(new PropertyValueFactory<>("hasIssue"));
        this.colSetIssue.setCellFactory(TextFieldTableCell.forTableColumn());
        this.colSetIssue.setOnEditCommit((TableColumn.CellEditEvent<ExecutionExportData, String> issue) ->
                    ( issue.getTableView().getItems().get(
                            issue.getTablePosition().getRow())
                    ).setHasIssue(issue.getNewValue()));
        this.tableReportEnd.setItems(this.execOrdersData);
        
    }    

    @FXML
    private void cancel(ActionEvent event) {
        if(FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, 
                "Realy want to exit?", 
                "If you didn't save all inserted data will be lost.").showAndWait().get() == ButtonType.OK){
            Stage stage = (Stage) this.tableReportEnd.getScene().getWindow();
            stage.close();
        }else{
            event.consume();
        }
    }

    @FXML
    private void saveEndWork(ActionEvent event) {
        ExecutionOrdersRegister execOrdRegit = this.consultExecOrdersContr.getCompany().getExecutionOrdersRegister();
        ExecutionOrder execOr;
        for(ExecutionExportData expData : tableReportEnd.getItems()){
            if(expData.isIsEnded()){
                execOr = execOrdRegit.getExecutionOrderById(expData.getId());
                execOr.setIsEnded(true);
            }
            if(expData.getHasIssue() != null){
                execOr = execOrdRegit.getExecutionOrderById(expData.getId());
                execOr.setHasIssue(expData.getHasIssue());
            }
        }
        Stage stage = (Stage) this.tableReportEnd.getScene().getWindow();
        FxmlUtils.makeToast(stage, "Data saved", true);
    }    
}
