/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.RegistServiceProviderController;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.GeographicalArea;
import lapr.project.gpsd.model.PostalCode;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;
import lapr.project.gpsd.utils.Constants;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ServiceProviderResgisterFxmlController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private RegistServiceProviderController registSPCont;
    @FXML
    private TextField numMec;
    @FXML
    private TextField spEmail;
    @FXML
    private PasswordField spPass;
    @FXML
    private TextField spVATNumber;
    @FXML
    private TableView<Category> spCatListTable;
    @FXML
    private TableColumn<Category, String> idCat;
    @FXML
    private TableColumn<Category, String> descCat;
    @FXML
    private ComboBox<Category> catList;
    ;
    @FXML
    private TableView<GeographicalArea> spGeoListTable;
    @FXML
    private TableColumn<GeographicalArea, PostalCode> postalCodeGeo;
    @FXML
    private TableColumn<GeographicalArea, String> descGeo;
    @FXML
    private ComboBox<GeographicalArea> geoList;
    @FXML
    private TextField spCompleteName;
    @FXML
    private TextField spShortName;
    @FXML
    private TextField spPostalCode;

    private ObservableList<Category> lstCat;
    private ObservableList<GeographicalArea> lstGeo;



    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.registSPCont = new RegistServiceProviderController();

        idCat.setCellValueFactory(new PropertyValueFactory<>("code"));
        descCat.setCellValueFactory(new PropertyValueFactory<>("description"));
        postalCodeGeo.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
        descGeo.setCellValueFactory(new PropertyValueFactory<>("designation"));
        lstCat = FXCollections.observableArrayList();
        lstGeo = FXCollections.observableArrayList();
        spCatListTable.setItems(lstCat);
        spGeoListTable.setItems(lstGeo);
        this.catList.getItems().addAll(this.registSPCont.getCategoryRegister().getCategories());
        this.geoList.getItems().addAll(this.registSPCont.getGeoAreaRegister().getListGeoAreas());
    }

    @FXML
    private void addCatToSp(ActionEvent event) {
        if(this.catList.getValue() != null && !this.lstCat.contains(this.catList.getValue())){
            this.lstCat.add(this.catList.getValue());
        }
    }

    @FXML
    private void addGeoToSp(ActionEvent event) {
        if(this.geoList.getValue() != null && !this.lstGeo.contains(this.geoList.getValue())){
            this.lstGeo.add(this.geoList.getValue());
        }
    }

    @FXML
    private void registSp(ActionEvent event) {
        Stage actualStage = (Stage) this.numMec.getScene().getWindow();
        
        if(this.verifyInputs()){
            int numMec = Integer.valueOf(this.numMec.getText());
            String completName = this.spCompleteName.getText();
            String shortName = this.spShortName.getText();
            String email = this.spEmail.getText();
            String vatNum = this.spVATNumber.getText();
            String spPCode = this.spPostalCode.getText();
            List<Category> selectedCatList = spCatListTable.getSelectionModel().getSelectedItems();
            List<GeographicalArea> selectedGeoList = spGeoListTable.getSelectionModel().getSelectedItems();
            boolean success = this.registSPCont.newServiceProvider(
                    numMec, 
                    completName, 
                    shortName, 
                    email, 
                    vatNum,
                    spPCode,
                    selectedGeoList, 
                    selectedCatList );
            if (success && FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION,
                    "You really want to Regist the Service Provider?",
                    shortName).showAndWait().get() == ButtonType.OK) {
                this.registSPCont.registServiceProvider();
                this.registSPCont.createAuthForServicerovider(
                        shortName, 
                        email, 
                        this.spPass.getText(), 
                        Constants.ROLE_SERVICE_PROVIDER);
                actualStage.close();
                FxmlUtils.makeToast(actualStage, 
                        "The Service Provider " + shortName + " was created",
                        true);
            }else{
                FxmlUtils.makeToast(actualStage, 
                        "Error Creating service " + shortName, 
                        false);
            }
            
        }else{
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Please Fill all", "all fileds must be filled").show();
        }
    }

    @FXML
    private void cancel(ActionEvent event) {
         if(FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, 
                "Realy want to cancel?", 
                "All inserted data will be lost.").showAndWait().get() == ButtonType.OK){
            Stage stage = (Stage) this.numMec.getScene().getWindow();
            stage.close();
        }else{
            event.consume();
        }
    }

    private boolean verifyInputs() {
        boolean valid = true;
        if (this.spCompleteName.getText().isEmpty()
                || this.spVATNumber.getText().isEmpty()
                || this.spEmail.getText().isEmpty()
                || this.numMec.getText().isEmpty()
                || this.spPass.getText().isEmpty()
                || this.spPostalCode.getText().isEmpty()
                || this.lstCat.isEmpty()
                || this.lstGeo.isEmpty()) {
            valid = false;
        }

        if (!FxmlUtils.isEmail(this.spEmail.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Email field is not valid!").show();
            valid = false;
        }

        if (!FxmlUtils.isVatNumber(this.spVATNumber.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "VAT Number field is not valid!").show();
            valid = false;
        }
        if (!FxmlUtils.isInt(this.numMec.getText())) {
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Num Mec Number field is not valid!").show();
            valid = false;
        }

        return valid;

    }

    @FXML
    private void searchApplicationByVat(ActionEvent event) {
        String vatNumbet = this.spVATNumber.getText();
        Application spApp = null;
        if(vatNumbet !=null && !vatNumbet.isEmpty() && FxmlUtils.isVatNumber(vatNumbet)){
            spApp = this.registSPCont.getApplicationByVat(vatNumbet);
        }
        if(spApp != null){
            this.spShortName.setText(spApp.getSPName());
            this.spEmail.setText(spApp.getSPEmail());
        }
    }

}
