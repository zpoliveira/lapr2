/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.controller.RateServicesController;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ExecutionOrder;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceRequest;
import lapr.project.gpsd.model.registers.ServiceRequestRegister;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class RateServicesFXMLController implements Initializable {

    @FXML
    private AnchorPane basePane;
    @FXML
    private TableView<ExecutionOrder> tblExecOrder;
    @FXML
    private TableView<ServiceProvider> tblSP;
    @FXML
    private TableView<Service> tblSer;
    @FXML
    private ListView<String> tblDate;
    @FXML
    private TableColumn<ExecutionOrder, Integer> colExecOrderID;
    @FXML
    private TableColumn<ExecutionOrder, Integer> colServiceReqID;
    @FXML
    private TableColumn<ServiceProvider, String> colSPName;
    @FXML
    private TableColumn<Service, String> colService;
    @FXML
    private TableColumn<ServiceProvider, Double> colSPRating;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private ComboBox<Integer> comboExecID;
    @FXML
    private ComboBox<Integer> comboRating;

    private RateServicesController rateServicesController;
    private AuthController authController;
    private ServiceRequestRegister serviceRequestRegister;
    private List<Integer> execIDList;
    private List<Integer> ratings;
    private List<ExecutionOrder> unratedExecOrders;
    private List<ServiceProvider> serviceProviderList;
    private List<Service> serviceList;
    private List<String> calendarList;
    private ObservableList<ExecutionOrder> tableList;
    private ObservableList<ServiceProvider> SPList;
    private ObservableList<Service> serList;
    private ObservableList<String> calList;
    private ObservableList<Integer> executionIDList;
    private Company company;
    private Client client;
    private ServiceRequest serReq;
    private ExecutionOrder selectedExecutionOrder;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.rateServicesController = new RateServicesController();
        this.execIDList = new ArrayList<>();
        this.ratings = new ArrayList<>();
        this.unratedExecOrders = new ArrayList<>();
        this.serviceProviderList = new ArrayList<>();
        this.serviceList = new ArrayList<>();
        this.calendarList = new ArrayList<>();

        this.client = this.rateServicesController.getClient();

        List<ExecutionOrder> execList
                = this.rateServicesController.getExecutionOrdersByClient(client);

        this.ratings.add(0);
        this.ratings.add(1);
        this.ratings.add(2);
        this.ratings.add(3);
        this.ratings.add(4);
        this.ratings.add(5);

        for (ExecutionOrder exe : execList) {

            if (!exe.isRated()) {
                execIDList.add(exe.getId());
            }
        }

        for (ExecutionOrder exec : execList) {
            if (!exec.isRated()) {
                this.unratedExecOrders.add(exec);
            }
        }

        for (ExecutionOrder exe : this.unratedExecOrders) {
            this.serviceProviderList.add(exe.getServiceProvider());
            this.serviceList.add(exe.getService());

            int id = exe.getId();
            this.serReq = this.rateServicesController.getServiceRequestByID(id);
            this.calendarList.add(serReq.getCreationDate().getTime().toString());
        }
    }

    @FXML
    private void onSave(ActionEvent event) {

        if (this.verifyComboInputs()) {
            Integer execID = comboExecID.getValue();
            Integer rat = comboRating.getValue();

            for (ExecutionOrder exe : this.unratedExecOrders) {
                if (exe.getId() == execID) {
                    selectedExecutionOrder = exe;
                    this.rateServicesController.rateService(exe, rat);
                    int i = this.unratedExecOrders.indexOf(exe);
                    exe.setRated(true);
                    tableList.remove(i);
                    SPList.remove(i);
                    serList.remove(i);
                    executionIDList.remove(i);
                    calList.remove(i);
                    this.comboExecID.getSelectionModel().clearAndSelect(-1);
                    this.comboRating.getSelectionModel().clearAndSelect(-1);
                }
            }

            if (FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, "Service evaluated.", "See Invoice for this Service?").showAndWait().get() == ButtonType.OK) {

                FxmlUtils.createAlert(Alert.AlertType.INFORMATION, "INVOICE", this.getMessage()).show();
            }

        } else {
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Selection Error", "Both Execution Order ID and Rating have to be selected").show();
        }
    }

    private String getMessage() {

        StringBuilder sb = new StringBuilder();

        sb.append("***********************************************");
        sb.append(System.getProperty("line.separator"));
        sb.append("Name: ").append(this.client.getName());
        sb.append(System.getProperty("line.separator"));
        sb.append("VAT Number: ").append(this.client.getVatNumber());
        sb.append(System.getProperty("line.separator"));
        sb.append("Execution Order ID: ").append(selectedExecutionOrder.getId());
        sb.append(System.getProperty("line.separator"));
        sb.append("Service: ").append(selectedExecutionOrder.getService().getLittleDescription());
        sb.append(System.getProperty("line.separator"));
        sb.append("Service Provider: ").append(selectedExecutionOrder.getServiceProvider().getShortName());
        sb.append(System.getProperty("line.separator"));
        sb.append("NIF AQUI");
        //sb.append("VAT Number: " + selectedExecutionOrder.getServiceProvider().getVatNumber());
        sb.append(System.getProperty("line.separator"));
        sb.append("CUSTO AQUI");
        //sb.append("Total Price: "serReq.getServiceRequestDescriptions().getTotalCost());
        sb.append(System.getProperty("line.separator"));        
        sb.append("***********************************************");

        return sb.toString();
    }

    private boolean verifyComboInputs() {
        boolean valid = true;
        if (this.comboExecID.getSelectionModel().isEmpty() && this.comboRating.getSelectionModel().isEmpty()) {
            valid = false;
        }
        return valid;
    }

    @FXML
    private void onCancel(ActionEvent event) {
        if (FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION,
                "Realy want to cancel?",
                "All inserted data will be lost.").showAndWait().get() == ButtonType.OK) {
            Stage stage = (Stage) this.btnCancel.getScene().getWindow();
            stage.close();

        } else {
            event.consume();
        };
    }

    @FXML
    private void comboShowExecID(ActionEvent event) {
        Integer execID = comboExecID.getValue();
    }

    @FXML
    private void showRating(ActionEvent event) {
        Integer rat = comboRating.getValue();
    }


    public void associateAuthController(AuthController authController) {
        this.authController = authController;
        this.company = this.authController.getApplicationGPSD().getCompany();
        this.serviceRequestRegister = company.getServiceRequestRegister();

        colExecOrderID.setCellValueFactory(new PropertyValueFactory<>("id"));
        colServiceReqID.setCellValueFactory(new PropertyValueFactory<>("servicerequestID"));
        colSPName.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getShortName()));
        colService.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLittleDescription()));
        colSPRating.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getRatingMean()).asObject());

        tableList = FXCollections.observableArrayList();
        tableList.addAll(this.unratedExecOrders);
        tblExecOrder.setItems(tableList);

        SPList = FXCollections.observableArrayList();
        SPList.addAll(this.serviceProviderList);
        tblSP.setItems(SPList);

        serList = FXCollections.observableArrayList();
        serList.addAll(this.serviceList);
        tblSer.setItems(serList);

        calList = FXCollections.observableArrayList();
        calList.addAll(this.calendarList);
        tblDate.setItems(calList);

        executionIDList = FXCollections.observableArrayList();
        executionIDList.addAll(execIDList);
        this.comboExecID.getItems().addAll(executionIDList);

        this.comboRating.getItems().addAll(ratings);

    }

}
