/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import lapr.project.auth.model.UserRole;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.controller.SpecifyGeographicalAreaController;
import lapr.project.gpsd.controller.SpecifyServiceController;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;
import lapr.project.gpsd.utils.Constants;

/**
 * FXML Controller class
 *
 /**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class AfterLoginController implements Initializable {

    @FXML
    private BorderPane internalPane;

    private AuthController authController;
    private SpecifyServiceController specifyServiceController;

    private SpecifyGeographicalAreaController specifyGeoAreaCtrl;

    private List<UserRole> userRoles = new ArrayList();
    @FXML
    private Tab tabClient;
    @FXML
    private Tab tabAdministrative;
    @FXML
    private Tab tabServiceProvider;
    @FXML
    private Tab tabHumanResorces;
    @FXML
    private AnchorPane clientAnchor;
    @FXML
    private AnchorPane administrativeAnchor;
    @FXML
    private AnchorPane serviceProAnchor;
    @FXML
    private AnchorPane hrAnchor;
    @FXML
    private TabPane tabPane;

    private SingleSelectionModel selectionModel;
    private boolean alreadySelected = false;
    @FXML
    private Button specifyServiceButton;
    @FXML
    private Button btSpecifyGeoArea;
    @FXML
    private Button btServiceRequest;
    @FXML
    private Button specifyCategoryButton;
    @FXML
    private Button associatePostalAddress;
    @FXML
    private Button decideServiceRequestsButton;
    @FXML
    private Button registSpBtn;
    @FXML
    private Button rateService;
    @FXML
    private Button consultExcOrders;
    @FXML
    private Button reportEndWork;
    @FXML
    private Button specifyAvailabilityTimeButton;
    @FXML
    private MenuItem doLogOut;
    @FXML
    private Button evaluateSP;



    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.selectionModel = this.tabPane.getSelectionModel();
    }

    @FXML
    private void logOut(ActionEvent event) {
        this.authController.doLogout();
        FxmlUtils.doLogout(this.internalPane);
    }

    public void associateAuthController(AuthController authController) {
        this.authController = authController;
        this.userRoles = this.authController.getUserRoles();
        this.createDiferentTabScenes();
    }

    private void createDiferentTabScenes() {
        this.userRoles.forEach((userRole) -> {
            this.activateTab(userRole.getRole());
        });
    }

    private void activateTab(String userRole) {
        switch (userRole) {
            case Constants.ROLE_CLIENT:
                this.tabClient.setDisable(false);
                this.clientAnchor.setVisible(true);
                this.setSelectedTab(this.tabClient);
                break;
            case Constants.ROLE_ADMINISTRATIVE:
                this.tabAdministrative.setDisable(false);
                this.administrativeAnchor.setVisible(true);
                this.setSelectedTab(this.tabAdministrative);
                break;
            case Constants.ROLE_HR:
                this.tabHumanResorces.setDisable(false);
                this.hrAnchor.setVisible(true);
                this.setSelectedTab(this.tabHumanResorces);
                break;
            case Constants.ROLE_SERVICE_PROVIDER:
                this.tabServiceProvider.setDisable(false);
                this.serviceProAnchor.setVisible(true);
                this.setSelectedTab(this.tabServiceProvider);
                break;
            default:
        }
    }

    private void setSelectedTab(Tab currentTab) {
        if (!this.alreadySelected) {
            this.alreadySelected = true;
            this.selectionModel.clearSelection();
            this.selectionModel.select(currentTab);
        }
    }

    @FXML
    private void specifyService(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getResource("/fxml/specifyservice.fxml"));
        try {
            Pane pane = loader.load();
            SpecifyServiceFxmlController specifyServiceFxmlController = loader.getController();
            specifyServiceFxmlController.associateAuthController(authController);
            FxmlUtils.newWindow("Specify Service", pane);

        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void evaluateSP(ActionEvent event){
        FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getResource("/fxml/evaluateServiceProvider.fxml"));
        try {
            Pane pane = loader.load();
            EvaluateServiceProviderFXMLController evaluateServiceProviderFXMLController = loader.getController();
            evaluateServiceProviderFXMLController.associateAuthController(authController);
            FxmlUtils.newWindow("Evaluate Service Provider", pane);

        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void specifyAvailabilityTime(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getResource("/fxml/specifyDailyAvailability.fxml"));
        try {
            Pane pane = loader.load();
            SpecifyDailyAvailabilityFXMLController specifyDailyAvailabilityFXMLController = loader.getController();
            specifyDailyAvailabilityFXMLController.associateAuthController(authController);
            FxmlUtils.newWindow("Specify Availability Time", pane);

        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void specifyCategory(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getResource("/fxml/specifyCategory.fxml"));
        try {
            Pane pane = loader.load();
            SpecifyCategoryFXMLController specifyCategoryFXMLController = loader.getController();
            specifyCategoryFXMLController.associateAuthController(authController);
            FxmlUtils.newWindow("Specify Category", pane);

        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btSpecifyGeoArea(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(
                FxmlUtils.class.getResource("/fxml/specifyGeoArea.fxml"));
        try {
            Pane pane = loader.load();
            SpecifyGeoAreaFXMLController specifyGeoAreaFXMLController
                    = loader.getController();
            FxmlUtils.newWindow("Specify Geographical Area", pane);
        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void goToAssociatePostalAddress(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(
                FxmlUtils.class.getResource("/fxml/associatePostalAddress.fxml"));
        try {
            Pane pane = loader.load();
            AssociatePostalAddressFXMLController associatePostalAddressFXMLController = loader.getController();
            associatePostalAddressFXMLController.associateAuthController(
                    authController);
            FxmlUtils.newWindow("Associate Postal Address", pane);

        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName()
            ).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void goToDecideServiceResquests(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(
                FxmlUtils.class.getResource("/fxml/decideServiceRequest.fxml"));
        try {
            Pane pane = loader.load();
            DecideServiceRequestsFXMLController decideServiceRequestsFXMLController = loader.getController();
            decideServiceRequestsFXMLController.associateAuthController(
                    authController);
            FxmlUtils.newWindow("Decide Service Requests", pane);

        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName()
            ).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void btActionServiceRequest(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(
                FxmlUtils.class.getResource("/fxml/requestService.fxml"));
        try {
            Pane pane = loader.load();
            RequestServiceFXMLController serviceRequestFxmlController
                    = loader.getController();
            FxmlUtils.newWindow("Request Service", pane);
        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void goToRateService(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(
                FxmlUtils.class.getResource("/fxml/rateServices.fxml"));
        try {
            Pane pane = loader.load();
            RateServicesFXMLController rateServicesFXMLController
                    = loader.getController();
            rateServicesFXMLController.associateAuthController(
                    authController);
            FxmlUtils.newWindow("Request Service", pane);
        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void goToRegistSp(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getResource("/fxml/serviceProviderResgisterFxml.fxml"));
        try {
            Pane pane = loader.load();
            FxmlUtils.newWindow("Regist Service Provider", pane);

        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void goToConsultExcOrder(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getResource("/fxml/consultExecutionOrders.fxml"));
        try {
            Pane pane = loader.load();
            FxmlUtils.newWindow("Consult Execution Orders", pane);
        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void goToReportEndWork(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getResource("/fxml/reportEndWorkFxml.fxml"));
        try {
            Pane pane = loader.load();
            FxmlUtils.newWindow("Report End of Work", pane);
        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
