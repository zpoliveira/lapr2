/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class StartFXMLController implements Initializable {

    @FXML
    private BorderPane mainBorderPane;
    @FXML
    private Button startLogIn;
    @FXML
    private Button startSignIn;
    @FXML
    private Button startApplyToSP;
    
    private AuthController authController;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void goToLogin(ActionEvent event) {
        Pane pane = FxmlUtils.createNewBorderPane("/fxml/mainFXML.fxml");
        mainBorderPane.getChildren().clear();
        mainBorderPane.getChildren().setAll(pane);
    }

    @FXML
    private void goToSignIn(ActionEvent event) {
         FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getResource("/fxml/clientRegister.fxml"));
        try {
            Pane pane = loader.load();
            FxmlUtils.newWindow("Client Register", pane);

        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void goToApplyToSP(ActionEvent event) {
       
        FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getResource("/fxml/submitApplicationFXML.fxml"));
        try {
            Pane pane = loader.load();
            SubmitApplicationFXMLController submitApplicationFXMLController = loader.getController();
            submitApplicationFXMLController.associateAuthController(authController);
            FxmlUtils.newWindow("Specify Service", pane);

        } catch (IOException ex) {
            Logger.getLogger(AfterLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
