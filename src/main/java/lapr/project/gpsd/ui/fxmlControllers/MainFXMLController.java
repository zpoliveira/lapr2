/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class MainFXMLController implements Initializable {

    @FXML
    private TextField username;
    @FXML
    private TextField password;
    
    @FXML
    private BorderPane mainBorderPane;
    
    private AuthController auth;
    @FXML
    private Button login;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.auth = new AuthController();
    }    


    @FXML
    private void doLogin(ActionEvent event) {
        String user;
        user = this.username.getText();
        String pass;
        pass = this.password.getText();
        
        if(this.auth.doLogin(user, pass)){
            FXMLLoader loader = new FXMLLoader(FxmlUtils.class.getResource("/fxml/afterLogin.fxml"));
            try {
                Pane pane = loader.load();
                AfterLoginController afterLoginController = loader.getController();
                afterLoginController.associateAuthController(this.auth);
                mainBorderPane.getChildren().setAll(pane);
            } catch (IOException ex) {
                Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }else{
            FxmlUtils.createAlert(Alert.AlertType.ERROR, 
                    "Invalid Credentials", 
                    "The email or password are invalid").show();
        }
        
    }


    
}
