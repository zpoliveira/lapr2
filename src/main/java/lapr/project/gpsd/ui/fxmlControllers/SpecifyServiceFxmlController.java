/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.controller.SpecifyServiceController;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SpecifyServiceFxmlController implements Initializable {

    private AuthController authController;
    private SpecifyServiceController specifyServiceController;
    private String newServiceId, littleDescription, largeDescription, catId, type;
    private double cost;
    private Company company;
    @FXML
    private ComboBox<Category> categoryList;
    @FXML
    private ComboBox<String> serviceTypeList;
    @FXML
    private TextField serviceId;
    @FXML
    private TextField serviceDescription;
    @FXML
    private TextField serviceCost;
    @FXML
    private TextArea serviceLargeDesc;
    @FXML
    private Button cancelSpecify;
    @FXML
    private Label durationLabel;
    @FXML
    private TextField durationInput;
    
    private boolean isFixedService = false;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.specifyServiceController = new SpecifyServiceController();
        this.serviceTypeList.getItems().addAll(this.specifyServiceController.getServiceTypesNames());
    } 
    
     public void associateAuthController(AuthController authController) {
        this.authController = authController;
        this.company = this.authController.getApplicationGPSD().getCompany();
        this.categoryList.getItems().addAll(this.company.getCategoryRegister().getCategories());
    }

    @FXML
    private void saveNewService(ActionEvent event) {
        Stage actualStage = (Stage) this.cancelSpecify.getScene().getWindow();
        if(this.verifyInputs()){
            this.newServiceId  = this.serviceId.getText();
            this.littleDescription = this.serviceDescription.getText();
            this.largeDescription = this.serviceLargeDesc.getText();
            this.cost = Double.valueOf(this.serviceCost.getText());
            this.catId = this.categoryList.getValue().getCatId();
            this.type = this.serviceTypeList.getValue();
            boolean success = this.specifyServiceController.newService(this.newServiceId, 
                    this.littleDescription, 
                    this.largeDescription, 
                    this.cost, 
                    this.catId, 
                    type
            );
            if (success && FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION,
                    "You really want to create the Service?",
                    this.littleDescription).showAndWait().get() == ButtonType.OK) {
                this.specifyServiceController.registService();
                if(this.isFixedService){
                    if (!FxmlUtils.isInt(this.durationInput.getText())) {
                        FxmlUtils.createAlert(Alert.AlertType.WARNING, "Information", "Duration field is not valid!").show();
                    }else{
                        this.specifyServiceController.setDurationForFixedService(Integer.valueOf(this.durationInput.getText()), this.newServiceId);
                    }
                    
                }
                actualStage.close();
                FxmlUtils.makeToast(actualStage, 
                        "The service " + this.newServiceId + " was created",
                        true);
            }else{
                FxmlUtils.makeToast(actualStage, 
                        "Error Creating service " + this.newServiceId, 
                        false);
            }
        }else{
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Please Fill all", "all fileds must be filled").show();
        }
        
    }

    @FXML
    private void cancelNewService(ActionEvent event) {
        if(FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, 
                "Realy want to cancel?", 
                "All inserted data will be lost.").showAndWait().get() == ButtonType.OK){
            Stage stage = (Stage) this.cancelSpecify.getScene().getWindow();
            stage.close();
        }else{
            event.consume();
        }
    }
     
     private boolean verifyInputs(){
         boolean valid = true;
         if(this.serviceId.getText().isEmpty() ||
            this.serviceDescription.getText().isEmpty() ||
            this.serviceLargeDesc.getText().isEmpty() ||
            this.serviceCost.getText().isEmpty() ||
            this.categoryList.getValue().getCatId().isEmpty() ||
            this.serviceTypeList.getValue().isEmpty() ){
             valid = false;
         }
         if(this.isFixedService && this.durationInput.getText().isEmpty()){
             valid=false;
         }
         return valid;
     }

    @FXML
    private void isFixed(ActionEvent event) {
        if(this.serviceTypeList.getValue().equalsIgnoreCase("Fixed Service")){
            this.durationLabel.setVisible(true);
            this.durationInput.setVisible(true);
            this.isFixedService = true;
        }else{
            this.durationLabel.setVisible(false);
            this.durationInput.setVisible(false);
            this.isFixedService = false;
        }
    }
}
