/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.controller.SpecifyDailyAvailabilityController;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;

/**
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SpecifyDailyAvailabilityFXMLController implements Initializable {

    private SpecifyDailyAvailabilityController specifyDailyAvailabilityController;
    private AuthController authController;

    @FXML
    private ComboBox<String> cbTimeHourInitial;

    @FXML
    private ComboBox<String> cbTimeMinuteInitial;

    @FXML
    private ComboBox<String> cbTimeHourFinal;

    @FXML
    private ComboBox<String> cbTimeMinuteFinal;

    @FXML
    private Button backButton;

    @FXML
    private DatePicker dateSelection;

    @FXML
    private Button addDisponibilityButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.specifyDailyAvailabilityController = new SpecifyDailyAvailabilityController();

        //populate comboBoxes
        for (int i = 6; i < 24; i++) {
            cbTimeHourInitial.getItems().add(String.format("%02d", i));
        }
        for (int i = 0; i < 60; i++) {
            cbTimeMinuteInitial.getItems().add(String.format("%02d", i));
        }

        for (int i = 6; i < 24; i++) {
            cbTimeHourFinal.getItems().add(String.format("%02d", i));
        }
        for (int i = 0; i < 60; i++) {
            cbTimeMinuteFinal.getItems().add(String.format("%02d", i));
        }

    }

    public void associateAuthController(AuthController authController) {
        this.authController = authController;
    }

    @FXML
    private void clickAddAvailability(ActionEvent event) {
        Stage stage = (Stage) this.backButton.getScene().getWindow();
        ServiceProvider sp = this.specifyDailyAvailabilityController.getServiceProvider();
        if (dateSelection.getValue() != null && cbTimeHourInitial.getValue() != null
                && cbTimeMinuteInitial.getValue() != null && cbTimeHourFinal.getValue() != null
                && cbTimeMinuteFinal.getValue() != null) {
            LocalDate selDate = dateSelection.getValue();
            LocalDateTime dateTimeInitial = LocalDateTime.of(
                    selDate,
                    LocalTime.of(
                            Integer.valueOf(cbTimeHourInitial.getValue()),
                            Integer.valueOf(cbTimeMinuteInitial.getValue())));

            LocalDateTime dateTimeFinal = LocalDateTime.of(
                    selDate,
                    LocalTime.of(
                            Integer.valueOf(cbTimeHourFinal.getValue()),
                            Integer.valueOf(cbTimeMinuteFinal.getValue())));


            if (dateTimeFinal.isAfter(dateTimeInitial) && dateTimeFinal.isAfter(LocalDateTime.now()) && FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION,
                    "You really want to create the availability?",
                    "It will be saved in our database.").showAndWait().get() == ButtonType.OK) {
                stage.close();
                boolean func = this.specifyDailyAvailabilityController.addDisponibility(sp, dateTimeInitial, dateTimeFinal);
                if (func) {
                    FxmlUtils.makeToast(stage, "The availability was created with success.",
                            true);
                } else {
                    FxmlUtils.makeToast(stage, "The availability was already specified.",
                            false);
                }
            } else {
                FxmlUtils.createAlert(Alert.AlertType.ERROR, "Wrong dates", "The final date must be after the initial date and the initial date must be in the future!").show();
            }
        } else {
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Empty parameters", "All information must be filled.").show();
        }

    }

    @FXML
    private void goBack(ActionEvent event) {
        if (FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION,
                "Realy want to cancel?",
                "All inserted data will be lost.").showAndWait().get() == ButtonType.OK) {
            Stage stage = (Stage) this.backButton.getScene().getWindow();
            stage.close();
        } else {
            event.consume();
        }
    }

    private static Calendar localDateTimeToDate(LocalDateTime localDateTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(localDateTime.getYear(), localDateTime.getMonthValue() - 1, localDateTime.getDayOfMonth(),
                localDateTime.getHour(), localDateTime.getMinute(), localDateTime.getSecond());
        return calendar;
    }
}
