/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui.fxmlControllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.ApplicationGPSD;
import lapr.project.gpsd.controller.AssociatePostalAddressController;
import lapr.project.gpsd.controller.AuthController;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.ui.fxmlUtils.FxmlUtils;
import lapr.project.gpsd.utils.Utils;

/**
 * FXML Controller class
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class AssociatePostalAddressFXMLController implements Initializable {

    private AuthController authController;
    
    @FXML
    private AnchorPane basePane;
    @FXML
    private Label clientName;
    @FXML
    private TableView<PostalAddress> tblAddresses;
    @FXML
    private TableColumn<?, ?> colAddress;
    @FXML
    private TableColumn<?, ?> colPostalCode;
    @FXML
    private TableColumn<?, ?> colLocal;
    @FXML
    private TextField txtAddress;
    @FXML
    private TextField txtPostalCode;
    @FXML
    private TextField txtLocal;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private Label clientEmail;
    @FXML
    private Label clientVAT;
    @FXML
    private Label clientPhone;
    
    private ObservableList<PostalAddress> lstAddresses;
    
    private Client client;
    
    private String address, postalCode, local;
    
    private AssociatePostalAddressController associatePostalAddressController;
    
    private PostalAddress postalAddress;
    
    private Company company;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.associatePostalAddressController = new AssociatePostalAddressController();
        
        this.company = ApplicationGPSD.getInstance().getCompany();
        String aux = company.getAuthFacade().getCurrentSession().getUserEmail();
        this.client = company.getClientRegister().getClientByEmail(aux);
        this.clientName.setText(client.getName());
        this.clientEmail.setText(client.getEmail());
        this.clientVAT.setText(client.getVatNumber());
        this.clientPhone.setText(client.getPhone());
        //Create collumns to tableview
        
        this.colAddress.setCellValueFactory(new PropertyValueFactory<>("address"));
        this.colPostalCode.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
        this.colLocal.setCellValueFactory(new PropertyValueFactory<>("place"));
        this.lstAddresses = FXCollections.observableArrayList(this.client.getAddresses());
        this.tblAddresses.setItems(lstAddresses);
        
    } 
    
    private boolean verifyPostalAddressInputs(){
         boolean valid = true;
         if(this.txtAddress.getText().isEmpty() ||
            this.txtPostalCode.getText().isEmpty() ||
            this.txtLocal.getText().isEmpty()){
             valid = false;
         }
         return valid;
    }
    
    private boolean verifyPostalCode(){
        boolean valid = true;
        if (!Utils.postalCodes.containsKey(this.txtPostalCode.getText())) {
            
            valid = false;
            }
        
        return valid;
    }

    @FXML
    private void onSave(ActionEvent event) {
        if(this.verifyPostalAddressInputs() && this.verifyPostalCode()){
        this.address = txtAddress.getText();
        this.postalCode = txtPostalCode.getText();
        this.local = txtLocal.getText();
        this.associatePostalAddressController.getClient().addAddress(new PostalAddress(this.address, this.postalCode, this.local));
        lstAddresses.add(new PostalAddress(this.address, this.postalCode, this.local));
        tblAddresses.refresh();
        if(FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, "Add new Postal Address?", "Please choose").showAndWait().get() == ButtonType.CANCEL){
            Pane pane = FxmlUtils.createNewBorderPane("/fxml/afterLogin.fxml");
            basePane.getScene().setRoot(pane);
        };
        
        }else if(!this.verifyPostalAddressInputs()){
            FxmlUtils.createAlert(Alert.AlertType.ERROR, "Please Fill all", "all fileds must be filled").show();
        }else if(!this.verifyPostalCode()){
            FxmlUtils.createAlert(Alert.AlertType.WARNING, "Invalid Postal Code", "Please insert postal address").show();
        }
    }

    @FXML
    private void onCancel(ActionEvent event) {
       if(FxmlUtils.createAlert(Alert.AlertType.CONFIRMATION, 
                "Realy want to cancel?", 
                "All inserted data will be lost.").showAndWait().get() == ButtonType.OK){
            Stage stage = (Stage) this.btnCancel.getScene().getWindow();
            stage.close();
        }else{
            event.consume();
        }
    }
    
    public void associateAuthController(AuthController authController) {
        this.authController = authController;        
    }
    
}
