/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class Utils {

    public static HashMap<String, Double[]> postalCodes = new HashMap<>();

    public static BufferedReader bfreader;

    public static BufferedWriter bfwrite;

    public static HashMap<String, Double[]> readPostalCodesFromFile(String filePath) {
        try {
            bfreader = new BufferedReader(
                    new FileReader(filePath));
            String firstLine = bfreader.readLine().trim();
            String[] tempArray = firstLine.split(";");
            int cp4Index = Arrays.asList(tempArray).lastIndexOf("CP4");
            int cp3Index = Arrays.asList(tempArray).lastIndexOf("CP3");
            int latIndex = Arrays.asList(tempArray).lastIndexOf("LATITUDE");
            int logIndex = Arrays.asList(tempArray).lastIndexOf("LONGITUDE");
            int numberColumns = firstLine.split(";").length;
            generateData(bfreader, cp4Index, cp3Index, latIndex, logIndex, numberColumns);
        }catch(Exception e){
           return null;
        }

        return postalCodes;
    }

    private static void generateData(BufferedReader bfreader, int cp4Index, int cp3Index, int latIndex, int logIndex, int numberColumns) {
        String line = "";
        String[] temp = new String[numberColumns];
        try {
            while ((line = bfreader.readLine()) != null) {
                line.trim();
                temp = line.split(";");
                String cp3modified = temp[cp3Index];
                if(temp[cp3Index].length() == 1){
                    cp3modified = "00"+temp[cp3Index];
                }
                if(temp[cp3Index].length() == 2){
                    cp3modified = "0"+temp[cp3Index];
                }
                String key = temp[cp4Index] +"-"+cp3modified;
                Double latitude = Double.valueOf(temp[latIndex].replace(",", "."));
                Double longitude = Double.valueOf(temp[logIndex].replace(",", "."));
                Double[] latLong = new Double[2];
                latLong[0] = latitude;
                latLong[1] = longitude;
                postalCodes.put(key, latLong);
            }

        } catch (Exception ex) {
           Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    public static LocalDateTime getCalendar(Date date, Date time) {
        if (date == null) {
            return null;
        }

        if (time == null) {
            return null;
        }
        LocalDateTime local = null;
        try {
            LocalDate dt = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalTime tm = LocalTime.of(time.getHours(), time.getMinutes());
          local = LocalDateTime.of(dt,tm);
        } catch (Exception e) {
            local = null;
        }

        return local;
    }

}
