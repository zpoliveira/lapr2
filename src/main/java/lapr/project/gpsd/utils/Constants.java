/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.utils;

import java.util.HashMap;

/**
 *
 * @author paulomaio
 /**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class Constants
{
    public static final String ROLE_ADMINISTRATIVE = "ADMINISTRATIVE";
    public static final String ROLE_CLIENT = "CLIENT";
    public static final String ROLE_HR = "HR";
    public static final String ROLE_SERVICE_PROVIDER = "SERVICE PROVIDER";
    
    public static final String PARAMS_FILE = "config.properties";
    public static final String PARAMS_COMPANY_DESIGNATION = "Company.Designation";
    public static final String PARAMS_COMPANY_VAT = "Company.VAT";
    public static final String PARAMS_COMPANY_TASKINTERVAL = "Company.TaskInterval";
    public static final String PARAMS_COMPANY_TASKDELAY = "Company.TaskDelay";
    public static final String PARAMS_COMPANY_TASKSCHEDULE = "Company.TaskSchedule"; 
    public static final HashMap<String,String> SERVICE_TYPES = new HashMap(){
        {
            put("FixedService","Fixed Service");
            put("LimitatedService","Limitated Service");
            put("ExpansibleService","Expansible Service");
        }
    
    };
}
