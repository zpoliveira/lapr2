/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import lapr.project.gpsd.model.AcademicQualification;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.Disponibility;
import lapr.project.gpsd.model.ExecutionOrder;
import lapr.project.gpsd.model.FixedService;
import lapr.project.gpsd.model.GeographicalArea;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.ProfessionalQualification;
import lapr.project.gpsd.model.Rating;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceRequest;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ExcelReader {

    private static final String GEOGRAFICALAREA_FILE = "AreaGeografica_UC5.xlsx";
    private static final String CATEGORY_FILE = "Categoria_UC3.xlsx";
    private static final String APPLICATION_FILE = "CandidaturaPrestadorServicos_UC2.xlsx";
    private static final String CLIENT_FILE = "RegistoCiente_UC1.xlsx";
    private static final String SERVICEPROVIDER_FILE = "RegistoPrestadorServico_UC8.xlsx";
    private static final String DISPONIBILITIES_FILE = "IndicarDisponibilidade_UC9.xlsx";
    private static final String SERCICES_FILE = "EspecificarServico_UC4.xlsx";
    private static final String SERVICEREQUEST_FILE = "PedidoPrestacaoServico_UC6.xlsx";
    private static final String EXECUTIONORDERS1_FILE = "OrdensExecucao_APadrao_UC16.xlsx";
    private static final String EXECUTIONORDERS2_FILE = "OrdensExecucao_JSantos_UC16.xlsx";
    private static final String EXECUTIONORDERS3_FILE = "OrdensExecucao_MSilva_UC16.xlsx";
    private static final String RATINGSERVICEPROVIDER_FILE = "RatingPrestadorServico_UC14.xlsx";
    private static final String ENDSERVICE_FILE = "ServicoCompleto_UC13.xlsx";

    //DateFormat
    private static final String DATETIME_FORMAT = "dd/MM/yyyy HH:mm";

    /**
     * Read Geographical Areas from Excel
     *
     * @param company actual company
     */
    public static void readGeograficalArea(Company company) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(GEOGRAFICALAREA_FILE);

            // Using XSSF for xlsx format, for xls use HSSF
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();
            //read header
            if (rowIterator.hasNext()) {
                Row row = rowIterator.next();
            }
            DataFormatter dataFormatter = new DataFormatter();
            //iterating over each row
            while (rowIterator.hasNext()) {

                try {

                    Row row = rowIterator.next();
                    String designation = dataFormatter.formatCellValue(row.getCell(0));
                    String postalCode = dataFormatter.formatCellValue(row.getCell(1));
                    double radius = Double.valueOf(dataFormatter.formatCellValue(row.getCell(2)));
                    double cust = Double.valueOf(dataFormatter.formatCellValue(row.getCell(3)));

                    company.getGeographicalAreaRegister().registerGeographicalArea(new GeographicalArea(designation, cust, radius, postalCode,company.getExternalService()));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Read Categories from Excel
     *
     */
    public static void readCategories(Company company) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(CATEGORY_FILE);

            // Using XSSF for xlsx format, for xls use HSSF
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();
            //read header
            if (rowIterator.hasNext()) {
                Row row = rowIterator.next();
            }

            //iterating over each row
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                try {
                    String code = String.valueOf((int) row.getCell(0).getNumericCellValue());
                    String description = row.getCell(2).getStringCellValue();
                    company.getCategoryRegister().registerCategory(new Category(code, description));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * *
     * Read Applications to Service Provider from Excel
     *
     * @param catRegister
     */
    public static void readApplicationsServiceProvider(Company company) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(APPLICATION_FILE);

            // Using XSSF for xlsx format, for xls use HSSF
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();
            //read header
            if (rowIterator.hasNext()) {
                Row row = rowIterator.next();
            }
            DataFormatter dataFormatter = new DataFormatter();
            //iterating over each row
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                try {
                    String vat = dataFormatter.formatCellValue(row.getCell(0));
                    String completeName = dataFormatter.formatCellValue(row.getCell(1));
                    String phone = dataFormatter.formatCellValue(row.getCell(2));
                    String email = dataFormatter.formatCellValue(row.getCell(3));
                    String address = dataFormatter.formatCellValue(row.getCell(4));
                    String local = dataFormatter.formatCellValue(row.getCell(5));
                    String postalCode = dataFormatter.formatCellValue(row.getCell(6));

                    Application appli = new Application(completeName, vat, phone, email, new PostalAddress(address, local, postalCode));

                    String aq1 = dataFormatter.formatCellValue(row.getCell(7));
                    String aq2 = dataFormatter.formatCellValue(row.getCell(8));
                    String aq3 = dataFormatter.formatCellValue(row.getCell(9));

                    if (!aq1.isEmpty()) {
                        appli.getAcademicQualificationList().add(new AcademicQualification("", aq1, ""));
                    }

                    if (!aq2.isEmpty()) {
                        appli.getAcademicQualificationList().add(new AcademicQualification("", aq2, ""));
                    }

                    if (!aq3.isEmpty()) {
                        appli.getAcademicQualificationList().add(new AcademicQualification("", aq3, ""));
                    }

                    String ap1 = dataFormatter.formatCellValue(row.getCell(10));
                    String ap2 = dataFormatter.formatCellValue(row.getCell(11));
                    String ap3 = dataFormatter.formatCellValue(row.getCell(12));

                    if (!ap1.isEmpty()) {
                        appli.getProfessionalQualificationList().add(new ProfessionalQualification(ap1));
                    }

                    if (!ap2.isEmpty()) {
                        appli.getProfessionalQualificationList().add(new ProfessionalQualification(ap2));
                    }

                    if (!ap3.isEmpty()) {
                        appli.getProfessionalQualificationList().add(new ProfessionalQualification(ap3));
                    }

                    String cat1 = dataFormatter.formatCellValue(row.getCell(13));
                    String cat2 = dataFormatter.formatCellValue(row.getCell(14));
                    String cat3 = dataFormatter.formatCellValue(row.getCell(15));

                    if (!cat1.isEmpty()) {
                        Category cat = company.getCategoryRegister().getCategorieByDescription(cat1);
                        if (cat != null) {
                            appli.getCategoryList().add(cat);
                        }
                    }

                    if (!cat2.isEmpty()) {
                        Category cat = company.getCategoryRegister().getCategorieByDescription(cat2);
                        if (cat != null) {
                            appli.getCategoryList().add(cat);
                        }
                    }

                    if (!cat3.isEmpty()) {
                        Category cat = company.getCategoryRegister().getCategorieByDescription(cat3);
                        if (cat != null) {
                            appli.getCategoryList().add(cat);
                        }
                    }
                    company.getApplicationRegister().registerApplication(appli);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * *
     * Read Clients from Excel and adds to registers
     */
    public static void readClient(Company company) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(CLIENT_FILE);

            // Using XSSF for xlsx format, for xls use HSSF
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();
            //read header
            if (rowIterator.hasNext()) {
                Row row = rowIterator.next();
            }
            DataFormatter dataFormatter = new DataFormatter();
            //iterating over each row
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                try {
                    String vat = dataFormatter.formatCellValue(row.getCell(0));
                    String completName = dataFormatter.formatCellValue(row.getCell(1));
                    String phone = dataFormatter.formatCellValue(row.getCell(2));
                    String email = dataFormatter.formatCellValue(row.getCell(3));
                    String pass = dataFormatter.formatCellValue(row.getCell(4));
                    String address = dataFormatter.formatCellValue(row.getCell(5));
                    String local = dataFormatter.formatCellValue(row.getCell(6));
                    String postalCode = dataFormatter.formatCellValue(row.getCell(7));

                    Client cli = new Client(completName, vat, phone, email);
                    cli.addAddress(new PostalAddress(address, local, postalCode));

                    company.getClientRegister().addClient(cli);
                    company.getAuthFacade().registerUserWithRole(cli.getName(), cli.getEmail(), pass, Constants.ROLE_CLIENT);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * *
     * Read Service Providers from Excel and adds to registers
     *
     * @param company
     */
    public static void readServiceProvider(Company company) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(SERVICEPROVIDER_FILE);

            // Using XSSF for xlsx format, for xls use HSSF
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();
            //read header
            if (rowIterator.hasNext()) {
                Row row = rowIterator.next();
            }
            DataFormatter dataFormatter = new DataFormatter();
            //iterating over each row
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                try {
                    int meca = Integer.valueOf(dataFormatter.formatCellValue(row.getCell(0)));
                    String vat = dataFormatter.formatCellValue(row.getCell(1));
                    String completName = dataFormatter.formatCellValue(row.getCell(2));
                    String shortName = dataFormatter.formatCellValue(row.getCell(3));
                    String email = dataFormatter.formatCellValue(row.getCell(4));
                    String postalCode = dataFormatter.formatCellValue(row.getCell(5));
                    String cat1 = dataFormatter.formatCellValue(row.getCell(6));
                    String cat2 = dataFormatter.formatCellValue(row.getCell(7));
                    String cat3 = dataFormatter.formatCellValue(row.getCell(8));
                    String ag1 = dataFormatter.formatCellValue(row.getCell(9));
                    String ag2 = dataFormatter.formatCellValue(row.getCell(10));

                    ServiceProvider sp = new ServiceProvider(meca, completName, shortName, email, vat, postalCode);

                    sp.addCategory(company.getCategoryRegister().getCategorieByDescription(cat1));
                    sp.addCategory(company.getCategoryRegister().getCategorieByDescription(cat2));
                    sp.addCategory(company.getCategoryRegister().getCategorieByDescription(cat3));

                    sp.addGeographicalArea(company.getGeographicalAreaRegister().getGeographicalAreaByDesignation(ag1));
                    sp.addGeographicalArea(company.getGeographicalAreaRegister().getGeographicalAreaByDesignation(ag2));

                    company.getServiceProviderRegister().registServiceProvider(sp);
                    company.getAuthFacade().registerUserWithRole(sp.getShortName(), sp.getEmail(), "sp", Constants.ROLE_SERVICE_PROVIDER);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * *
     * Read Disponibilities from Excel and adds to Service Providers
     *
     */
    public static void readDisponibilities(Company company) {
         FileInputStream fis = null;
        try {
            fis = new FileInputStream(DISPONIBILITIES_FILE);

            // Using XSSF for xlsx format, for xls use HSSF
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();
            //read header
            if (rowIterator.hasNext()) {
                Row row = rowIterator.next();
            }
            DataFormatter dataFormatter = new DataFormatter();
            //iterating over each row
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                try {
                    String a =dataFormatter.formatCellValue(row.getCell(0));
                    int meca = Integer.valueOf(dataFormatter.formatCellValue(row.getCell(0)));
                    
                    ServiceProvider sp = company.getServiceProviderRegister().getServiceProviderByMeca(meca);
                    
                    Date dtI1 = row.getCell(2).getDateCellValue();
                    Date tmI1 = row.getCell(3).getDateCellValue();
                    LocalDateTime dtTmI1 = Utils.getCalendar(dtI1, tmI1);
                    Date dtE1 = row.getCell(4).getDateCellValue();
                    Date tmE1 = row.getCell(5).getDateCellValue();
                    LocalDateTime dtTmE1 = Utils.getCalendar(dtE1, tmE1);
                    
                    if (dtTmE1 != null && dtTmI1 != null) {
                        sp.addDisponibility(new Disponibility(dtTmI1, dtTmE1)); 
                    }

                    Date dtI2 = HSSFDateUtil.isCellDateFormatted(row.getCell(6))? row.getCell(6).getDateCellValue():null;
                    Date tmI2 = HSSFDateUtil.isCellDateFormatted(row.getCell(7))? row.getCell(7).getDateCellValue():null;
                    LocalDateTime dtTmI2 = Utils.getCalendar(dtI2, tmI2);
                    Date dtE2 = HSSFDateUtil.isCellDateFormatted(row.getCell(8))? row.getCell(8).getDateCellValue():null;
                    Date tmE2 = HSSFDateUtil.isCellDateFormatted(row.getCell(9))? row.getCell(9).getDateCellValue():null;
                    LocalDateTime dtTmE2 = Utils.getCalendar(dtE2, tmE2);
                    
                    if (dtTmE2 != null && dtTmI2 != null) {
                        sp.addDisponibility(new Disponibility(dtTmI2, dtTmE2)); 
                    }
                    
                    Date dtI3 = HSSFDateUtil.isCellDateFormatted(row.getCell(10))? row.getCell(10).getDateCellValue():null;
                    Date tmI3 = HSSFDateUtil.isCellDateFormatted(row.getCell(11))? row.getCell(11).getDateCellValue():null;
                    LocalDateTime dtTmI3 = Utils.getCalendar(dtI3, tmI3);
                    Date dtE3 = HSSFDateUtil.isCellDateFormatted(row.getCell(12))? row.getCell(12).getDateCellValue():null;
                    Date tmE3 = HSSFDateUtil.isCellDateFormatted(row.getCell(13))? row.getCell(13).getDateCellValue():null;
                    LocalDateTime dtTmE3 = Utils.getCalendar(dtE3, tmE3);
                    
                    if (dtTmE3 != null && dtTmI3 != null) {
                        sp.addDisponibility(new Disponibility(dtTmI3, dtTmE3)); 
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * *
     * Read Services from Excel and adds to Register
     *
     */
    public static void readServices(Company company) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(SERCICES_FILE);

            // Using XSSF for xlsx format, for xls use HSSF
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();
            //read header
            if (rowIterator.hasNext()) {
                Row row = rowIterator.next();
            }
            DataFormatter dataFormatter = new DataFormatter();
            //iterating over each row
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                try {
                    String id = dataFormatter.formatCellValue(row.getCell(0));
                    String type = dataFormatter.formatCellValue(row.getCell(1));
                    String shortDesc = dataFormatter.formatCellValue(row.getCell(2));
                    String longDesc = dataFormatter.formatCellValue(row.getCell(3));
                    double cust =dataFormatter.formatCellValue(row.getCell(4)).isEmpty()?0: Double.valueOf(dataFormatter.formatCellValue(row.getCell(4)));
                    String cat = dataFormatter.formatCellValue(row.getCell(5));
                    Integer duration = Integer.getInteger(dataFormatter.formatCellValue(row.getCell(6)), 0);

                    Service ser = company.getServiceRegister().newService(id, shortDesc, longDesc, cust, company.getCategoryRegister().getCategorieById(cat), type);
                    if (ser instanceof FixedService) {
                        ((FixedService) ser).setDuration(duration);
                    }
                    company.getServiceRegister().registService(ser);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * *
     * Read Services from Excel and adds to Register
     *
     */
    public static void readRequests(Company company) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(SERVICEREQUEST_FILE);

            // Using XSSF for xlsx format, for xls use HSSF
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();
            //read header
            if (rowIterator.hasNext()) {
                Row row = rowIterator.next();
            }
            DataFormatter dataFormatter = new DataFormatter();
            //iterating over each row
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                try {
                    String id = dataFormatter.formatCellValue(row.getCell(0));
                    String vat = dataFormatter.formatCellValue(row.getCell(1));
                    String address = dataFormatter.formatCellValue(row.getCell(2));
                    String local = dataFormatter.formatCellValue(row.getCell(3));
                    String postalCode = dataFormatter.formatCellValue(row.getCell(4));
                    String cat = dataFormatter.formatCellValue(row.getCell(5));
                    String serID = dataFormatter.formatCellValue(row.getCell(6));
                    String desc = dataFormatter.formatCellValue(row.getCell(7));
                    int duration = row.getCell(8).getCellType() == CellType.NUMERIC ? (int)row.getCell(8).getNumericCellValue() :Integer.valueOf(dataFormatter.formatCellValue(row.getCell(8)));

                    Client cli = company.getClientRegister().getClientByVAT(vat);
                    Service serv = company.getServiceRegister().getServiceById(serID);
                    ServiceRequest sr = company.getServiceRequestRegister().newServiceRequest(cli.getEmail(), new PostalAddress(address, postalCode, local));
                    sr.addDescription(serv, desc, duration);

                    Date date1 = row.getCell(9).getDateCellValue();
                    Date time1 = row.getCell(10).getDateCellValue();
                    LocalDateTime localdate = Utils.getCalendar(date1, time1);
                    
                    if (localdate != null  ) {
                        sr.addSchedulePreference(localdate); 
                        System.out.println("lapr.project.gpsd.utils.ExcelReader.readRequests() - " + localdate.toString());
                    }

                    Date date2 = HSSFDateUtil.isCellDateFormatted(row.getCell(11))? row.getCell(11).getDateCellValue():null;
                    Date time2 = HSSFDateUtil.isCellDateFormatted(row.getCell(12))?row.getCell(12).getDateCellValue():null;
                    LocalDateTime localdate2 = Utils.getCalendar(date2, time2);
                    
                    
                    if (localdate2 != null  ) {
                        sr.addSchedulePreference(localdate2); 
                        System.out.println("lapr.project.gpsd.utils.ExcelReader.readRequests() - " + localdate2.toString());
                    }

                    double cust = Double.valueOf(dataFormatter.formatCellValue(row.getCell(13)));

                    company.getServiceRequestRegister().registerServiceRequest(sr);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void readExecutionOrders(Company company){
        List<String> lstFiles = new ArrayList<>();
        lstFiles.add(EXECUTIONORDERS1_FILE);
         lstFiles.add(EXECUTIONORDERS2_FILE);
          lstFiles.add(EXECUTIONORDERS3_FILE);
          
          for (String file : lstFiles) {
              readExecutionOrders(company,file);
        }
    }
    
     /**
     * *
     * Read Execution Orders from Excel and adds to Register
     *
     */
    private static void readExecutionOrders(Company company,String urlFile) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(urlFile);

            // Using XSSF for xlsx format, for xls use HSSF
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();
            //read header
            if (rowIterator.hasNext()) {
                Row row = rowIterator.next();
            }
            DataFormatter dataFormatter = new DataFormatter();
            //iterating over each row
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                try {
                    String[] str = urlFile.split("_");
                    
                   
                    
                    int id = Integer.valueOf(dataFormatter.formatCellValue(row.getCell(0)));;
                    String emailCli = dataFormatter.formatCellValue(row.getCell(1));
                    String distance = dataFormatter.formatCellValue(row.getCell(2));
                    String cat = dataFormatter.formatCellValue(row.getCell(3));
                    String serviceType = dataFormatter.formatCellValue(row.getCell(4));
                    Date dt = row.getCell(5).getDateCellValue();;
                    Date tm = row.getCell(6).getDateCellValue();;
                    String address = dataFormatter.formatCellValue(row.getCell(7));
                    String local = dataFormatter.formatCellValue(row.getCell(8));
                    String postalCode =dataFormatter.formatCellValue(row.getCell(9));

                     LocalDateTime localdate = Utils.getCalendar(dt, tm);
                    
                     ServiceProvider sp = company.getServiceProviderRegister().getServiceProviderByEmailPartial(str[1]);
                    Service serv = null;
                    int idRequest = 0;
                  

                    ExecutionOrder ex = new ExecutionOrder(id, idRequest, sp, serv, 0, emailCli);
                    company.getExecutionOrdersRegister().addExecutionOrder(ex);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * *
     * Read Ratings from Excel and adds to Register
     *
     */
    public static void readRatings(Company company) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(RATINGSERVICEPROVIDER_FILE);

            // Using XSSF for xlsx format, for xls use HSSF
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();
            //read header
            if (rowIterator.hasNext()) {
                Row row = rowIterator.next();
            }
            DataFormatter dataFormatter = new DataFormatter();
            //iterating over each row
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                try {
                    
                    int id = Integer.valueOf(dataFormatter.formatCellValue(row.getCell(0)));
                    int rating = Integer.valueOf(dataFormatter.formatCellValue(row.getCell(1)));

                    
                    
                     ExecutionOrder exec = company.getExecutionOrdersRegister().getexecutionOrderByID(id);
                     if(exec != null){
                         exec.getServiceProvider().addRating(new Rating(rating, id));
                     }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    public static void readFinishedServices(Company company){
        //TODO: falta modelo
     FileInputStream fis = null;
        try {
            fis = new FileInputStream(ENDSERVICE_FILE);

            // Using XSSF for xlsx format, for xls use HSSF
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();
            //read header
            if (rowIterator.hasNext()) {
                Row row = rowIterator.next();
            }
            DataFormatter dataFormatter = new DataFormatter();
            //iterating over each row
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                try {
                    
                    int id = Integer.valueOf(dataFormatter.formatCellValue(row.getCell(0)));
                    String problem = dataFormatter.formatCellValue(row.getCell(1));
                    String resolution = dataFormatter.formatCellValue(row.getCell(2));
                    Integer extraHours =Integer.getInteger(dataFormatter.formatCellValue(row.getCell(3)), null);
                    String avaliation = dataFormatter.formatCellValue(row.getCell(4));
                    
                     ExecutionOrder exec = company.getExecutionOrdersRegister().getexecutionOrderByID(id);
                     if(exec != null){
                         if(!problem.isEmpty())
                             exec.setHasIssue(problem);
                         exec.setIsEnded(true);
                     }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    }
    
    
    

}
