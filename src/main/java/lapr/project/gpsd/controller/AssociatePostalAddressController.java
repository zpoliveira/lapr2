/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.registers.ClientRegister;
import lapr.project.gpsd.utils.Constants;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class AssociatePostalAddressController {
    
    private Company company;
    private ApplicationGPSD appl;
    private UserSession session;
    private String email;
    private ClientRegister clientRegister;
    private Client client;
    private PostalAddress postalAddress;

    public AssociatePostalAddressController() {
        if(!ApplicationGPSD.getInstance().getActualSession()
                .isLoggedInWithRole(Constants.ROLE_CLIENT)){
            throw new IllegalStateException("User is not allowed");
        }
        this.company = ApplicationGPSD.getInstance().getCompany();
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }
       
    public Client getClient(){
        
        appl = ApplicationGPSD.getInstance();
        session = appl.getActualSession();
        email = session.getUserEmail();
        clientRegister = company.getClientRegister();
        this.client = clientRegister.getClientByEmail(email);
        return this.client;        
    }
    
    public PostalAddress newAddress(String address, String postalCode, String local){
        postalAddress = Client.newPostalAddress(address, postalCode, local);
        return postalAddress;
    }   
    
    public Boolean addAddress(PostalAddress postalAddress){
        this.getClient();
        return this.client.addAddress(postalAddress);
    }
    
}
