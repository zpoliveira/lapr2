package lapr.project.gpsd.controller;

import java.time.LocalDateTime;
import java.util.List;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.OtherCost;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.SchedulePreference;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceRequest;
import lapr.project.gpsd.model.ServiceRequestDescription;
import lapr.project.gpsd.model.registers.ServiceRequestRegister;
import lapr.project.gpsd.utils.Constants;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class ServiceRequestController {

    private final Company company;
    private ServiceRequest activeRequest;
    private final ServiceRequestRegister servRequestRegister;

    public ServiceRequestController() {

        if (ApplicationGPSD.getInstance().getActualSession()
                .isLoggedInWithRole(Constants.ROLE_ADMINISTRATIVE)
                || ApplicationGPSD.getInstance().getActualSession()
                        .isLoggedInWithRole(Constants.ROLE_CLIENT)) {

            this.company = ApplicationGPSD.getInstance().getCompany();
            this.servRequestRegister = company.getServiceRequestRegister();

        } else {
            throw new IllegalStateException("User is not allowed");
        }

    }

    public boolean newServiceRequest(
            String clientEmail,
            PostalAddress postalAddress) {
        this.activeRequest = this.servRequestRegister.newServiceRequest(
                clientEmail,
                postalAddress);

        return this.servRequestRegister.validateServiceRquest(activeRequest);
    }

    public boolean registerServiceRequest() {
        return this.servRequestRegister.registerServiceRequest(activeRequest);
    }

    public boolean addSchedulePreference(LocalDateTime cal) {
        return this.activeRequest.addSchedulePreference(cal);
    }

    public boolean addDescription(Service serv, String description, int duration) {
        return this.activeRequest.addDescription(serv, description, duration);
    }

    public List<SchedulePreference> getListSchedules() {
        return this.activeRequest.getLstSchedulePreference();
    }

    public List<ServiceRequestDescription> getDescriptionList() {
        return this.activeRequest.getServiceRequestDescriptions();
    }

    public List<OtherCost> getOtherCostList() {
        return this.activeRequest.getListOtherCost();
    }
    
    public boolean calculateCost(){
        return this.activeRequest.calculateCost(this.company);
    }
}
