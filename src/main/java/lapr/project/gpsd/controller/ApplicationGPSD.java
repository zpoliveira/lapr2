/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import lapr.project.auth.AuthFacade;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.RequestStatus;
import lapr.project.gpsd.model.ServiceRequest;
import lapr.project.gpsd.model.ServiceType;
import lapr.project.gpsd.model.TaskSchedule;
import lapr.project.gpsd.utils.Constants;
import lapr.project.gpsd.utils.ExcelReader;
import lapr.project.gpsd.utils.Utils;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * @author paulomaio
 */
public class ApplicationGPSD {

    private final Company company;
    private final AuthFacade auth;

    private ApplicationGPSD() {
        Properties props = getProperties();
       
        long taskInterval ,taskDelay;
        try {
            taskInterval = Long.parseLong(props.getProperty(Constants.PARAMS_COMPANY_TASKINTERVAL));
        } catch (NumberFormatException e) {
            taskInterval = 0;
        }
        try {
            taskDelay = Long.parseLong(props.getProperty(Constants.PARAMS_COMPANY_TASKDELAY));
        } catch (NumberFormatException e) {
            taskDelay = 0;
        }

        TaskSchedule taskSchedule = TaskSchedule.valueOf(props.getProperty(Constants.PARAMS_COMPANY_TASKSCHEDULE));
         this.company = new Company(props.getProperty(Constants.PARAMS_COMPANY_DESIGNATION),
                props.getProperty(Constants.PARAMS_COMPANY_VAT),taskDelay,taskInterval, taskSchedule);


        this.auth = this.company.getAuthFacade();
        this.newBootstrap();
        //Initialize task
        this.company.initTask();
    }

    public Company getCompany() {
        return this.company;
    }

    public UserSession getActualSession() {
        return this.auth.getCurrentSession();
    }

    public boolean doLogin(String strId, String strPwd) {
        return this.auth.doLogin(strId, strPwd) != null;
    }

    public void doLogout() {
        this.auth.doLogout();
    }

    /**
     * Adds default key values defined in utils constants class or in
     * config.properties file
     *
     * @return the Object props with the values that have been read
     */
    private Properties getProperties() {
        Properties props = new Properties();

        // adds default key values defined in utils constants class 
        //in case the next step entres in catch this will be assumed else it will be overrided
        props.setProperty(Constants.PARAMS_COMPANY_DESIGNATION, "Default Lda.");
        props.setProperty(Constants.PARAMS_COMPANY_VAT, "Default NIF");
         props.setProperty(Constants.PARAMS_COMPANY_TASKSCHEDULE, "FIFO");
        props.setProperty(Constants.PARAMS_COMPANY_TASKDELAY, "600000");
        props.setProperty(Constants.PARAMS_COMPANY_TASKINTERVAL, "600000");
       

        //adds key values difined in config.properties file the props Object
        try {
            InputStream in = new FileInputStream(Constants.PARAMS_FILE);
            props.load(in);
            in.close();
        } catch (Exception ex) {

        }
        return props;
    }

    private void newBootstrap() {
        this.auth.registerUserRole(Constants.ROLE_ADMINISTRATIVE);
        this.auth.registerUserRole(Constants.ROLE_CLIENT);
        this.auth.registerUserRole(Constants.ROLE_HR);
        this.auth.registerUserRole(Constants.ROLE_SERVICE_PROVIDER);

        Constants.SERVICE_TYPES.forEach((className, descriptor) -> {
            ServiceType serType = this.company.getServiceTypeRegister().newServiceType(className, descriptor);
            this.company.getServiceTypeRegister().registServiceType(serType);
        });

        Utils.readPostalCodesFromFile("codigopostal_alt_long.csv");
        this.company.getExternalService().setPostalCodes(Utils.postalCodes);

        ExcelReader.readGeograficalArea(this.company);
        ExcelReader.readCategories(this.company);

        ExcelReader.readApplicationsServiceProvider(this.company);
        ExcelReader.readClient(this.company);
        ExcelReader.readServiceProvider(this.company);
        ExcelReader.readServices(this.company);
        ExcelReader.readRequests(this.company);
        ExcelReader.readDisponibilities(this.company);
        ExcelReader.readExecutionOrders(this.company);
        ExcelReader.readRatings(this.company);
        ExcelReader.readFinishedServices(this.company);
        
        
        //FOR TEST
        this.auth.registerUserWithRole("FRH 1", "frh1@esoft.pt", "123456", Constants.ROLE_HR);
        this.auth.registerUserWithRoles("test", "test", "test", new String[]{Constants.ROLE_ADMINISTRATIVE, Constants.ROLE_CLIENT, Constants.ROLE_HR, Constants.ROLE_SERVICE_PROVIDER});
        Client test = new Client("test", "test", "test", "test");
        test.addAddress(new PostalAddress("Rua QQ", "4000-117", "Porto"));
        this.company.getClientRegister().addClient(test);
        ServiceRequest serviceRequest1 = new ServiceRequest("test", new PostalAddress("Rua QQ", "4000-117", "Porto"));
        serviceRequest1.setStatus(RequestStatus.ASSIGNED);
        this.company.getServiceRequestRegister().registerServiceRequest(serviceRequest1);
        ServiceRequest serviceRequest2 = new ServiceRequest("test", new PostalAddress("Rua d332Q", "4020-020", "Lisboa"));
        serviceRequest2.setStatus(RequestStatus.ASSIGNED);
        this.company.getServiceRequestRegister().registerServiceRequest(serviceRequest2);
        this.company.getCategoryRegister().registerCategory(new Category("1", "Cat1"));
    }

    // Inspirado em https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
    private static ApplicationGPSD singleton = null;

    public static ApplicationGPSD getInstance() {
        if (singleton == null) {
            synchronized (ApplicationGPSD.class) {
                singleton = new ApplicationGPSD();
            }
        }
        return singleton;
    }

}
