/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.Disponibility;
import lapr.project.gpsd.model.GeographicalArea;
import lapr.project.gpsd.model.SchedulePreference;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceRequest;
import lapr.project.gpsd.model.ServiceRequestDescription;
import lapr.project.gpsd.model.TaskSchedule;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class AssociateServiceProviderTask extends TimerTask {

    private Company company;

    public AssociateServiceProviderTask(Company company) {
        this.company = company;
    }

    /**
     * *
     * Assign Service Provider to Services
     */
    private void assignServiceProviderToServiceRequest() {
        List<ServiceRequest> lstServiceRequest = new ArrayList<>();
        boolean available = false;
        TaskSchedule taskSchedule = this.company.getTaskSchedule();
        switch (taskSchedule) {
            case FIFO:
                lstServiceRequest = this.company.getServiceRequestRegister().getResquestByNumber();
                break;
            case RANDOM:
            default:
                lstServiceRequest = this.company.getServiceRequestRegister().getResquestRandom();
        }

        for (int i = 0; i < lstServiceRequest.size() && !available; i++) {

            GeographicalArea geo = this.company.getGeographicalAreaRegister().getGeographicalAreaByPostalCode(lstServiceRequest.get(i).getPostalAddress().getPostalCode());

            if (geo != null) {

                for (ServiceRequestDescription serDesc : lstServiceRequest.get(i).getServiceRequestDescriptions()) {
                    Category cat = serDesc.getService().getCategory();
                    List<ServiceProvider> lstServiceProvider = this.company.getServiceProviderRegister().getServiceProviderByRatingDistanceName(geo, cat, lstServiceRequest.get(i).getPostalAddress().getPostalCode());

                    for (int iSP = 0; iSP < lstServiceProvider.size() && !available; iSP++) {
                        List<SchedulePreference> lstSchedulePrefrence = lstServiceRequest.get(i).getListSchedulePreferenceByOrderFilterByNow();

                        for (SchedulePreference sp : lstSchedulePrefrence) {
                            LocalDateTime dateIni = sp.getDateHourBegin();
                            LocalDateTime dateEnd = dateIni;
                            dateEnd = dateEnd.plusMinutes((long)serDesc.getDuration());
                            Disponibility dis = lstServiceProvider.get(iSP).getDisponibility(dateIni, dateEnd);

                            if (dis != null) {
                                available = true;
                                dis.setAvailable(available);
                                serDesc.setServiceProvider(lstServiceProvider.get(iSP));
                                serDesc.setDisponibility(dis);
                                serDesc.setStartDate(sp.getDateHourBegin());
                            }
                        }

                    }
                }

                //Validate if all services are assigned
                if (!available) {
                    for (ServiceRequestDescription serDesc : lstServiceRequest.get(i).getServiceRequestDescriptions()) {
                        if (serDesc.getServiceProvider() != null) {
                            serDesc.setServiceProvider(null);
                        }
                        if (serDesc.getDisponibility() != null) {
                            serDesc.getDisponibility().setAvailable(available);
                            serDesc.setDisponibility(null);
                        }
                    }
                }
            }

        }

    }

    @Override
    public void run() {

        if (this.company != null) {
            assignServiceProviderToServiceRequest();
        }
    }
}
