/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.registers.CategoryRegister;
import lapr.project.gpsd.utils.Constants;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class SpecifyCategoryController
{
    private Company company;
    private CategoryRegister cr;
    
    public SpecifyCategoryController(){
        if(!ApplicationGPSD.getInstance().getActualSession().isLoggedInWithRole(Constants.ROLE_ADMINISTRATIVE))
            throw new IllegalStateException("User not Authorized");
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.cr = this.company.getCategoryRegister();
    }
    
    public Category newCategory(String code, String description)
    {
        try
        {
            return new Category(code, description);
        }
        catch(RuntimeException ex)
        {
            return null;
        }
    }
   
    
    public boolean registerCategory(Category category)
    {
        return this.cr.registerCategory(category);
    }

}