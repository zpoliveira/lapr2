/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.time.LocalDateTime;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.Disponibility;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.utils.Constants;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class SpecifyDailyAvailabilityController {

    private Company company;
    private ServiceProvider sp;

    public SpecifyDailyAvailabilityController() {
        if (!ApplicationGPSD.getInstance().getActualSession().isLoggedInWithRole(Constants.ROLE_SERVICE_PROVIDER)) {
            throw new IllegalStateException("User not Authorized");
        }
        this.company = ApplicationGPSD.getInstance().getCompany();
    }

    public ServiceProvider getServiceProvider() {
        String email = ApplicationGPSD.getInstance().getActualSession().getUserEmail();
        this.sp = this.company.getServiceProviderRegister().getServiceProviderByEmail(email);
        return this.sp;
    }

    public boolean addDisponibility(ServiceProvider sp, LocalDateTime initialDate, LocalDateTime finalDate) {
        Disponibility disp = new Disponibility(initialDate, finalDate);
        return sp.addDisponibility(disp);
    }

}
