/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.GeographicalArea;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.registers.ApplicationRegister;
import lapr.project.gpsd.model.registers.CategoryRegister;
import lapr.project.gpsd.model.registers.GeographicalAreaRegister;
import lapr.project.gpsd.model.registers.ServiceProviderRegister;
import lapr.project.gpsd.utils.Constants;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class RegistServiceProviderController {

    private Company company;
    private CategoryRegister categoryRegister;
    private GeographicalAreaRegister geoAreaRegister;
    private ApplicationRegister applicationRegister;
    private ServiceProviderRegister spRegister;
    private ServiceProvider serviceProvider;

    public RegistServiceProviderController() {
        if (!ApplicationGPSD.getInstance().getActualSession().isLoggedInWithRole(Constants.ROLE_HR)) {
            throw new IllegalStateException("User is not allowed");
        }
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.categoryRegister = this.company.getCategoryRegister();
        this.geoAreaRegister = this.company.getGeographicalAreaRegister();
        this.applicationRegister = this.company.getApplicationRegister();
        this.spRegister = this.company.getServiceProviderRegister();
    }

    public Company getCompany() {
        return this.company;
    }

    public CategoryRegister getCategoryRegister() {
        return this.categoryRegister;
    }

    public GeographicalAreaRegister getGeoAreaRegister() {
        return this.geoAreaRegister;
    }

    public ApplicationRegister getApplicationRegister() {
        return this.applicationRegister;
    }

    public ServiceProviderRegister getSpRegister() {
        return this.spRegister;
    }
    
    public Application getApplicationByVat(String vatNumber){
        Application spApp = null;
        for(Application application : this.applicationRegister.getApplicationRegister()){
            if(application.getSPVATNumber().equals(vatNumber)){
                spApp = application;
            }
        }
        return spApp;
    }

    public boolean newServiceProvider(int numMec,
            String completName,
            String shortName,
            String email,
            String vatNum,
            String spPCode,
            List<GeographicalArea> geoAreaList,
            List<Category> catList) {
        try {
            this.serviceProvider = this.spRegister.newServiceProvider(numMec, completName, shortName, email, vatNum,spPCode);
            geoAreaList.forEach(geoArea -> this.serviceProvider.addGeographicalArea(geoArea));
            catList.forEach(cat -> this.serviceProvider.addCategory(cat));
        } catch (Exception e) {
            e.getCause().toString();
            this.serviceProvider = null;
        }
        return this.spRegister.validateServiceProvider(this.serviceProvider);
    }
    
    public boolean registServiceProvider() 
    {
        return this.spRegister.registServiceProvider(this.serviceProvider);
    }
    
    public void createAuthForServicerovider(String name, String email, String password, String role){
        this.company.getAuthFacade().registerUserWithRole(name, email, password, role);
    }
    
    
    public boolean registServiceProvider(ServiceProvider sp, String pwd) 
    {
        createAuthForServicerovider(sp.getShortName(), sp.getEmail(), pwd, Constants.ROLE_SERVICE_PROVIDER);
        return this.spRegister.registServiceProvider(this.serviceProvider);
    }

}
