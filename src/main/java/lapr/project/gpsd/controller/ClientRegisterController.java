/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.utils.Constants;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * @author paulomaio
 */
public class ClientRegisterController {

    private ApplicationGPSD app;
    private Company company;
    private Client client;
    private String pwd;

    public ClientRegisterController() {
        this.app = ApplicationGPSD.getInstance();
        this.company = app.getCompany();
    }

    public boolean newClient(String name, String vatNumber, String phone, String email, String pwd, List<PostalAddress> lstAddresses) throws Exception {
        try {
            this.pwd = pwd;
            this.client = this.company.getClientRegister().newClient(name, vatNumber, phone, email, lstAddresses);

            return this.validateClient(this.client, this.pwd);
        } catch (RuntimeException ex) {
            this.client = null;
            return false;
        }
    }

    public boolean addPostalAddress(String address, String postalCode, String place) {
        if (this.client != null) {
            try {
                
                PostalAddress morada = Client.newPostalAddress(address, postalCode, place);
                if (morada == null) {
                    return false;
                }
                return this.client.addAddress(morada);
            } catch (RuntimeException ex) {
                return false;
            }
        }
        return false;
    }

    public boolean createCliente() throws Exception {
        return this.createClient(this.client, this.pwd);
   }

    public String getClientString() {
        return this.client.toString();
    }

    public boolean createClient(Client client, String strPwd) throws Exception{
        if (this.validateClient(client, strPwd)) {
            if (this.company.getAuthFacade().registerUserWithRole(client.getName(), client.getEmail(), strPwd, Constants.ROLE_CLIENT)) {
                return this.company.getClientRegister().addClient(client);
            }
        }
        return false;
    }

    private boolean validateClient(Client client, String strPwd) throws Exception {
        boolean bRet = true;
        //validate if client exists
        if(this.company.getClientRegister().existsClient(client)){
            throw new Exception("Email or VAT Number already registed.");
        }
        
        //validade if list is null
        if(client.getAddresses() == null){
            throw new Exception("Is required one Postal Address.");
        }
        
        //validade if has postal addresses
        if(client.getAddresses().isEmpty()){
            throw new Exception("Is required one Postal Address.");
        }
        
        //Validate if postal code exists
        for(int i=0; i<client.getAddresses().size() && bRet;i++){
           bRet = this.company.getExternalService().isValidPostalCode(client.getAddresses().get(i).getPostalCode());
           if(!bRet)
               throw new Exception( "[" + client.getAddresses().get(i).getPostalCode() + "] is not a valid Postal Code.");
        }
        
        if (this.company.getAuthFacade().userExists(client.getEmail())) {
            throw new Exception("Email already registed.");
        }
        
        if (strPwd == null) {
             throw new Exception("Password invalid.");
        }
        
        if (strPwd.isEmpty()) {
             throw new Exception("Password invalid.");
        }

        return bRet;
    }

}