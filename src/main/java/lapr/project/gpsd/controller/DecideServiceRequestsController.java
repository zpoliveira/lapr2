/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ExecutionOrder;
import lapr.project.gpsd.model.RequestStatus;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceRequest;
import lapr.project.gpsd.utils.Constants;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * @author paulomaio
 */
public class DecideServiceRequestsController {

    private Company company;

    public DecideServiceRequestsController() {
        if (!ApplicationGPSD.getInstance().getActualSession().isLoggedInWithRole(Constants.ROLE_CLIENT)) {
            throw new IllegalStateException("User not Authorized");
        }
        this.company = ApplicationGPSD.getInstance().getCompany();
    }

    public List<ServiceRequest> getUserServiceRequests() {
        String email = ApplicationGPSD.getInstance().getActualSession().getUserEmail();
        List<ServiceRequest> serviceRequests = company.getServiceRequestRegister().getClientAssignedServiceRequests(email);
        return serviceRequests;
    }

    public boolean acceptServiceRequest(ServiceRequest sr) {
           if (sr.getServiceDescriptions().size() > 0) {
            for (int i = 0; i < sr.getServiceDescriptions().size(); i++) {
                ServiceProvider sp = sr.getServiceDescriptions().get(i).getServiceProvider();
                Service s = sr.getServiceDescriptions().get(i).getService();
                int duration = sr.getServiceDescriptions().get(i).getDuration();
                String clientEmail = sr.getClientEmail();
                ExecutionOrder eo = new ExecutionOrder(i, sr.getID(), sp, s, duration, clientEmail);
                this.company.getExecutionOrdersRegister().addExecutionOrder(eo);
            }
            sr.setStatus(RequestStatus.APPROVED);
            return true;
        } else {
            return false;
        }

    }

    public void rejectServiceRequest(ServiceRequest sr) {
        sr.setStatus(RequestStatus.NEW);
    }

}
