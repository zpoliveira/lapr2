/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.ActsIn;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ExecutionExportData;
import lapr.project.gpsd.model.ExecutionOrder;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceRequest;
import lapr.project.gpsd.model.ServiceRequestDescription;
import lapr.project.gpsd.model.ServiceType;
import lapr.project.gpsd.model.registers.ClientRegister;
import lapr.project.gpsd.model.registers.ExecutionOrdersRegister;
import lapr.project.gpsd.model.registers.ServiceProviderRegister;
import lapr.project.gpsd.model.registers.ServiceRequestRegister;
import lapr.project.gpsd.model.registers.ServiceTypeRegister;
import lapr.project.gpsd.utils.Constants;
import lapr.project.gpsd.utils.Utils;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class ConsultExecutionOrdersController {
    private Company company;
    private ServiceProviderRegister spRegister;
    private ExecutionOrdersRegister execOrdRegist;
    private ServiceRequestRegister serviceRequestRegist;
    private ServiceTypeRegister serviceTypeRegister;
    private ClientRegister clientRegister;
    private ServiceProvider serviceProvider;
    private List<ExecutionOrder> executionOrdersList = new ArrayList<>();
    public List<ExecutionExportData> executionsDataToExport = new ArrayList<>();

    public ConsultExecutionOrdersController() {
        if (!ApplicationGPSD.getInstance().getActualSession().isLoggedInWithRole(Constants.ROLE_SERVICE_PROVIDER)) {
            throw new IllegalStateException("User is not allowed");
        }
        this.company = ApplicationGPSD.getInstance().getCompany();
        String spEmail = this.company.getAuthFacade().getCurrentSession().getUserEmail();
        this.spRegister = this.company.getServiceProviderRegister();
        this.clientRegister = this.company.getClientRegister();
        this.execOrdRegist = this.company.getExecutionOrdersRegister();
        this.serviceRequestRegist = this.company.getServiceRequestRegister();
        this.serviceProvider = this.spRegister.getServiceProviderByEmail(spEmail);
        this.serviceTypeRegister = this.company.getServiceTypeRegister();
        if(this.execOrdRegist.getExecutionOrdersBySp(serviceProvider) != null){
            this.executionOrdersList = this.execOrdRegist.getExecutionOrdersBySp(serviceProvider);
        }
        this.generateExecutionOrdersExportData();
    }

    public Company getCompany() {
        return this.company;
    }

  
    private void generateExecutionOrdersExportData(){
        for(ExecutionOrder exeOrder : this.executionOrdersList){
            if(exeOrder.isIsEnded()){
                continue;
            }
            int serviceReqId = exeOrder.getServicerequestID();
            Service service = exeOrder.getService();
            Category servCat = service.getCategory();
            String serviceTypeName = this.getServiceTypeName(service);
            ServiceRequest serviceRequest = this.serviceRequestRegist.getServiceRequestById(serviceReqId);
            ServiceRequestDescription servReqDesc;
            servReqDesc = serviceRequest.getServiceRequestDescriptionByService(service);
            if(servReqDesc == null){
                continue;
            }
            String clientName = this.clientRegister.getClientByEmail(serviceRequest.getClientEmail()).getName();
            PostalAddress postalAddr = serviceRequest.getPostalAddress();
            String clientPostalCode = serviceRequest.getPostalAddress().getPostalCode();
            String spPostalCode = this.serviceProvider.getPostalCode();
            double latCli = Utils.postalCodes.get(clientPostalCode)[0];
            double logCli = Utils.postalCodes.get(clientPostalCode)[1];
            double latSp = Utils.postalCodes.get(spPostalCode)[0];
            double logSp = Utils.postalCodes.get(spPostalCode)[1];
            double distance = ActsIn.distance(latCli, logCli, latSp, logSp);
            LocalDateTime date = servReqDesc.getStartDate();
            this.executionsDataToExport.add(new ExecutionExportData(
                    exeOrder.getId(),
                    clientName, 
                    postalAddr, 
                    distance, 
                    servCat, 
                    serviceTypeName, 
                    date, 
                    exeOrder.isIsEnded(), 
                    exeOrder.getHasIssue()));
        }
    } 

    private String getServiceTypeName(Service service){
        String serviceTypeName = ""; 
        for (ServiceType serType : this.serviceTypeRegister.getServiceTypes() ){
            if(serType.getClassName().equalsIgnoreCase(service.getClass().getSimpleName())){
                serviceTypeName = serType.getDescriptor();
            } 
        
        } 
        return serviceTypeName;
    }
}
