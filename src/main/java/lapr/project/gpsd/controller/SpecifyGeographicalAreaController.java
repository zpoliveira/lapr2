package lapr.project.gpsd.controller;

import java.util.ArrayList;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.GeographicalArea;
import lapr.project.gpsd.model.registers.GeographicalAreaRegister;
import lapr.project.gpsd.utils.Constants;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class SpecifyGeographicalAreaController {

    private final Company company;
    private final GeographicalAreaRegister geoAreaRegister;

    public SpecifyGeographicalAreaController() {
        if (!ApplicationGPSD.getInstance().getActualSession()
                .isLoggedInWithRole(Constants.ROLE_ADMINISTRATIVE)) {
            throw new IllegalStateException("User is not allowed");
        } else {
            this.company = ApplicationGPSD.getInstance().getCompany();
            this.geoAreaRegister = company.getGeographicalAreaRegister();
        }
    }

    public boolean newGeographicalArea(String designation, float transitCost,
            float workingRadius, String postalCode) {

        GeographicalArea ga = this.geoAreaRegister.newGeographicalArea(designation, transitCost,
                workingRadius, postalCode, company.getExternalService());

        if (this.geoAreaRegister.validateGeographicalArea(ga)) {
            return this.registerGeoArea(ga);
        } else {
            return false;
        }

    }

    public boolean registerGeoArea(GeographicalArea ga) {
        return geoAreaRegister.registerGeographicalArea(ga);
    }

    public ArrayList<GeographicalArea> getAllGeoAreas() {
        return geoAreaRegister.getListGeoAreas();
    }
}
