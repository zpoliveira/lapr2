/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.FixedService;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.registers.CategoryRegister;
import lapr.project.gpsd.model.registers.ServiceRegister;
import lapr.project.gpsd.model.registers.ServiceTypeRegister;
import lapr.project.gpsd.utils.Constants;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class SpecifyServiceController {

    private Company company;
    private Service service;
    private CategoryRegister categoryRegister;
    private ServiceRegister serviceRegister;
    private ServiceTypeRegister serviceTypeRegister;

    public SpecifyServiceController() {
        if (!ApplicationGPSD.getInstance().getActualSession().isLoggedInWithRole(Constants.ROLE_ADMINISTRATIVE)) {
            throw new IllegalStateException("User is not allowed");
        }
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.categoryRegister = this.company.getCategoryRegister();
        this.serviceRegister = this.company.getServiceRegister();
        this.serviceTypeRegister = this.company.getServiceTypeRegister();
    }

    public List<Category> getCategories() {
        return this.categoryRegister.getCategories();
    }

    public List<String> getServiceTypesNames() {
        return this.serviceTypeRegister.getServiceTypesNames();
    }

    public boolean newService(String serviceId,
            String littleDescription,
            String largeDescription,
            double costHour,
            String catId,
            String type) {
        try {
            Category cat = this.categoryRegister.getCategorieById(catId);
            this.service = this.serviceRegister.newService(serviceId,
                    littleDescription,
                    largeDescription,
                    costHour,
                    cat,
                    type);
        } catch (Exception ex) {
            this.service = null;
        }
        return this.serviceRegister.validateService(this.service);
    }

    public void setDurationForFixedService(int duration, String serviceId) {
        Service ser = this.serviceRegister.getServiceById(serviceId);
       if(ser instanceof FixedService){
         ((FixedService) ser).setDuration(duration);
       }     
    }
    
    public void setDurationForFixedService(int duration) {
       if(this.service instanceof FixedService){
         ((FixedService) this.service).setDuration(duration);
       }     
    }

    public boolean registService() {
        return this.serviceRegister.registService(this.service);
    }
    
}
