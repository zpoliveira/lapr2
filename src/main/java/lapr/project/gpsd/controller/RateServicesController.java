/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ExecutionOrder;
import lapr.project.gpsd.model.Rating;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceRequest;
import lapr.project.gpsd.model.registers.ClientRegister;
import lapr.project.gpsd.utils.Constants;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class RateServicesController {
    
    private Company company;
    private ApplicationGPSD appl;
    private UserSession session;
    private String email;
    private ClientRegister clientRegister;
    private Client client;

    public RateServicesController() {
        
        if(!ApplicationGPSD.getInstance().getActualSession()
                .isLoggedInWithRole(Constants.ROLE_CLIENT)){
            throw new IllegalStateException("User is not allowed");
        }
        this.company = ApplicationGPSD.getInstance().getCompany();
    }
    
    public Client getClient(){
        
        this.appl = ApplicationGPSD.getInstance();
        this.session = this.appl.getActualSession();
        this.email = this.session.getUserEmail();
        this.clientRegister = this.company.getClientRegister();
        this.client = this.clientRegister.getClientByEmail(email);
        return this.client;  
        
    }
    
    public List<ExecutionOrder> getExecutionOrdersByClient(Client client){
        
        return this.company.getExecutionOrdersRegister().getExecutionOrdersByClient(client);
        
    }
    
    public ServiceRequest getServiceRequestByID(int id){
        return this.company.getServiceRequestRegister().getServiceRequestById(id);
    }
    
    public boolean rateService(ExecutionOrder executionOrder, int rating){
        
        ServiceProvider sp = executionOrder.getServiceProvider();
        Rating rat = new Rating(rating, executionOrder.getServicerequestID());
        
        return sp.addRating(rat);
        
    }
    
    
    
    
    
}
