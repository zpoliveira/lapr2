/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.auth.model.UserRole;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * @author paulomaio
 */
public class AuthController
{
    private ApplicationGPSD AppGpsd;
    
    public AuthController()
    {
        this.AppGpsd = ApplicationGPSD.getInstance();
    }
    
    public boolean doLogin(String strId, String strPwd)
    {
        return this.AppGpsd.doLogin(strId, strPwd);
    }
    
    public List<UserRole> getUserRoles()
    {
        if (this.AppGpsd.getActualSession().isLoggedIn())
        {
            return this.AppGpsd.getActualSession().getUserRoles();
        }
        return null;
    }

    public void doLogout()
    {
        this.AppGpsd.doLogout();
    }
    
    public ApplicationGPSD getApplicationGPSD(){
        return this.AppGpsd;
    }
}
