/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.utils.Constants;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class EvaluateServiceProviderController {
    
    private Company company;
    private ServiceProvider sp;
    
    public EvaluateServiceProviderController(){
        if (!ApplicationGPSD.getInstance().getActualSession().isLoggedInWithRole(Constants.ROLE_HR)) {
            throw new IllegalStateException("User not Authorized");
        }
        this.company = ApplicationGPSD.getInstance().getCompany();
    }
    
    public List<ServiceProvider> getServiceProviderList(){
        return this.company.getServiceProviderRegister().getServiceProviders();
    }
    
    public ServiceProvider getServiceProvider(String email){
        this.sp = this.company.getServiceProviderRegister().getServiceProviderByEmail(email);
        return sp;
    }
}
