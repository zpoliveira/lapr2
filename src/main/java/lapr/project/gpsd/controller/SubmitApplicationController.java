/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.registers.ApplicationRegister;
import lapr.project.gpsd.model.registers.CategoryRegister;


/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 * 
 */
public class SubmitApplicationController {
    
    private Application app;
    private CategoryRegister cr;
    private ApplicationGPSD AGPSD;
    private Company company;
    
    
    public SubmitApplicationController(){
        this.AGPSD = ApplicationGPSD.getInstance();
        this.company = this.AGPSD.getCompany();
        this.cr = this.company.getCategoryRegister();
        
    }

    public Application getApp() {
        return this.app;
    }
    
    public Application newApplication(String name, String VATNumber, String phoneNumber, String email, String address, String postalCode, String local){

        return this.app = company.getApplicationRegister().newApplication(name, VATNumber, phoneNumber, email, address, postalCode, local);
        
    }
    
    public void addPostalAddress(String address, String postalCode, String local){
        
        PostalAddress postalA = new PostalAddress(address, postalCode, local);
        
        this.app.addPostalAddress(postalA);
        
    }
    
    public void addAcademicQual(String designation, String degree, String classification){
        
        this.app.addAcademicQualification(designation, degree, classification);
        
    }
    
    public void addProfessionalQual(String description){
        
        this.app.addProfessionalQualification(description);
        
    }
    
    public void addSupDocument(String doc){
        
        this.app.addSuportDocument(doc);
        
    }
    
    public List<Category> getCategories(){
        
        cr = this.company.getCategoryRegister();
        
        return cr.getCategories();
        
    }
    
    public void addCategory(String idCat){
        
        Category cat = cr.getCategorieById(idCat);
        
        this.getApp().addCategory(cat);
    }
    
    private Boolean applicationValidator(Application apl){
        
        return this.company.getApplicationRegister().applicationValidator(apl);
        
    }
    
    public Boolean registerApplication(Application apl){
        
        Boolean flag = false;
        
        
        if(applicationValidator(apl)){
            ApplicationRegister appReg = this.company.getApplicationRegister();
           flag = appReg.registerApplication(apl);
        }
        
        return flag; 
        
    }
}
