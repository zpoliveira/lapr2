/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.auth.model;

import java.util.List;

/**
 *
 * @author paulomaio
 */
public class UserSession
{
    private User user = null;
    
    private UserSession()
    {
    }
    
    public UserSession(User oUser)
    {
        if (oUser == null)
            throw new IllegalArgumentException("Argument can't be null.");
        this.user = oUser;
    }
    
    public void doLogout()
    {
        this.user = null;
    }
    
    public boolean isLoggedIn()
    {
        return this.user != null;
    }
    
    public boolean isLoggedInWithRole(String role)
    {
        if (isLoggedIn())
        {
            return this.user.hasRole(role);
        }
        return false;
    }
    
    public String getUserName()
    {
        if (isLoggedIn()){
            return this.user.getName();
        }
        return null;
    }
    
    public String getUserId()
    {
        if (isLoggedIn()){
            return this.user.getId();
        }
        return null;
    }
    
    public String getUserEmail()
    {
        if (isLoggedIn()){
            return this.user.getEmail();
        }
        return null;
    }
    
    public List<UserRole> getUserRoles()
    {
        return this.user.getRoles();
    }
}
