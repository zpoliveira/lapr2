/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.auth.model;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author paulomaio
 */
public class UserRegister
{
    private Set<User> listUsers = new HashSet<>();
    
    
    public User newUser(String name, String email, String password)
    {
        return new User(name,email,password);
    }
    
    public boolean addUser(User user)
    {
        if (user != null){
         return this.listUsers.add(user);
        }
            
                   
        return false;
    }
    
    public boolean removeUser(User user)
    {
        if (user != null)
            return this.listUsers.remove(user);
        return false;
    }
    
    public User findUser(String strId)
    {
        for(User user: this.listUsers)
        {
            if(user.hasId(strId))
                return user;
        }
        return null;
    }
    
    public boolean hasUser(String strId)
    {
        User user = findUser(strId);
        if (user != null)
            return this.listUsers.contains(user);
        return false;
    }
    
    public boolean hasUser(User user)
    {
        return this.listUsers.contains(user);
    }
}
