/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.auth.model;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author paulomaio
 */
public class UserRoleRegister
{
    private final Set<UserRole> listRoles = new HashSet<>();
    
    public UserRole newUserRole(String role)
    {
        return new UserRole(role);
    }
    
    public UserRole newUserRole(String role, String description)
    {
        return new UserRole(role,description);
    }
    
    public boolean addRole(UserRole userRole)
    {
        if (userRole != null)
            return this.listRoles.add(userRole);
        return false;
    }
    
    public boolean removeRole(UserRole userRole)
    {
        if (userRole != null)
            return this.listRoles.remove(userRole);
        return false;
    }
    
    public UserRole findRole(String role)
    {
        for(UserRole r: this.listRoles)
        {
            if(r.hasId(role))
                return r;
        }
        return null;
    }
    
    public boolean hasRole(String role)
    {
        UserRole userRole = findRole(role);
        if (userRole != null)
            return this.listRoles.contains(userRole);
        return false;
    }
    
    public boolean hasRole(UserRole userRole)
    {
        return this.listRoles.contains(userRole);
    }
}
