package lapr.project.auth.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author paulomaio
 */
public class User
{
    private String name;
    private String email;
    private String password; // Shouldn't save password in plain text
    private Set<UserRole> userRoles = new HashSet<>();
    
    public User(String name, String email, String password)
    {
    
        if ( (name == null) || (email == null) || (password == null) || (name.isEmpty()) || (email.isEmpty()) || (password.isEmpty()))
            throw new IllegalArgumentException("No argument may be null.");
        
        this.name = name;
        this.email = email;
        this.password = password;        
    }
    
    public String getId()
    {
        return this.email;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public String getEmail()
    {
        return this.email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }
    
    
    public boolean hasId(String strId)
    {
        return this.email.equals(strId);
    }
    
    public boolean hasPassword(String strPwd)
    {
        return this.password.equals(strPwd);
    }
    
    public boolean addRole(UserRole role)
    {
        if (role != null)
            return this.userRoles.add(role);
        return false;
    }
    
    
    public boolean removeRole(UserRole role)
    {
        if (role != null)
            return this.userRoles.remove(role);
        return false;
    }
    
    public boolean hasRole(UserRole role)
    {
        return this.userRoles.contains(role);
    }
    
    public boolean hasRole(String strRole)
    {
        for(UserRole papel: this.userRoles)
        {
            if (papel.hasId(strRole))
                return true;
        }
        return false;
    }
    
    public List<UserRole> getRoles()
    {
        List<UserRole> list = new ArrayList<>();
        for(UserRole role: this.userRoles)
            list.add(role);
        return list;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.email);
        return hash;
    }
    
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        User obj = (User) o;
        return Objects.equals(this.email, obj.email);
    }
    
    @Override
    public String toString()
    {
        return String.format("%s - %s", this.name, this.email);
    }
}
