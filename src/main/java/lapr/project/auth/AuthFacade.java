/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.auth;

import lapr.project.auth.model.User;
import lapr.project.auth.model.UserRegister;
import lapr.project.auth.model.UserRole;
import lapr.project.auth.model.UserRoleRegister;
import lapr.project.auth.model.UserSession;

/**
 *
 * @author paulomaio
 */
public class AuthFacade {

    private UserSession session = null;

    private final UserRoleRegister listRoles = new UserRoleRegister();
    private UserRegister listUsers = new UserRegister();

    public boolean registerUserRole(String role) {
        UserRole papel = this.listRoles.newUserRole(role);
        return this.listRoles.addRole(papel);
    }

    public boolean registerUserRole(String role, String description) {
        UserRole papel = this.listRoles.newUserRole(role, description);
        return this.listRoles.addRole(papel);
    }

    public boolean registerUser(String name, String email, String password) {
        User user = this.listUsers.newUser(name, email, password);
        return this.listUsers.addUser(user);
    }

    public boolean registerUserWithRole(String name, String email, String password, String role) {
        UserRole userRole = this.listRoles.findRole(role);
        User user = this.listUsers.newUser(name, email, password);
        user.addRole(userRole);
        return this.listUsers.addUser(user);
    }

    public boolean registerUserWithRoles(String name, String email, String password, String[] roles) {
        User user = this.listUsers.newUser(name, email, password);
        for (String role : roles) {
            UserRole userRole = this.listRoles.findRole(role);
            user.addRole(userRole);
        }

        return this.listUsers.addUser(user);
    }

    public boolean userExists(String strId) {
        return this.listUsers.hasUser(strId);
    }

    public UserSession doLogin(String strId, String strPwd) {
        User utlz = this.listUsers.findUser(strId);
        if (utlz != null && utlz.hasPassword(strPwd)) {
            this.session = new UserSession(utlz);
        }

        return getCurrentSession();
    }

    public UserSession getCurrentSession() {
        return this.session;
    }

    public void doLogout() {
        if (this.session != null) {
            this.session.doLogout();
        }
        this.session = null;
    }
}
