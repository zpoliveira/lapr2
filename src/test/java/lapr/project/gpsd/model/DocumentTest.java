/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class DocumentTest {
    
    private Document doc;
    
    public DocumentTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        doc = new Document("doc");
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getDocument method, of class Document.
     */
    @Test
    public void testGetDocument() {
        System.out.println("getDocument");
        String expResult = "doc";
        String result = doc.getDocument();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDocument method, of class Document.
     */
    @Test
    public void testSetDocument() {
        System.out.println("setDocument");
        String document = "otherDoc";
        doc.setDocument(document);
        assertEquals(doc.getDocument(), document);
    }

    /**
     * Test of hashCode method, of class Document.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = 100125;
        int result = doc.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Document.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        boolean expResult = false;
        boolean result = doc.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Document.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Document: doc";
        String result = doc.toString();
        assertEquals(expResult, result);
    }
    
}
