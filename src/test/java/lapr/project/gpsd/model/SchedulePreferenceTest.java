package lapr.project.gpsd.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SchedulePreferenceTest {

    private final SchedulePreference sched;
    private final LocalDateTime now;

    public SchedulePreferenceTest() {
        this.now = LocalDateTime.now();
        this.sched = new SchedulePreference(1, now);
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {

    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getOrder method, of class SchedulePreference.
     */
    @Test
    public void testGetOrder() {
        System.out.println("getOrder");
        int expResult = 1;
        int result = this.sched.getOrder();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDateHourBegin method, of class SchedulePreference.
     */
    @Test
    public void testGetDateHourBegin() {
        System.out.println("getDateHourBegin");
        LocalDateTime expResult = now;
        LocalDateTime result = this.sched.getDateHourBegin();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDate method, of class SchedulePreference.
     */
    @Test
    public void testGetDate() {
        System.out.println("getDate");
         DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String expResult = now.format(formatter);
        String result = this.sched.getDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTime method, of class SchedulePreference.
     */
    @Test
    public void testGetTime() {
        System.out.println("getTime");
         DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        String expResult = now.format(formatter);
                
        String result = this.sched.getTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class SchedulePreference.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = sched;
        SchedulePreference instance = sched;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

}
