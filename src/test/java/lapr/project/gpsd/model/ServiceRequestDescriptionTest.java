///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package lapr.project.gpsd.model;
//
//import java.lang.reflect.Constructor;
//import java.lang.reflect.InvocationTargetException;
//import java.time.LocalDateTime;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.AfterEach;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
///**
// *
// * @author USER-Admin
// */
//public class ServiceRequestDescriptionTest {
//
//    public ServiceRequestDescription serviceRequestDescription;
//    private Service serv;
//
//    public ServiceRequestDescriptionTest() {
//
//    }
//
//    @BeforeAll
//    public static void setUpClass() {
//    }
//
//    @AfterAll
//    public static void tearDownClass() {
//    }
//
//    @BeforeEach
//    public void setUp() throws Exception {
//        String serviceType = "sErvice";
//        Class serviceTypeClass = Class.forName("lapr.project.gpsd.model." + serviceType.replaceAll(" ", ""));
//        Constructor construtor = serviceTypeClass.getConstructor(
//                String.class,
//                String.class,
//                String.class,
//                double.class,
//                Category.class
//        );
//        serv = (Service) construtor.newInstance(
//                "id",
//                "lilDesc",
//                "largDec",
//                20.0,
//                new Category("c", "c"));
//        this.serviceRequestDescription = new ServiceRequestDescription(serv, "desc", 1);
//    }
//
//    @AfterEach
//    public void tearDown() {
//    }
//    
//    @Test
//    public void testGetSet(){
//        System.out.println("getSet");
//        
//        this.serviceRequestDescription.setCost(21.0);
//        this.serviceRequestDescription.setDuration(2);
//        LocalDateTime x = LocalDateTime.now();
//        this.serviceRequestDescription.setStartDate(x);
//        Service res = this.serviceRequestDescription.getService();
//        
//        double cost = this.serviceRequestDescription.getCost();
//        int duration = this.serviceRequestDescription.getDuration();
//        LocalDateTime res2 = this.serviceRequestDescription.getStartDate();
//        
//        
//        
//        
//        
//        
//        assertEquals(21.0,cost);
//        assertEquals(2,duration);
//        assertEquals(serv, res);
//        assertEquals(x, res2);
//    }
//
//}
