/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ProfessionalQualificationTest {
    
    public ProfessionalQualificationTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getDescription method, of class ProfessionalQualification.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        ProfessionalQualification instance = new ProfessionalQualification("ProfQual");
        String expResult = "ProfQual";
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDescription method, of class ProfessionalQualification.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "otherQual";
        ProfessionalQualification instance = new ProfessionalQualification("ProfQual");
        instance.setDescription(description);
        assertEquals(description, instance.getDescription());
    }

    /**
     * Test of hashCode method, of class ProfessionalQualification.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        ProfessionalQualification instance = new ProfessionalQualification("ProfQual");
        int expResult = -938325497;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ProfessionalQualification.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        ProfessionalQualification instance = new ProfessionalQualification("ProfQual");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class ProfessionalQualification.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        ProfessionalQualification instance = new ProfessionalQualification("ProfQual");
        String expResult = "Professional Qualification: description: ProfQual";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
