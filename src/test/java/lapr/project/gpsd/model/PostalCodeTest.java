/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class PostalCodeTest {
    private static final double DELTA = 1e-15;
    private PostalCode pc; 
    
    public PostalCodeTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        this.pc = new PostalCode("4000-001", 41.3, 8.4);
    }
    
    @AfterEach
    public void tearDown() {
    }


    /**
     * Test of toString method, of class PostalCode.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "4000-001";
        String result = this.pc.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class PostalCode.
     */
    @Test
    public void testEqualsNull() {
        System.out.println("equals");
        Object obj = null;
        boolean expResult = false;
        boolean result = pc.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class PostalCode.
     */
    @Test
    public void testEqualsOtherPostalCode() {
        System.out.println("equals");
        PostalCode pc2 = new PostalCode("2000-002", 41.6, 8.9);
        boolean expResult = false;
        boolean result = pc.equals(pc2);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class PostalCode.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = pc;
        PostalCode instance = pc;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPostalCode method, of class PostalCode.
     */
    @Test
    public void testGetPostalCode() {
        System.out.println("getPostalCode");
        PostalCode instance = pc;
        String expResult = "4000-001";
        String result = instance.getPostalCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLatitude method, of class PostalCode.
     */
    @Test
    public void testGetLatitude() {
        System.out.println("getLatitude");
        PostalCode instance = pc;
        double expResult = 41.3;
        double result = instance.getLatitude();
        assertEquals(expResult, result, DELTA);
    }

    /**
     * Test of getLongitude method, of class PostalCode.
     */
    @Test
    public void testGetLongitude() {
        System.out.println("getLongitude");
        PostalCode instance = pc;
        double expResult = 8.4;
        double result = instance.getLongitude();
        assertEquals(expResult, result, DELTA);
    }
    
}
