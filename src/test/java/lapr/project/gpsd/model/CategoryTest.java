/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class CategoryTest {
     
    private Category c;
    private Category cCopy;
    private Category cNull;
    private String expectedCode = "code";
    private String code;
    
    @Test
    public void testEquals(){
        this.c = new Category("code","description");
        this.cCopy = new Category("code", "description");
        this.cNull = null;
        this.code = this.c.getCatId();
        assertEquals(this.code,expectedCode);
        assertEquals(c,cCopy);
        assertNotEquals(c,cNull);
        
    }
    
    @Test
    public void testHasId(){
        this.c = new Category("code","description");
        boolean idTrue = true;
        boolean id = this.c.hasId("code");
        boolean idF = this.c.hasId("code1");
        assertEquals(idTrue, id);
        assertNotEquals(idTrue, idF);
    }
    
}
