/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ServiceTypeTest {
    
    private ServiceType st;
    
    public ServiceTypeTest(){
        
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
       this.st = new ServiceType("class","desc");
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void testGetSet(){
        System.out.println("getSet");
        
        String res1 = st.getClassName();
        String res2 = st.getDescriptor();
        
        assertEquals("class", res1);
        assertEquals("desc",res2);
    }
    
}
