/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class PostalAddressTest {
    
    private PostalAddress postalA;
    private PostalAddress postalB;
    
    public PostalAddressTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        
        postalA = new PostalAddress("Rua", "4000-000", "Porto");
        postalB = new PostalAddress("Ave", "5000-100", "Maia");
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of equals method, of class PostalAddress.
     */
    @Test
    public void testEqualsNull() {
        System.out.println("equals");
        Object o = null;
        boolean expResult = false;
        boolean result = postalA.equals(o);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class PostalAddress.
     */
    @Test
    public void testEqualsOtherPostalAddress() {
        System.out.println("equals");
        boolean expResult = false;
        boolean result = postalA.equals(postalB);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAddress method, of class PostalAddress.
     */
    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
        String expResult = "Rua";
        String result = postalA.getAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAddress method, of class PostalAddress.
     */
    @Test
    public void testSetAddress() {
        System.out.println("setAddress");
        String address = "Praça";
        postalA.setAddress(address);
        
        assertEquals(postalA.getAddress(), address);
        
    }

    /**
     * Test of getPostalCode method, of class PostalAddress.
     */
    @Test
    public void testGetPostalCode() {
        System.out.println("getPostalCode");
        String expResult = "4000-000";
        String result = postalA.getPostalCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPostalCode method, of class PostalAddress.
     */
    @Test
    public void testSetPostalCode() {
        System.out.println("setPostalCode");
        String postalCode = "1000-000";
        postalA.setPostalCode(postalCode);
        assertEquals(postalA.getPostalCode(), postalCode);
        
    }

    /**
     * Test of getPlace method, of class PostalAddress.
     */
    @Test
    public void testGetPlace() {
        System.out.println("getPlace");
        String expResult = "Porto";
        String result = postalA.getPlace();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPlace method, of class PostalAddress.
     */
    @Test
    public void testSetPlace() {
        System.out.println("setPlace");
        String place = "Gaia";
        postalA.setPlace(place);
        assertEquals(postalA.getPlace(), place);
    }

    /**
     * Test of hashCode method, of class PostalAddress.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = -518423219;
        int result = postalA.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class PostalAddress.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Rua - 4000-000 - Porto";
        String result = postalA.toString();
        assertEquals(expResult, result);
    }
    
}
