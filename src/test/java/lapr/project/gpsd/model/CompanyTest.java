/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import lapr.project.auth.AuthFacade;
import lapr.project.gpsd.model.registers.ApplicationRegister;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class CompanyTest {
    
    private Company AGPSD;
    
    public CompanyTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        AGPSD = new Company("AGPSD", "123456789");
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getAuthFacade method, of class Company.
     */
    @Test
    public void testGetAuthFacade() {
        System.out.println("getAuthFacade");
        AuthFacade expResult = new AuthFacade();
        AuthFacade result = AGPSD.getAuthFacade();
        assertEquals(expResult.getCurrentSession(), result.getCurrentSession());
    }

    /**
     * Test of getCompanyDescription method, of class Company.
     */
    @Test
    public void testGetCompanyDescription() {
        System.out.println("getCompanyDescription");
        String expResult = "AGPSD";
        String result = AGPSD.getCompanyDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCompanyVatNum method, of class Company.
     */
    @Test
    public void testGetCompanyVatNum() {
        System.out.println("getCompanyVatNum");
        String expResult = "123456789";
        String result = AGPSD.getCompanyVatNum();
        assertEquals(expResult, result);
    }


    /**
     * Test of getApplicationRegister method, of class Company.
     */
    @Test
    public void testGetApplicationRegister() {
        System.out.println("getApplicationRegister");
        ApplicationRegister expResult = new ApplicationRegister();
        ApplicationRegister result = AGPSD.getApplicationRegister();
        assertEquals(expResult, result);
    }


    /**
     * Test of newApplication method, of class Company.
     */
    @Test
    public void testNewApplication() {
        System.out.println("newApplication");
        String SPName = "Name";
        String SPVATNumber = "987654321";
        String SPPhoneNumber = "933333333";
        String SPEmail = "z@zzz.zz";
        PostalAddress postalA = new PostalAddress("Rua", "4000-000", "Porto");
        Application expResult = new Application(SPName, SPVATNumber, SPPhoneNumber, SPEmail, postalA);
        Application result = AGPSD.getApplicationRegister().newApplication(SPName, SPVATNumber, SPPhoneNumber, SPEmail, "Rua", "4000-000", "Porto");
        assertEquals(expResult, result);
    }
    
}
