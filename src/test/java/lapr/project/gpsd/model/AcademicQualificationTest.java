/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class AcademicQualificationTest {
    
    private static AcademicQualification academicQualification;
    
    public AcademicQualificationTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {

    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {    
        academicQualification = new AcademicQualification("Master in Software Engineering", "Master Desgree", "Excelente");

    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getDesignation method, of class AcademicQualification.
     */
    @Test
    public void testGetDesignation() {
        System.out.println("getDesignation");
        String expResult = "Master in Software Engineering";
        String result = academicQualification.getDesignation();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDegree method, of class AcademicQualification.
     */
    @Test
    public void testGetDegree() {
        System.out.println("getDegree");
        String expResult = "Master Desgree";
        String result = academicQualification.getDegree();
        assertEquals(expResult, result);
    }

    /**
     * Test of getClassification method, of class AcademicQualification.
     */
    @Test
    public void testGetClassification() {
        System.out.println("getClassification");
        String expResult = "Excelente";
        String result = academicQualification.getClassification();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDesignation method, of class AcademicQualification.
     */
    @Test
    public void testSetDesignation() {
        System.out.println("setDesignation");
        String designation = "Master otherMasterDegree";
        academicQualification.setDesignation(designation);
        assertEquals(designation, academicQualification.getDesignation());
    }

    /**
     * Test of setDegree method, of class AcademicQualification.
     */
    @Test
    public void testSetDegree() {
        System.out.println("setDegree");
        String degree = "otherDegree";
        academicQualification.setDegree(degree);
        assertEquals(degree, academicQualification.getDegree());
    }

    /**
     * Test of setClassification method, of class AcademicQualification.
     */
    @Test
    public void testSetClassification() {
        System.out.println("setClassification");
        String classification = "otherClassification";
        academicQualification.setClassification(classification);
        assertEquals(classification, academicQualification.getClassification());
    }

    /**
     * Test of hashCode method, of class AcademicQualification.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = 1667579720;
        int result = academicQualification.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class AcademicQualification.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        boolean expResult = false;
        boolean result = academicQualification.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class AcademicQualification.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "AcademicQualification: Designation: Master in Software Engineering | Degree: Master Desgree | Classification: Excelente";
        String result = academicQualification.toString();
        assertEquals(expResult, result);
    }
    
}
