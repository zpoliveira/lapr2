/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.ExternalService;
import lapr.project.gpsd.model.GeographicalArea;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class GeographicalAreaRegisterTest {
    
    private GeographicalAreaRegister gaR;
    
    private GeographicalArea ga;

    public GeographicalAreaRegisterTest() {

    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        this.gaR = new GeographicalAreaRegister();
        ga = new GeographicalArea("ga2",20.5, 12, "2", new ExternalService());
        
        this.gaR.getListGeoAreas().add(ga);
      
    }

    @AfterEach
    public void tearDown() {
    }
    
//    @Test
//    public void testRegisterGA(){
//        System.out.println("testRegisterGA");
//        boolean expF = false;
//        boolean expT = true;
//        
//        GeographicalArea gaNew = new GeographicalArea("ga3",20.5, 12, "2", new ExternalService());
//        
//        boolean res = this.gaR.registerGeographicalArea(gaNew);
//        //boolean res2 = this.gaR.registerGeographicalArea(gaNew);
//        
//        assertEquals(expF,res);
//        //assertEquals(expT,res2);
//        
//    }
    
    @Test
    public void testGetList(){
        System.out.println("tesdtGetList");
        List<GeographicalArea> exp = new ArrayList<>();
        exp.add(ga);
        List<GeographicalArea> areas = this.gaR.getListGeoAreas();
        assertEquals(exp,areas);
    }

}
