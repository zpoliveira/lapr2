/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.PostalAddress;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ApplicationRegisterTest {
    
    private List<Application> appList;
    private ApplicationRegister appRegister;
    private Application app; 
    
    public ApplicationRegisterTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        
        appRegister = new ApplicationRegister();
        appList = new ArrayList<Application>();
        app = new Application("ZP", "123456789", "910000000", "a@aaa.aa", new PostalAddress("Rua", "4000-000", "Porto"));
        appList.add(app);
        appRegister.getApplicationRegister().add(app);
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getApplicationRegister method, of class ApplicationRegister.
     */
    @Test
    public void testGetApplicationRegister() {
        System.out.println("getApplicationRegister");
        List<Application> expResult = appList;
        List<Application> result = appRegister.getApplicationRegister();
        assertEquals(expResult, result);
    }

    /**
     * Test of setApplicationRegister method, of class ApplicationRegister.
     */
    @Test
    public void testSetApplicationRegister() {
        System.out.println("setApplicationRegister");
        List<Application> applicationRegister = null;
        appRegister.setApplicationRegister(applicationRegister);
        assertEquals(appRegister.getApplicationRegister(), null);
    }

    /**
     * Test of hashCode method, of class ApplicationRegister.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = -1525249282;
        int result = appRegister.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ApplicationRegister.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        boolean expResult = false;
        boolean result = appRegister.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class ApplicationRegister.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "ApplicationRegister: [Application: Name: ZP\n" +
        "VAT Number: 123456789\n" +
        "Phone Number: 910000000\n" +
        "E-mail: a@aaa.aa\n" +
        "Postal address: [Rua - 4000-000 - Porto]\n" +
        "Academic qualification: []\n" +
        "Professional qualification: []\n" +
        "Categories: []]";
        String result = appRegister.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of applicationValidator method, of class ApplicationRegister.
     */
    @Test
    public void testApplicationValidatorTrue() {
        System.out.println("applicationValidatorFalse");
        boolean expResult = false;
        boolean result = appRegister.applicationValidator(app);
        assertEquals(expResult, result);
    }
    
    /**
     * Second Test of applicationValidator method, of class ApplicationRegister.
     */
    @Test
    public void testApplicationValidatorFalse() {
        System.out.println("applicationValidatorTrue");
        Application app2 = new Application("Outro", "111111111", "930000000", "b@bbb.bb", new PostalAddress("Avenida", "2000-000", "Outra"));
        boolean expResult = true;
        boolean result = appRegister.applicationValidator(app2);
        assertEquals(expResult, result);
    }

    /**
     * Test of registerApplication method, of class ApplicationRegister.
     */
    @Test
    public void testRegisterApplication() {
        System.out.println("registerApplication");
        Application app = new Application("Outro", "111111111", "930000000", "b@bbb.bb", new PostalAddress("Avenida", "2000-000", "Outra"));;
        appRegister.registerApplication(app);
        assertEquals(appRegister.getApplicationRegister().size(), 2);
    }
    
}
