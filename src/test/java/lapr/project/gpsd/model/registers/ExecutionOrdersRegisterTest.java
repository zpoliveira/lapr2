/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.controller.ApplicationGPSD;
import lapr.project.gpsd.controller.RateServicesController;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ExecutionOrder;
import lapr.project.gpsd.model.FixedService;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceProvider;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ExecutionOrdersRegisterTest {
    
    private ExecutionOrdersRegister execOrdersRegister;
    private Company company;
    private ServiceProvider sp;
    private Service ser;
    private Category cat;
    private ExecutionOrder execOrder;
    private RateServicesController rateServicesController;
    
    public ExecutionOrdersRegisterTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");
        this.execOrdersRegister = new ExecutionOrdersRegister();
        this.rateServicesController = new RateServicesController();
        this.sp = new ServiceProvider(1, "ZePedro", "ZP","zp@zp.pt","123456789", "3333");
        this.cat = new Category("1", "Food");
        this.ser = new FixedService("1", "Cook", "Restaurant", 30.0, cat);
        this.execOrder = new ExecutionOrder(1, 1, sp, ser, 2, "test");
        
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getExecutionOrdersregister method, of class ExecutionOrdersRegister.
     */
    @Test
    public void testGetExecutionOrdersregister() {
        System.out.println("getExecutionOrdersregister");
        List<ExecutionOrder> expResult = null;
        List<ExecutionOrder> result = this.execOrdersRegister.getExecutionOrdersregister();
        Assertions.assertNotNull(result);
    }

    /**
     * Test of getExecutionOrdersByClient method, of class ExecutionOrdersRegister.
     */
    @Test
    public void testGetExecutionOrdersByClient() {
        System.out.println("getExecutionOrdersByClient");
        List<ExecutionOrder> expResult = new ArrayList<ExecutionOrder>();
        expResult.add(execOrder);
        execOrdersRegister.addExecutionOrder(execOrder);
        Client cli = rateServicesController.getClient();
        List<ExecutionOrder> result = execOrdersRegister.getExecutionOrdersByClient(cli);
        assertEquals(expResult, result);
    }

    /**
     * Test of addExecutionOrder method, of class ExecutionOrdersRegister.
     */
    @Test
    public void testAddExecutionOrderFalse() {
        System.out.println("addExecutionOrder");
        boolean expResult = false;
        boolean first = execOrdersRegister.addExecutionOrder(execOrder);
        boolean result = execOrdersRegister.addExecutionOrder(execOrder);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of addExecutionOrder method, of class ExecutionOrdersRegister.
     */
    @Test
    public void testAddExecutionOrdertrue() {
        System.out.println("addExecutionOrder");
        boolean expResult = true;
        boolean result = execOrdersRegister.addExecutionOrder(execOrder);
        assertEquals(expResult, result);
    }
    
}
