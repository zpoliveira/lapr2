/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model.registers;

import lapr.project.gpsd.model.Client;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ClientRegisterTest {
    
    private Client cli;
    
    public ClientRegisterTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
         cli = new Client("Joao", "123456789", "987654321", "mail@mail.com");
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getClientByEmail method, of class ClientRegister.
     */
    @Test
    public void testGetClientByEmail() {
        System.out.println("getClientByEmail");
        String email = "mail@mail.com";
        ClientRegister instance = new ClientRegister();
        instance.addClient(cli);
        Client expResult = cli;
        Client result = instance.getClientByEmail(email);
        assertEquals(expResult, result);
    }

    /**
     * Test of newClient method, of class ClientRegister.
     */
    @Test
    public void testNewClient() {
        System.out.println("newClient");
        String name = "Raquel";
        String vatName = "123498789";
        String phone = "255582628";
        String email = "raquel@mail.com";
        ClientRegister instance = new ClientRegister();
        Client expResult = new Client(name, vatName, phone, email);
        Client result = instance.newClient(name, vatName, phone, email);
        assertEquals(expResult, result);
    }

    /**
     * Test of addClient method, of class ClientRegister.
     */
    @Test
    public void testAddClient() {
        System.out.println("addClient");
        Client client = cli;
        ClientRegister instance = new ClientRegister();
        boolean expResult = true;
        boolean result = instance.addClient(client);
        assertEquals(expResult, result);
    }
    
}
