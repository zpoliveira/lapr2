/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ExternalServiceTest {

    private ExternalService extService;
    private HashMap<String, Double[]> hashPostalCodes;
    private PostalCode pc1;
    List<PostalCode> lstPostalCodes;

    public ExternalServiceTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        hashPostalCodes = new HashMap<>();
        hashPostalCodes.put("4000-000", new Double[]{8.8, 4.6});
        hashPostalCodes.put("4000-001", new Double[]{8.6, 0.6});
        hashPostalCodes.put("4000-002", new Double[]{4.6, 7.6});
        extService = new ExternalService(hashPostalCodes);
        pc1 = new PostalCode("4000-000", 8.8, 4.6);
        lstPostalCodes = new ArrayList<>();
        lstPostalCodes.add(pc1);
        lstPostalCodes.add(new PostalCode("4000-001", 8.6, 0.6));
        lstPostalCodes.add(new PostalCode("4000-002", 4.6, 7.6));

    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getPostalCode method, of class ExternalService.
     */
    @Test
    public void testGetPostalCode() {
        System.out.println("getPostalCode");
        String postalCode = "4000-000";
        ExternalService instance = extService;
        PostalCode expResult = new PostalCode("4000-000", 8.8, 4.6);
        PostalCode result = instance.getPostalCode(postalCode);
        assertEquals(expResult, result);
    }

    /**
     * Test of actsIn method, of class ExternalService.
     */
    @Test
    public void testActsIn() {
        System.out.println("actsIn");
        String pc1 = "4000-001";
        String pc2 = "4000-002";
        Double radius = 600d;
        ExternalService instance = extService;
        boolean expResult = false;
        boolean result = instance.actsIn(pc1, pc2, radius);
        assertEquals(expResult, result);
    }

    /**
     * Test of getActuation method, of class ExternalService.
     */
    @Test
    public void testGetActuation() {
        System.out.println("getActuation");
        String pc = "4000-000";
        double radius = 80000.0;
        ExternalService instance = extService;
        List<PostalCode> expResult = lstPostalCodes;
        List<PostalCode> result = instance.getActuation(pc, radius);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListPC method, of class ExternalService.
     */
    @Test
    public void testGetListPC() {
        System.out.println("getListPC");
        ExternalService instance = extService;
        List<PostalCode> expResult = lstPostalCodes;
        List<PostalCode> result = instance.getListPC();
        assertEquals(expResult, result);
    }

}
