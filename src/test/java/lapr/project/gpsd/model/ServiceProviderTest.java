/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ServiceProviderTest {

    private ServiceProvider sp;
    private Rating r;

    public ServiceProviderTest() {

    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        sp = new ServiceProvider(1, "FULLname", "nameShort", "email","123456789", "4000-018");
        r = new Rating(1, 1);
        sp.addRating(r);

    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testGets() {
        System.out.println("testGetsSets");
        ServiceProvider sp2 = new ServiceProvider(1, "FULLname", "nameShort", "email", "123456789", "4000-017");
        String name1 = sp2.getCompletName();
        String name2 = sp.getCompletName();
        sp.setLabel("Label");
        String label = sp.getLabel();

        assertEquals(name1, name2);
        assertEquals("Label",label);
    }

    @Test
    public void testAddRating() {
        System.out.println("testAddRating");

        boolean expTrue = true;
        boolean expFalse = false;

        Rating r2 = new Rating(1, 1);
        Rating r3 = null;

        boolean result = sp.addRating(r2);
        boolean result2 = sp.addRating(r3);

        assertEquals(expTrue, result);
        assertEquals(expFalse, result2);

    }

    @Test
    public void testGetRatings() {
        System.out.println("testGetRatings");

        List<Rating> exp = new ArrayList<>();
        exp.add(r);

        List<Rating> result = sp.getRatings();

        assertEquals(exp, result);
    }

//    @Test
//    public void testHasGeoArea() {
//        System.out.println("testHasGeoArea");
//
//        GeographicalArea ga = new GeographicalArea("ga", 20.2f, 10, "Postal", new ExternalService());
//        
//        
//    }
    
//    @Test
//    public void testGetMean(){
//        
//    }
    
    @Test
    public void testAddDisponibility(){
        System.out.println("addDisp");
        
        Disponibility disp = new Disponibility(LocalDateTime.now(), LocalDateTime.now());
        
        
        
        boolean expT = true;
        boolean expF = false;
        
        boolean res = this.sp.addDisponibility(disp);
        boolean res2 = this.sp.addDisponibility(disp);
        boolean res3 = this.sp.addDisponibility(null);
        
        assertEquals(expT, res);
        assertEquals(expF, res2);
        assertEquals(expF, res3);
    }
}   
