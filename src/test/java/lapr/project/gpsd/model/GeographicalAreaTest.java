/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class GeographicalAreaTest {
    
    private GeographicalArea ga;

    public GeographicalAreaTest() {

    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        ga  = new GeographicalArea("ga",20.2f,10,"Postal",new ExternalService());
        

    }

    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void testGetsSets(){
        System.out.println("testGestSets");
        
        
        GeographicalArea gaNew = new GeographicalArea("ga2",20.5, 12, "2", new ExternalService());
        gaNew.setDesignation("ga");
        gaNew.setPostalCode("Postal");
        gaNew.setTransitCost(20.2f);
        gaNew.setWorkingRadius(10);
        assertEquals(gaNew.getDesignation(), this.ga.getDesignation());
        assertEquals(gaNew.getPostalCode(), this.ga.getPostalCode());
        assertEquals(gaNew.getTransitCost(), this.ga.getTransitCost());
        assertEquals(gaNew.getWorkingRadius(), this.ga.getWorkingRadius());
    }
    
    @Test
    public void testEquals(){
        System.out.println("testEquals");
        
        boolean expFalse = false;
        boolean expTrue = true;
        
        GeographicalArea gaNew = new GeographicalArea("ga",20.2f,10,"Postal",new ExternalService());
        GeographicalArea gaDif = new GeographicalArea("ga",20.2f,10,"Postal222",new ExternalService());
        GeographicalArea gaNull = null;
        Category cat = new Category("c","c");
        
        boolean result = gaNew.equals(this.ga);
        boolean result2 = this.ga.equals(gaNull);
        boolean result3 = this.ga.equals(cat);
        boolean result4 = this.ga.equals(gaDif);
        
        assertEquals(expTrue, result);
        assertEquals(expFalse, result2);
        assertEquals(expFalse, result3);
        assertEquals(expFalse, result4);
    }
}
