/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ClientTest {

    private Client cli;

    private PostalAddress postalAddress;

    public ClientTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        cli = new Client("Joao", "123456789", "987654321", "mail@mail.com");
        postalAddress = new PostalAddress("Rua de Cima", "4444-444", "Porto");

    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of addAddress method, of class Client.
     */
    @Test
    public void testAddAddress() {
        System.out.println("addAddress");
        PostalAddress address = postalAddress;
        Client instance = cli;
        Client instance2 = new Client("Joao2", "123456789", "987654321", "mail@mail.com");
        instance2.getAddresses().add(address);
        boolean expResult = true;
        boolean expResult2 = false;
        boolean result = instance.addAddress(postalAddress);
        boolean result2 = instance2.addAddress(address);
        assertEquals(expResult, result);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of removeAddress method, of class Client.
     */
    @Test
    public void testRemoveAddress() {
        System.out.println("removeAddress");
        PostalAddress address = postalAddress;
        Client instance = cli;
        boolean expResult = false;
        boolean result = instance.removeAddress(address);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Client.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object o = cli;
        Object clientNull = null;
        Client instance = cli;
        Category cat = new Category("c","c");
        boolean expResult = true;   
        boolean expFalse = false;
        boolean result = instance.equals(o);
        boolean result2 = instance.equals(clientNull);
        boolean result3 = instance.equals(cat);
        assertEquals(expResult, result);
        assertEquals(expFalse, result2);
        assertEquals(expFalse, result3);
    }

    /**
     * Test of newPostalAddress method, of class Client.
     */
    @Test
    public void testNewPostalAddress() {
        System.out.println("newPostalAddress");
        String address = "Rua de Baixo";
        String postalCode = "5555-555";
        String place = "Porto";
        PostalAddress expResult = new PostalAddress(address, postalCode, place);
        PostalAddress result = Client.newPostalAddress(address, postalCode, place);
        assertEquals(expResult, result);
    }

    
    @Test
    public void testToString(){
        Client c = this.cli;
        String toString = "Joao - 123456789 - 987654321 - mail@mail.com";
        String result = c.toString();
        assertEquals(toString, result);
    }
}
