/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;


import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ApplicationTest {
    
    private Application app;
    private PostalAddress postalA;
    private List<PostalAddress> postalList; 
    private List<AcademicQualification> academicList; 
    
    public ApplicationTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        
        postalA = new PostalAddress("Rua qqcoisa", "4000-000", "qqlado");
        Category cat = new Category("1", "Cleaning");
        app = new Application("ZP", "123456789", "911111111", "a@aaa.com", postalA);
        app.addAcademicQualification("Master qq coisa", "Master degree", "Excelente");
        app.addProfessionalQualification("NoQualification");
        app.addSuportDocument("doc");
        app.addCategory(cat);
        postalList = app.getPostalAddressList();
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getSPName method, of class Application.
     */
    @Test
    public void testGetSPName() {
        System.out.println("getSPName");
        String expResult = "ZP";
        String result = app.getSPName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSPVATNumber method, of class Application.
     */
    @Test
    public void testGetSPVATNumber() {
        System.out.println("getSPVATNumber");
        String expResult = "123456789";
        String result = app.getSPVATNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSPPhoneNumber method, of class Application.
     */
    @Test
    public void testGetSPPhoneNumber() {
        System.out.println("getSPPhoneNumber");
        String expResult = "911111111";
        String result = app.getSPPhoneNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSPEmail method, of class Application.
     */
    @Test
    public void testGetSPEmail() {
        System.out.println("getSPEmail");
        String expResult = "a@aaa.com";
        String result = app.getSPEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPostalAddressList method, of class Application.
     */
    @Test
    public void testGetPostalAddressList() {
        System.out.println("getPostalAddressList");
        List<PostalAddress> expResult = new ArrayList<PostalAddress>();
        expResult.add(postalA);
        List<PostalAddress> result = app.getPostalAddressList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAcademicQualificationList method, of class Application.
     */
    @Test
    public void testGetAcademicQualificationList() {
        System.out.println("getAcademicQualificationList");
        List<AcademicQualification> expResult = new ArrayList<AcademicQualification>();
        expResult.add(new AcademicQualification("Master qq coisa", "Master degree", "Excelente"));
        List<AcademicQualification> result = app.getAcademicQualificationList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getProfessionalQualificationList method, of class Application.
     */
    @Test
    public void testGetProfessionalQualificationList() {
        System.out.println("getProfessionalQualificationList");
        List<ProfessionalQualification> expResult = new ArrayList<ProfessionalQualification>();
        expResult.add(new ProfessionalQualification("NoQualification"));
        List<ProfessionalQualification> result = app.getProfessionalQualificationList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDocumentList method, of class Application.
     */
    @Test
    public void testGetDocumentList() {
        System.out.println("getDocumentList");
        List<Document> expResult = new ArrayList<Document>();
        expResult.add(new Document("doc"));
        List<Document> result = app.getDocumentList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCategoryList method, of class Application.
     */
    @Test
    public void testGetCategoryList() {
        System.out.println("getCategoryList");
        List<Category> expResult = new ArrayList<Category>();
        expResult.add(new Category("1", "Cleaning"));
        List<Category> result = app.getCategoryList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSPName method, of class Application.
     */
    @Test
    public void testSetSPName() {
        System.out.println("setSPName");
        String SPName = "otherName";
        app.setSPName(SPName);
        assertEquals(SPName, app.getSPName());
    }

    /**
     * Test of setSPVATNumber method, of class Application.
     */
    @Test
    public void testSetSPVATNumber() {
        System.out.println("setSPVATNumber");
        String SPVATNumber = "987654321";
        app.setSPVATNumber(SPVATNumber);
        assertEquals(SPVATNumber, app.getSPVATNumber());
    }

    /**
     * Test of setSPPhoneNumber method, of class Application.
     */
    @Test
    public void testSetSPPhoneNumber() {
        System.out.println("setSPPhoneNumber");
        String SPPhoneNumber = "966666666";
        app.setSPPhoneNumber(SPPhoneNumber);
        assertEquals(SPPhoneNumber, app.getSPPhoneNumber());
    }

    /**
     * Test of setSPEmail method, of class Application.
     */
    @Test
    public void testSetSPEmail() {
        System.out.println("setSPEmail");
        String SPEmail = "x@xxx.xxx";
        app.setSPEmail(SPEmail);
        assertEquals(SPEmail, app.getSPEmail());
    }

    /**
     * Test of setPostalAddressList method, of class Application.
     */
    @Test
    public void testSetPostalAddressList() {
        System.out.println("setPostalAddressList");
        List<PostalAddress> newpostalAddressList = new ArrayList<PostalAddress>();
        newpostalAddressList.add(new PostalAddress("Largo", "1000-000", "seiLa"));
        app.setPostalAddressList(newpostalAddressList);
        assertEquals(newpostalAddressList, app.getPostalAddressList());
    }

    /**
     * Test of setAcademicQualificationList method, of class Application.
     */
    @Test
    public void testSetAcademicQualificationList() {
        System.out.println("setAcademicQualificationList");
        List<AcademicQualification> newacademicQualificationList = new ArrayList<AcademicQualification>();
        newacademicQualificationList.add(new AcademicQualification("MEI", "Master Degree", "Excelent"));
        app.setAcademicQualificationList(newacademicQualificationList);
        assertEquals(newacademicQualificationList, app.getAcademicQualificationList());
    }

    /**
     * Test of setProfessionalQualificationList method, of class Application.
     */
    @Test
    public void testSetProfessionalQualificationList() {
        System.out.println("setProfessionalQualificationList");
        List<ProfessionalQualification> newprofessionalQualificationList = new ArrayList<ProfessionalQualification>();
        newprofessionalQualificationList.add(new ProfessionalQualification("ProfessionalQualification"));
        app.setProfessionalQualificationList(newprofessionalQualificationList);
        assertEquals(newprofessionalQualificationList, app.getProfessionalQualificationList());
    }

    /**
     * Test of setDocumentList method, of class Application.
     */
    @Test
    public void testSetDocumentList() {
        System.out.println("setDocumentList");
        List<Document> newdocumentList = new ArrayList<Document>();
        newdocumentList.add(new Document("otherDoc"));
        app.setDocumentList(newdocumentList);
        assertEquals(newdocumentList, app.getDocumentList());
    }

    /**
     * Test of setCategoryList method, of class Application.
     */
    @Test
    public void testSetCategoryList() {
        System.out.println("setCategoryList");
        List<Category> newcategoryList = new ArrayList<Category>();
        newcategoryList.add(new Category("2", "UBER"));
        app.setCategoryList(newcategoryList);
        assertEquals(newcategoryList, app.getCategoryList());
    }

    /**
     * Test of hashCode method, of class Application.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = 1083510389;
        int result = app.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Application.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        boolean expResult = false;
        boolean result = app.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Application.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Application: Name: ZP\n" +
        "VAT Number: 123456789\n" +
        "Phone Number: 911111111\n" +
        "E-mail: a@aaa.com\n" +
        "Postal address: [Rua qqcoisa - 4000-000 - qqlado]\n" +
        "Academic qualification: [AcademicQualification: Designation: Master qq coisa | Degree: Master degree | Classification: Excelente]\n" +
        "Professional qualification: [Professional Qualification: description: NoQualification]\n" +
        "Categories: [1 - Cleaning ]";
        String result = app.toString();
        assertEquals(expResult, result);
    }

//    /**
//     * Test of newAdress method, of class Application.
//     */
//    @Test
//    public void testNewAdress() {
//        System.out.println("newAdress");
//        String strLocal = "Home";
//        String strCodPostal = "7000-000";
//        String strLocalidade = "PorAi";
//        PostalAddress expResult = new PostalAddress(strLocal, strCodPostal, strLocalidade);
//        PostalAddress result = Application.newAdress(strLocal, strCodPostal, strLocalidade);
//        assertEquals(expResult.hashCode(), result.hashCode());
//    }

    /**
     * Test of addPostalAddress method, of class Application.
     */
    @Test
    public void testAddPostalAddress() {
        System.out.println("addPostalAddress");
        PostalAddress postalB = new PostalAddress("Home", "7000-000", "PorAli");
        app.addPostalAddress(postalB);
        int expResult = 2;
        int result = app.getPostalAddressList().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of addAcademicQualification method, of class Application.
     */
    @Test
    public void testAddAcademicQualification() {
        System.out.println("addAcademicQualification");
        String designation = "LEI";
        String degree = "Lic";
        String classification = "18";
        app.addAcademicQualification(designation, degree, classification);
        int expResult = 2;
        int result = app.getAcademicQualificationList().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of addProfessionalQualification method, of class Application.
     */
    @Test
    public void testAddProfessionalQualification() {
        System.out.println("addProfessionalQualification");
        String designation = "outra";
        app.addProfessionalQualification(designation);
        int expResult = 2;
        int result = app.getProfessionalQualificationList().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of addSuportDocument method, of class Application.
     */
    @Test
    public void testAddSuportDocument() {
        System.out.println("addSuportDocument");
        String document = "newDoc";
        app.addSuportDocument(document);
        int expResult = 2;
        int result = app.getDocumentList().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of addCategory method, of class Application.
     */
    @Test
    public void testAddCategory() {
        System.out.println("addCategory");
        Category cat2 = new Category("3", "UberEats");
        app.addCategory(cat2);
        int expResult = 2;
        int result = app.getCategoryList().size();
        assertEquals(expResult, result);
    }
    
}
