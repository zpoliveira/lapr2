/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ExpansibleServiceTest {

    private ExpansibleService es;
    private Category c;

    public ExpansibleServiceTest() {

    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        c = new Category("c", "c");
        es = new ExpansibleService("id", "lilDesc", "largDesc", 20, c);
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testGetsSets() {
        System.out.println("testGetsSets");

        boolean expT = true;
        boolean expF = false;

        ExpansibleService es1 = new ExpansibleService("id", "lilDesc", "largDesc", 20, c);
        assertEquals(es.getId(), es1.getId());
        assertEquals(es.getCategory(), es1.getCategory());
        assertEquals(es.getCostHour(), es1.getCostHour());
        assertEquals(es.getLittleDescription(), es.getLittleDescription());
        assertEquals(es.getCost4Duration(20), es1.getCost4Duration(20));

        boolean resultHasIdT = es.hasId("id");
        boolean resultHasIdF = es.hasId("iiiid");

        assertEquals(expT, resultHasIdT);
        assertEquals(expF, resultHasIdF);
    }
    
    @Test
    public void testEquals(){
        System.out.println("testEquals");
        
        boolean expF = false;
        boolean expT = true;
        
        ExpansibleService es1 = new ExpansibleService("id", "lilDesc", "largDesc", 20, c);
        ExpansibleService es3 = new ExpansibleService("id2", "lilDesc", "largDesc", 20, c);
        ExpansibleService es2 = null;
        
        boolean res = es1.equals(es);
        boolean res2 = es.equals(es2);
        boolean res3 = es.equals(c);
        boolean res4 = es.equals(es3);
        
       assertEquals(expT, res);
       assertEquals(expF,res2);
       assertEquals(expF, res3);
       assertEquals(expF,res4);
        
    }
    
    @Test
    public void testToString(){
        String other = String.format("%.2f", 20.00);
        String exp = "id - lilDesc - largDesc - "+ other +" - Expansible Service: c - c ";
        
        assertEquals(exp, es.toString());
    }

}
