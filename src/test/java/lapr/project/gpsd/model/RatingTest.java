/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Pedro
 */
public class RatingTest {
    Rating rating;
    
    public RatingTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        this.rating = new Rating(3, 3);
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getRate method, of class Rating.
     */
    @Test
    public void testGetRate() {
        System.out.println("getRate");
        Rating instance = this.rating;
        int expResult = 3;
        int result = instance.getRate();
        assertEquals(expResult, result);

    }
    
}
