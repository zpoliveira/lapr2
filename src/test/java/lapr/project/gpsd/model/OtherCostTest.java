/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class OtherCostTest {

    private OtherCost oc;

    public OtherCostTest() {

    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
       oc = new OtherCost("cost",10);

    }

    @AfterEach
    public void tearDown() {
    }
    
    /**
     * Test of getDesignation method, of class OtherCost.
     */
    @org.junit.Test
    public void testGetDesignation() {
        System.out.println("getDesignation");
        OtherCost instance = this.oc;
        String expResult = "cost";
        String result = instance.getDesignation();
        assertEquals(expResult, result);
    }

    /**
     * Test of getValue method, of class OtherCost.
     */
    @org.junit.Test
    public void testGetValue() {
        System.out.println("getValue");
        OtherCost instance = this.oc;
        double expResult = 10;
        double result = instance.getValue();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDesignation method, of class OtherCost.
     */
    @org.junit.Test
    public void testSetDesignation() {
        System.out.println("setDesignation");
        String designation = "newcost";
        OtherCost instance = this.oc;
        instance.setDesignation(designation);
        assertEquals(designation, this.oc.getDesignation());
    }

    /**
     * Test of setValue method, of class OtherCost.
     */
    @org.junit.Test
    public void testSetValue() {
        System.out.println("setValue");
        int value = 20;
        OtherCost instance = this.oc;
        instance.setValue(value);
        assertEquals(value, this.oc.getValue());
    }
}
