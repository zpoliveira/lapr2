/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Pedro
 */
public class ExecutionOrderTest {
    ServiceProvider serviceProvider;
    Category cat; 
    LimitatedService service; 
    ExecutionOrder exeOrd; 
    
    public ExecutionOrderTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        this.serviceProvider = new ServiceProvider(3, "name", "name", "mails", "123456789", "4000-017");
        this.cat = new Category("4", "des"); 
        this.service = new LimitatedService("2", "des", "des", 30, this.cat);
        this.exeOrd = new ExecutionOrder(2, 2, this.serviceProvider, this.service, 30, "mail");
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getId method, of class ExecutionOrder.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        ExecutionOrder instance = this.exeOrd;
        int expResult = 2;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class ExecutionOrder.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        int id = 1;
        ExecutionOrder instance = this.exeOrd;
        instance.setId(id);
        assertEquals(id, this.exeOrd.getId());
    }

    /**
     * Test of getService method, of class ExecutionOrder.
     */
    @Test
    public void testGetService() {
        System.out.println("getService");
        ExecutionOrder instance = this.exeOrd;
        Service expResult = this.service;
        Service result = instance.getService();
        assertEquals(expResult, result);
    }

    /**
     * Test of setService method, of class ExecutionOrder.
     */
    @Test
    public void testSetService() {
        System.out.println("setService");
        Service service = new LimitatedService("1", "des", "des", 30, this.cat);;
        ExecutionOrder instance = this.exeOrd;
        instance.setService(service);
        assertEquals(service, this.exeOrd.getService());
    }

    /**
     * Test of getServicerequestID method, of class ExecutionOrder.
     */
    @Test
    public void testGetServicerequestID() {
        System.out.println("getServicerequestID");
        ExecutionOrder instance = this.exeOrd;
        int expResult = 2;
        int result = instance.getServicerequestID();
        assertEquals(expResult, result);

    }

    /**
     * Test of setServicerequestID method, of class ExecutionOrder.
     */
    @Test
    public void testSetServicerequestID() {
        System.out.println("setServicerequestID");
        int servicerequestID = 1;
        ExecutionOrder instance = this.exeOrd;
        instance.setServicerequestID(servicerequestID);
        assertEquals(servicerequestID, this.exeOrd.getServicerequestID());
    }

    /**
     * Test of getServiceProvider method, of class ExecutionOrder.
     */
    @Test
    public void testGetServiceProvider() {
        System.out.println("getServiceProvider");
        ExecutionOrder instance = this.exeOrd;
        ServiceProvider expResult = this.serviceProvider;
        ServiceProvider result = instance.getServiceProvider();
        assertEquals(expResult, result);
    }

    /**
     * Test of setServiceProvider method, of class ExecutionOrder.
     */
    @Test
    public void testSetServiceProvider() {
        System.out.println("setServiceProvider");
        ServiceProvider serviceProvider = new ServiceProvider(1, "name", "name2", "mails2", "123456788", "4000-017");;
        ExecutionOrder instance = this.exeOrd;
        instance.setServiceProvider(serviceProvider);
        assertEquals(serviceProvider, this.exeOrd.getServiceProvider());
    }

    /**
     * Test of getDuration method, of class ExecutionOrder.
     */
    @Test
    public void testGetDuration() {
        System.out.println("getDuration");
        ExecutionOrder instance = this.exeOrd;
        int expResult = 30;
        int result = instance.getDuration();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDuration method, of class ExecutionOrder.
     */
    @Test
    public void testSetDuration() {
        System.out.println("setDuration");
        int duration = 60;
        ExecutionOrder instance = this.exeOrd;
        instance.setDuration(duration);
        assertEquals(duration, this.exeOrd.getDuration());
    }

    /**
     * Test of getClientEmail method, of class ExecutionOrder.
     */
    @Test
    public void testGetClientEmail() {
        System.out.println("getClientEmail");
        ExecutionOrder instance = this.exeOrd;
        String expResult = "mail";
        String result = instance.getClientEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setClientEmail method, of class ExecutionOrder.
     */
    @Test
    public void testSetClientEmail() {
        System.out.println("setClientEmail");
        String clientEmail = "mail2";
        ExecutionOrder instance = this.exeOrd;
        instance.setClientEmail(clientEmail);
        assertEquals(clientEmail, this.exeOrd.getClientEmail());
    }

    /**
     * Test of isRated method, of class ExecutionOrder.
     */
    @Test
    public void testIsRated() {
        System.out.println("isRated");
        ExecutionOrder instance = this.exeOrd;
        boolean expResult = false;
        boolean result = instance.isRated();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRated method, of class ExecutionOrder.
     */
    @Test
    public void testSetRated() {
        System.out.println("setRated");
        boolean rated = true;
        ExecutionOrder instance = this.exeOrd;
        instance.setRated(rated);
        assertEquals(rated, this.exeOrd.isRated());
    }

    /**
     * Test of isIsEnded method, of class ExecutionOrder.
     */
    @Test
    public void testIsIsEnded() {
        System.out.println("isIsEnded");
        ExecutionOrder instance = this.exeOrd;
        boolean expResult = false;
        boolean result = instance.isIsEnded();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIsEnded method, of class ExecutionOrder.
     */
    @Test
    public void testSetIsEnded() {
        System.out.println("setIsEnded");
        boolean isEnded = true;
        ExecutionOrder instance = this.exeOrd;
        instance.setIsEnded(isEnded);
        assertEquals(isEnded, this.exeOrd.isIsEnded());
    }

    /**
     * Test of getHasIssue method, of class ExecutionOrder.
     */
    @Test
    public void testGetHasIssue() {
        System.out.println("getHasIssue");
        ExecutionOrder instance = this.exeOrd;
        String expResult = null;
        String result = instance.getHasIssue();
        assertEquals(expResult, result);
    }

    /**
     * Test of setHasIssue method, of class ExecutionOrder.
     */
    @Test
    public void testSetHasIssue() {
        System.out.println("setHasIssue");
        String hasIssue = "issue";
        ExecutionOrder instance = this.exeOrd;
        instance.setHasIssue(hasIssue);
        assertEquals(hasIssue, this.exeOrd.getHasIssue());
    }


    /**
     * Test of equals method, of class ExecutionOrder.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        ExecutionOrder instance = this.exeOrd;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
