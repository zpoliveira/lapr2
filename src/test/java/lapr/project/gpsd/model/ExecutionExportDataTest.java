/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.time.LocalDateTime;
import java.time.Month;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Pedro
 */
public class ExecutionExportDataTest {
    ExecutionExportData exExpData;
    
    public ExecutionExportDataTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        PostalAddress pos = new PostalAddress("rua", "4000-017", "porto");
        Category cat = new Category("21", "desc");
        this.exExpData = new ExecutionExportData(
                1, 
                "ola", 
                pos, 
                20, 
                cat, 
                "Limitated Service", 
                LocalDateTime.of(2019, Month.MARCH, 3, 14, 20), 
                true, 
                "issue");
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getId method, of class ExecutionExportData.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        ExecutionExportData instance = this.exExpData;
        int expResult = 1;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class ExecutionExportData.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        int id = 3;
        ExecutionExportData instance = this.exExpData;
        instance.setId(id);
        assertEquals(id, this.exExpData.getId());
    }

    /**
     * Test of getClientName method, of class ExecutionExportData.
     */
    @Test
    public void testGetClientName() {
        System.out.println("getClientName");
        ExecutionExportData instance = this.exExpData;
        String expResult = "ola";
        String result = instance.getClientName();
        assertEquals(expResult, result);

    }

    /**
     * Test of setClientName method, of class ExecutionExportData.
     */
    @Test
    public void testSetClientName() {
        System.out.println("setClientName");
        String clientName = "adeus";
        ExecutionExportData instance = this.exExpData;
        instance.setClientName(clientName);
        assertEquals(clientName, this.exExpData.getClientName());
    }

    /**
     * Test of getPostalAddr method, of class ExecutionExportData.
     */
    @Test
    public void testGetPostalAddr() {
        System.out.println("getPostalAddr");
        ExecutionExportData instance = this.exExpData;
        PostalAddress expResult = new PostalAddress("rua", "4000-017", "porto");;
        PostalAddress result = instance.getPostalAddr();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPostalAddr method, of class ExecutionExportData.
     */
    @Test
    public void testSetPostalAddr() {
        System.out.println("setPostalAddr");
        PostalAddress postalAddr = new PostalAddress("outrarua", "4000-018", "porto");;
        ExecutionExportData instance = this.exExpData;
        instance.setPostalAddr(postalAddr);
        assertEquals(postalAddr, this.exExpData.getPostalAddr());
    }

    /**
     * Test of getDistance method, of class ExecutionExportData.
     */
    @Test
    public void testGetDistance() {
        System.out.println("getDistance");
        ExecutionExportData instance = this.exExpData;
        double expResult = 20;
        double result = instance.getDistance();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDistance method, of class ExecutionExportData.
     */
    @Test
    public void testSetDistance() {
        System.out.println("setDistance");
        double distance = 30;
        ExecutionExportData instance = this.exExpData;
        instance.setDistance(distance);
        assertEquals(distance, this.exExpData.getDistance());

    }

    /**
     * Test of getCat method, of class ExecutionExportData.
     */
    @Test
    public void testGetCat() {
        System.out.println("getCat");
        ExecutionExportData instance = this.exExpData;
        Category expResult = new Category("21", "desc");
        Category result = instance.getCat();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCat method, of class ExecutionExportData.
     */
    @Test
    public void testSetCat() {
        System.out.println("setCat");
        Category cat = new Category("26", "desc");
        ExecutionExportData instance = this.exExpData;
        instance.setCat(cat);
        assertEquals(cat, this.exExpData.getCat());
    }

    /**
     * Test of getServiceType method, of class ExecutionExportData.
     */
    @Test
    public void testGetServiceType() {
        System.out.println("getServiceType");
        ExecutionExportData instance = this.exExpData;
        String expResult = "Limitated Service";
        String result = instance.getServiceType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setServiceType method, of class ExecutionExportData.
     */
    @Test
    public void testSetServiceType() {
        System.out.println("setServiceType");
        String serviceType = "Expansible Service";
        ExecutionExportData instance = this.exExpData;
        instance.setServiceType(serviceType);
        assertEquals(serviceType, this.exExpData.getServiceType());
    }

    /**
     * Test of getDate method, of class ExecutionExportData.
     */
    @Test
    public void testGetDate() {
        System.out.println("getDate");
        ExecutionExportData instance = this.exExpData;
        LocalDateTime expResult = LocalDateTime.of(2019, Month.MARCH, 3, 14, 20);
        LocalDateTime result = instance.getDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDate method, of class ExecutionExportData.
     */
    @Test
    public void testSetDate() {
        System.out.println("setDate");
        LocalDateTime date = LocalDateTime.of(2019, Month.MARCH, 4, 14, 20);
        ExecutionExportData instance = this.exExpData;
        instance.setDate(date);
        assertEquals(date, this.exExpData.getDate());
    }

    /**
     * Test of isIsEnded method, of class ExecutionExportData.
     */
    @Test
    public void testIsIsEnded() {
        System.out.println("isIsEnded");
        ExecutionExportData instance = this.exExpData;
        boolean expResult = true;
        boolean result = instance.isIsEnded();
        assertEquals(expResult, result);

    }

    /**
     * Test of setIsEnded method, of class ExecutionExportData.
     */
    @Test
    public void testSetIsEnded() {
        System.out.println("setIsEnded");
        boolean isEnded = false;
        ExecutionExportData instance = this.exExpData;
        instance.setIsEnded(isEnded);
        assertEquals(isEnded, this.exExpData.isIsEnded());
    }

    /**
     * Test of isEndedProperty method, of class ExecutionExportData.
     */
    @Test
    public void testIsEndedProperty() {
        System.out.println("isEndedProperty");
        ExecutionExportData instance = this.exExpData;
        BooleanProperty expResult = new SimpleBooleanProperty(true);
        BooleanProperty result = instance.isEndedProperty();
        assertEquals(expResult.get(), result.get());
    }

    /**
     * Test of getHasIssue method, of class ExecutionExportData.
     */
    @Test
    public void testGetHasIssue() {
        System.out.println("getHasIssue");
        ExecutionExportData instance = this.exExpData;
        String expResult = "issue";
        String result = instance.getHasIssue();
        assertEquals(expResult, result);
    }

    /**
     * Test of setHasIssue method, of class ExecutionExportData.
     */
    @Test
    public void testSetHasIssue() {
        System.out.println("setHasIssue");
        String hasIssue = "no issue";
        ExecutionExportData instance = this.exExpData;
        instance.setHasIssue(hasIssue);
        assertEquals(hasIssue, this.exExpData.getHasIssue());
    }

    /**
     * Test of hasIssue method, of class ExecutionExportData.
     */
    @Test
    public void testHasIssue() {
        System.out.println("hasIssue");
        ExecutionExportData instance = this.exExpData;
        StringProperty expResult = new SimpleStringProperty("issue");
        StringProperty result = instance.hasIssue();
        assertEquals(expResult.get(), result.get());
    }
    
}
