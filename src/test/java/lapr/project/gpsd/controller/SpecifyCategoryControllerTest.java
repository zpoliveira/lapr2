/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SpecifyCategoryControllerTest {
    
    private SpecifyCategoryController specifyCategoryController;
    private Company company;

    public SpecifyCategoryControllerTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {

        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");

        this.specifyCategoryController = new SpecifyCategoryController();

    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testRegisterCategory() {
        boolean exptFalse = false;
        boolean exptTrue = true;
        boolean result = this.specifyCategoryController.registerCategory(new Category("code","desc"));
        assertEquals(exptTrue, result);
    }
    
    @Test
    public void testCreateCategory(){
        Category expNull = null;
        Category exp = new Category("c","c");
        Category nullCat = this.specifyCategoryController.newCategory(null, null);
        Category result = this.specifyCategoryController.newCategory("c", "c");
        
        assertEquals(exp,result);
        assertEquals(expNull,nullCat);
        
    }
}
