/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.FixedService;
import lapr.project.gpsd.model.OtherCost;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.SchedulePreference;
import lapr.project.gpsd.model.ServiceRequest;
import lapr.project.gpsd.model.ServiceRequestDescription;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ServiceRequestControllerTest {

    private final Company company;
    private final ServiceRequestController servReqControl;
    private ServiceRequest servReq;
    private PostalAddress postalAdd;
    private SchedulePreference sched;
    private String cEmail;
    private FixedService serv2;
    private LocalDateTime cal;
    private ServiceRequestDescription desc;
    private OtherCost otherCs;
    private List<ServiceRequestDescription> lstDesc;
    private List<SchedulePreference> lstSched;
    private List<OtherCost> lstOther;

    public ServiceRequestControllerTest() {
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");
        this.servReqControl = new ServiceRequestController();
    }

    @BeforeAll
    public static void setUpClass() {

    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {

        this.cEmail = "email@server.pt";
        String description2 = "testestessetsett";
        int duration2 = 30;
        Category cat = new Category("CAT222", description2);
        this.serv2 = new FixedService(this.cEmail, "teste", description2, 10, cat);
        this.lstDesc = new ArrayList<>();
        this.lstSched = new ArrayList<>();
        this.lstOther = new ArrayList<>();

        this.postalAdd = new PostalAddress("Street test", "4460-204", "PORTO");
        this.servReq = new ServiceRequest(this.cEmail, this.postalAdd);
        this.cal = LocalDateTime.now();
        this.sched = new SchedulePreference(1, this.cal);
        this.desc = new ServiceRequestDescription(this.serv2, this.cEmail, 1);
        this.otherCs = new OtherCost("Deslocation", 10);

        this.servReqControl.newServiceRequest("lol@lol.lol", this.postalAdd);
        this.servReqControl.registerServiceRequest();

        this.lstDesc.add(this.desc);
        this.lstSched.add(this.sched);
        this.lstOther.add(this.otherCs);

    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of newServiceRequest method, of class ServiceRequestController.
     */
    @Test
    public void testNewServiceRequest() {
        System.out.println("newServiceRequest");
        boolean expResult = true;
        boolean result
                = this.servReqControl.newServiceRequest("oudnju@kdk.com", this.postalAdd);
        assertEquals(expResult, result);
    }

    /**
     * Test of registerServiceRequest method, of class ServiceRequestController.
     */
    @Test
    public void testRegisterServiceRequest() {
        System.out.println("registerServiceRequest");
        this.servReqControl.newServiceRequest(this.cEmail, this.postalAdd);
        boolean expResult = true;
        boolean result = this.servReqControl.registerServiceRequest();
        assertEquals(expResult, result);
    }

    /**
     * Test of addSchedulePreference method, of class ServiceRequestController.
     */
    @Test
    public void testAddSchedulePreference() {
        System.out.println("addSchedulePreference");
        boolean expResult = true;
        boolean result = this.servReqControl.addSchedulePreference(LocalDateTime.now());
        assertEquals(expResult, result);
    }

    /**
     * Test of addDescription method, of class ServiceRequestController.
     */
    @Test
    public void testAddDescription() {
        System.out.println("addDescription");

        String description2 = "lolololol";
        int duration2 = 52;
        boolean expResult2 = true;
        boolean result2 = this.servReqControl.addDescription(serv2, description2, duration2);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of getListSchedules method, of class ServiceRequestController.
     */
    @Test
    public void testGetListSchedules() {
        System.out.println("getListSchedules");
        List<SchedulePreference> expResult = lstSched;
        this.servReqControl.addSchedulePreference(cal);
        List<SchedulePreference> result = this.servReqControl.getListSchedules();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDescriptionList method, of class ServiceRequestController.
     */
    @Test
    public void testGetDescriptionList() {
        System.out.println("getDescriptionList");
        List<ServiceRequestDescription> expResult = lstDesc;
        this.servReqControl.addDescription(serv2, cEmail, 0);
        List<ServiceRequestDescription> result = this.servReqControl.getDescriptionList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getOtherCostList method, of class ServiceRequestController.
     */
    @Test
    public void testGetOtherCostList() {
        System.out.println("getOtherCostList");
        
        List<OtherCost> expResult2 = new ArrayList<>();
        List<OtherCost> result2 = this.servReqControl.getOtherCostList();
        assertEquals(expResult2, result2);
    }

    /**
     * Test of calculateCost method, of class ServiceRequestController.
     */
    @Test
    public void testCalculateCost() {
        System.out.println("calculateCost");
        boolean result = this.servReqControl.calculateCost();
        boolean expResult = true;
        assertEquals(expResult, result);
    }

}
