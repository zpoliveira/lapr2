/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.RequestStatus;
import lapr.project.gpsd.model.ServiceRequest;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class DecideServiceRequestsControllerTest {

    private DecideServiceRequestsController decideServiceRequestsController;
    private Company company;
    private List<ServiceRequest> exp;
    private ServiceRequest serviceRequest1;
    private ServiceRequest acceptedReq;
    private ServiceRequest rejectedReq;

    public DecideServiceRequestsControllerTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {

        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");
        exp = this.company.getServiceRequestRegister().getClientAssignedServiceRequests(ApplicationGPSD.getInstance().getActualSession().getUserEmail());
        
        serviceRequest1 = new ServiceRequest("test",new PostalAddress("Rua wdwdw", "4000-000", "Porto"));
        serviceRequest1.setStatus(RequestStatus.ASSIGNED);
        
        acceptedReq = new ServiceRequest("test",new PostalAddress("Rua wdwddewdeww", "4000-000", "Porto"));
        acceptedReq.setStatus(RequestStatus.APPROVED);
        
        rejectedReq = new ServiceRequest("test",new PostalAddress("Rua dewdwqq", "4000-000", "Porto"));
        rejectedReq.setStatus(RequestStatus.NEW);
        
        this.decideServiceRequestsController = new DecideServiceRequestsController();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testGetUserServiceRequests() {
        System.out.println("getUserServiceReqs");
        String email = ApplicationGPSD.getInstance().getActualSession().getUserEmail();
        List<ServiceRequest> serviceRequests = this.decideServiceRequestsController.getUserServiceRequests();
        assertEquals(exp, serviceRequests);
    }
}
