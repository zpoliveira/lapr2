/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.PostalAddress;

import static org.junit.Assert.*;
import org.junit.jupiter.api.*;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ClientRegisterControllerTest {
    private ClientRegisterController controller;
    private Client cli;
    private PostalAddress postalAddress;
    
    public ClientRegisterControllerTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
        
        
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        this.controller = new ClientRegisterController();
        this.postalAddress = new PostalAddress("Rua de cima","4000-117","MCN");
        this.cli = new Client("Matia", "987654321", "987654321", "mail@mail.com");
        this.cli.addAddress(postalAddress);
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of newClient method, of class ClientRegisterController.
     */
    @Test
    public void testNewClient() {
        try {
            System.out.println("newClient");
            String name = "maria";
            String vatNumber = "987654123";
            String phone = "987654321";
            String email = "mail3@mail.com";
            String pwd = "pwd";
            List<PostalAddress> lstAddresses = new ArrayList<>();
            lstAddresses.add(this.postalAddress);
            boolean expResult = true;
            boolean result = controller.newClient(name, vatNumber, phone, email, pwd, lstAddresses);
            assertEquals(expResult, result);
            Assertions.assertDoesNotThrow(() -> {
                controller.newClient(name, vatNumber, phone, email, pwd, lstAddresses);
            });
        } catch (Exception ex) {
            Logger.getLogger(ClientRegisterControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     /**
     * Test of addPostalAddress method, of class ClientRegisterController.
     */
    @Test
    public void testAddPostalAddress() throws Exception{
        System.out.println("addPostalAddress");
         System.out.println("newClient");
        String name = "maria";
        String vatNumber = "987654123";
        String phone = "987654321";
        String email = "mail3@mail.com";
        String pwd = "pwd";
        List<PostalAddress> lstAddresses = new ArrayList<>();
        lstAddresses.add(this.postalAddress);
        controller.newClient(name, vatNumber, phone, email, pwd, lstAddresses);
        String address = "rua de baixo";
        String postalCode = "4000-118";
        String place = "MCN";
        
        boolean expResult = true;
        boolean result = controller.addPostalAddress(address, postalCode, place);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of addPostalAddress method, of class ClientRegisterController.
     */
    @Test
    public void testAddPostalAddressNOTOK() {
        System.out.println("addPostalAddress");
        String address = "";
        String postalCode = "4000-117";
        String place = "MCN";
        
        boolean expResult = false;
        boolean result = controller.addPostalAddress(address, postalCode, place);
        assertEquals(expResult, result);
    }
    
    
    /**
     * Test of createClient method, of class ClientRegisterController.
     */
    @Test
    public void testCreateClientOK() throws Exception {
        System.out.println("createClient");
        boolean expResult = true;
        boolean result = controller.createClient(cli, "123");
        assertEquals(expResult, result);
        Assertions.assertThrows(Exception.class, () -> {
             controller.createClient(cli, "");
         });
    }

     /**
     * Test of createClient method, of class ClientRegisterController.
     */
    @Test
    public void testCreateClientNOTOK() {
        System.out.println("createClient");
        boolean expResult = false;
         Assertions.assertThrows(Exception.class, () -> {
             controller.createClient(cli, "");
         });
    }

    /**
     * Test of createCliente method, of class ClientRegisterController.
     */
    @Test
    public void testCreateCliente() throws Exception {
        System.out.println("createCliente");
       System.out.println("newClient");
        String name = "maria";
        String vatNumber = "987654123";
        String phone = "987654321";
        String email = "mail3@mail.com";
        String pwd = "pwd";
        List<PostalAddress> lstAddresses = new ArrayList<>();
        lstAddresses.add(this.postalAddress);
        boolean expResult = true;
        controller.newClient(name, vatNumber, phone, email, pwd, lstAddresses);
        boolean result = controller.createCliente();
        assertEquals(expResult, result);
        Assertions.assertThrows(Exception.class, () -> {
             controller.createClient(cli, "");
         });
    }

    /**
     * Test of getClientString method, of class ClientRegisterController.
     */
    @Test
    public void testGetClientString() {
        try {
            System.out.println("getClientString");
            String name = "maria";
            String vatNumber = "987654123";
            String phone = "987654321";
            String email = "mail3@mail.com";
            String pwd = "pwd";
            List<PostalAddress> lstAddresses = new ArrayList<>();
            lstAddresses.add(this.postalAddress);
            String expResult = "maria - 987654123 - 987654321 - mail3@mail.com\nAddress:\nRua de cima - 4000-117 - MCN";
            controller.newClient(name, vatNumber, phone, email, pwd, lstAddresses);
            String result = controller.getClientString();
            assertEquals(expResult, result);
        } catch (Exception ex) {
            Logger.getLogger(ClientRegisterControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
