/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.FixedService;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SpecifyServiceControllerTest {
    private Company company;
    private SpecifyServiceController speServController;
    private final String serviceId = "1";
    private final String littleDescription = "litte desc";
    private final String largeDescription = "large desc";
    private final double costHour = 10.0;
    private final String catId = "2";
    private final String type = "Fixed Service";
    private final Category cat = new Category("2", "test");
    private final FixedService fixedService = new FixedService(this.serviceId, littleDescription, largeDescription, costHour, cat);
    private final FixedService fixedService2 = new FixedService("2", littleDescription, largeDescription, costHour, cat);
    public SpecifyServiceControllerTest() {
        
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");
        this.company.getCategoryRegister().registerCategory(new Category("1", "cat1"));
        this.company.getCategoryRegister().registerCategory(new Category("2", "cat2"));
        this.company.getServiceRegister().registService(fixedService);
        this.speServController = new SpecifyServiceController();
        
        
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getCategories method, of class SpecifyServiceController.
     */
    @Test
    public void testGetCategories() {
        System.out.println("getCategories");
        List<Category> expResult = company.getCategoryRegister().getCategories();
        List<Category> result = this.speServController.getCategories();
        assertEquals(expResult, result);
    }

    /**
     * Test of getServiceTypesNames method, of class SpecifyServiceController.
     */
    @Test
    public void testGetServiceTypesNames() {
        System.out.println("getServiceTypesNames");
        List<String> expResult = this.company.getServiceTypeRegister().getServiceTypesNames();
        List<String> result = this.speServController.getServiceTypesNames();
        assertEquals(expResult, result);
    }

    /**
     * Test of newService method, of class SpecifyServiceController.
     */
    @Test
    public void testNewService() {
        System.out.println("newService");
        boolean expResult = this.company.getServiceRegister().registService(this.fixedService);
        boolean result = this.speServController.newService(this.serviceId, this.littleDescription, this.largeDescription, this.costHour, this.catId, this.type);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDurationForService method, of class SpecifyServiceController.
     */
    @Test
    public void testSetDurationForFixedService() {
        System.out.println("setDurationForService");
        int duration = 30;
        FixedService fixedService = (FixedService) this.company.getServiceRegister().getServiceById("1");
        System.out.println("lapr.project.gpsd.controller.SpecifyServiceControllerTest.testSetDurationForService()" +  fixedService);
        this.speServController.setDurationForFixedService(duration, "1");
        assertEquals(duration, fixedService.getDuration());
    }

    /**
     * Test of registService method, of class SpecifyServiceController.
     */
    @Test
    public void testRegistService() {
        System.out.println("registService");
        this.speServController.newService("2", this.littleDescription, this.largeDescription, this.costHour, this.catId, this.type);
        boolean expResult = true;
        boolean result = this.speServController.registService();
        assertEquals(expResult, result);
    }
    
    @Test
    public void instanciateWihoutRole() {
        System.out.println("instanciateWihoutRole");
        this.company.getAuthFacade().doLogout();
        Assertions.assertThrows(NullPointerException.class, ()->{
            this.speServController = new SpecifyServiceController();
        });
    }
    
    @Test
    public void instanciateWithWrongRole() {
        System.out.println("instanciateWihoutRole");
        this.company.getAuthFacade().doLogout();
        this.company.getAuthFacade().doLogin("frh1@esoft.pt", "123456");
        Assertions.assertThrows(IllegalStateException.class, ()->{
            this.speServController = new SpecifyServiceController();
        });
    }
}
