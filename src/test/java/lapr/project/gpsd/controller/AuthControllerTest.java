/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import lapr.project.auth.model.User;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class AuthControllerTest {
    
    private AuthController authController;
    private ApplicationGPSD app;
    private User user;
    
    public AuthControllerTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        
        this.authController = new AuthController();     
        user = new User("test", "test", "test");
        
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of doLogin method, of class AuthController.
     */
    @Test
    public void testDoLogin() {
        System.out.println("doLogin");
        String strId = "test";
        String strPwd = "test";
        boolean expResult = true;
        boolean result = this.authController.doLogin(strId, strPwd);
        assertEquals(expResult, result);
    }

    /**
     * Test of getUserRoles method, of class AuthController.
     */
    @Test
    public void testGetUserRoles() {
        System.out.println("getUserRoles");
        int expResult = 4;
        this.authController.doLogin("test", "test");
        int result = this.authController.getUserRoles().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of getApplicationGPSD method, of class AuthController.
     */
    @Test
    public void testGetApplicationGPSD() {
        System.out.println("getApplicationGPSD");
        ApplicationGPSD expResult = this.authController.getApplicationGPSD();
        ApplicationGPSD result = this.authController.getApplicationGPSD();
        assertEquals(expResult, result);
    }
    
}
