package lapr.project.gpsd.controller;

import java.util.ArrayList;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.GeographicalArea;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SpecifyGeographicalAreaControllerTest {

    private Company company;
    private SpecifyGeographicalAreaController specifyGeoAreaController;
    private final String geoAreaName = "GA1";
    private final float cost = 20.5f;
    private final float wrokRadius = 20f;
    private final String postalcode = "4410-320";
    private GeographicalArea ga;

    public SpecifyGeographicalAreaControllerTest() {

    }

    @BeforeAll
    public static void setUpClass() {

    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");
        this.ga = new GeographicalArea(
                this.geoAreaName, this.cost, this.wrokRadius, this.postalcode, company.getExternalService());
        this.specifyGeoAreaController = new SpecifyGeographicalAreaController();
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of newGeographicalArea method, of class
     * SpecifyGeographicalAreaController.
     */
    @Test
    public void testNewGeographicalArea() {
        System.out.println("newGeographicalArea");
        boolean result1 = this.company.getGeographicalAreaRegister().registerGeographicalArea(this.ga);
        boolean result2 = this.specifyGeoAreaController.newGeographicalArea(this.geoAreaName, this.cost, this.wrokRadius, this.postalcode);
        assertEquals(true, result1);
        assertEquals(false, result2);
    }

    /**
     * Test of getAllGeoAreas method, of class
     * SpecifyGeographicalAreaController.
     */
    @Test
    public void testGetAllGeoAreas() {
        System.out.println("getAllGeoAreas");
        ArrayList<GeographicalArea> expResult = this.company.getGeographicalAreaRegister().getListGeoAreas();
        ArrayList<GeographicalArea> result = this.specifyGeoAreaController.getAllGeoAreas();
        assertEquals(expResult, result);
    }

    /**
     * Test of registerGeoArea method, of class SpecifyGeographicalAreaController.
     */
    @Test
    public void testRegisterGeoArea() {
        System.out.println("registerGeoArea");
        boolean expResult = false;
        boolean result = this.specifyGeoAreaController.registerGeoArea(ga);
        
        assertEquals(expResult, result);
    }

}
