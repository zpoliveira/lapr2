/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.registers.CategoryRegister;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SubmitApplicationControllerTest {
    
    private SubmitApplicationController saController;
    private Application app;
    private CategoryRegister cr;
    private Company company;
    private Application app1;
    private Application app2 ;
    private String name;
    private String VATNumber;
    private String phoneNumber;
    private String email;
    private String address;
    private String postalCode;
    private String local;
    
    public SubmitApplicationControllerTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        
        this.company = new Company("AGPSD", "123456");
        this.name = "ZP";
        this.VATNumber = "123456789";
        this.phoneNumber = "911111111";
        this.email = "zzz@zzz.zz";
        this.address = "Rua QQ";
        this.postalCode = "4000-000";
        this.local = "Porto";
        this.company.getCategoryRegister().registerCategory(new Category("1", "cat1"));
        this.company.getCategoryRegister().registerCategory(new Category("2", "cat2"));
        this.company.getCategoryRegister().registerCategory(new Category("3", "cat3"));
        saController = new SubmitApplicationController();
        this.app1 = new Application(name, VATNumber, phoneNumber, email, new PostalAddress(address, postalCode, local));
        
        
           
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of newApplication method, of class SubmitApplicationController.
     */
    @Test
    public void testNewApplication() {
        System.out.println("newApplication");
        saController.newApplication(name, VATNumber, phoneNumber, email, address, postalCode, local);
        String expResult = "ZP";
        String result = saController.getApp().getSPName();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of addPostalAddress method, of class SubmitApplicationController.
     */
    @Test
    public void testAddPostalAddress() {
        System.out.println("addPostalAddress");
        String address2 = "Rua outra";
        String postalCode2 = "2000-000";
        String local2 = "OutroLocal";
        saController.newApplication(name, VATNumber, phoneNumber, email, address, postalCode, local);
        saController.addPostalAddress(address2, postalCode2, local2);
        int expResult = 2;
        assertEquals(expResult, saController.getApp().getPostalAddressList().size());
    }

    /**
     * Test of addAcademicQual method, of class SubmitApplicationController.
     */
    @Test
    public void testAddAcademicQual() {
        System.out.println("addAcademicQual");
        String designation = "MEI";
        String degree = "Master Degree";
        String classification = "19";
        saController.newApplication(name, VATNumber, phoneNumber, email, address, postalCode, local);
        saController.addAcademicQual(designation, degree, classification);
        String expResult = "MEI";
        String result = this.saController.getApp().getAcademicQualificationList().iterator().next().getDesignation();
        assertEquals(expResult, result);
    }

    /**
     * Test of addProfessionalQual method, of class SubmitApplicationController.
     */
    @Test
    public void testAddProfessionalQual() {
        System.out.println("addProfessionalQual");
        String description = "ProfQual";
        saController.newApplication(name, VATNumber, phoneNumber, email, address, postalCode, local);
        saController.addProfessionalQual(description);
        String expResult = "ProfQual";
        String result = saController.getApp().getProfessionalQualificationList().iterator().next().getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of addSupDocument method, of class SubmitApplicationController.
     */
    @Test
    public void testAddSupDocument() {
        System.out.println("addSupDocument");
        String doc = "Doc";
        String name = "ZP";
        saController.newApplication(name, VATNumber, phoneNumber, email, address, postalCode, local);
        saController.addSupDocument(doc);
        String expResult ="Doc";
        String result = this.saController.getApp().getDocumentList().iterator().next().getDocument();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCategories method, of class SubmitApplicationController.
     */
    @Test
    public void testGetCategories() {
        System.out.println("getCategories");
        List<Category> expResult = new ArrayList<Category>();
        expResult.add(new Category("1", "cat1"));
        List<Category> result = this.company.getCategoryRegister().getCategories();
        assertEquals(expResult.iterator().next().getCatId(), result.iterator().next().getCatId());
    }

    /**
     * Test of addCategory method, of class SubmitApplicationController.
     */
    @Test
    public void testAddCategory() {
        System.out.println("addCategory");
        this.saController.newApplication(name, VATNumber, phoneNumber, email, address, postalCode, local);
        String idCat = "2";
        this.saController.addCategory(idCat);
        int expResult = 1;
        int result = saController.getApp().getCategoryList().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of applicationValidator method, of class SubmitApplicationController.
     */
    @Test
    public void testApplicationValidatorTrue() {
        
        this.saController.registerApplication(this.app1);
        Application app2 = new Application("Outro", "987654321", "930000000", "a@aaa.aa", new PostalAddress("Avenida", "3000-000", "QqSitio"));
        Boolean expResult = true;
        Boolean result = company.getApplicationRegister().applicationValidator(app2);
        assertEquals(expResult, result);
    }
   
    /**
     * Test of registerApplication method, of class SubmitApplicationController.
     */
    @Test
    public void testRegisterApplication() {
        System.out.println("registerApplication");
        Application app2 = new Application("Outro", "987654321", "930000000", "a@aaa.aa", new PostalAddress("Avenida", "3000-000", "QqSitio"));
        this.company.getApplicationRegister().registerApplication(app2);
        int expResult = 1;
        int result = this.company.getApplicationRegister().getApplicationRegister().size();
        assertEquals(expResult, result);
    }
    
}
