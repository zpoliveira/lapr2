/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.time.LocalDateTime;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.Disponibility;
import lapr.project.gpsd.model.ServiceProvider;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class SpecifyDailyAvailabilityControllerTest {

    private SpecifyDailyAvailabilityController specifyDailyAvailabilityController;
    private Company company;
    private ServiceProvider testsp;

    public SpecifyDailyAvailabilityControllerTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {

        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");
        testsp = new ServiceProvider(1, "test", "test", "test", "test", "4000-007");
        this.company.getServiceProviderRegister().registServiceProvider(testsp);

        this.specifyDailyAvailabilityController = new SpecifyDailyAvailabilityController();

    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testGetServiceProvider() {
        System.out.println("getServiceProvider");
        ServiceProvider exp = new ServiceProvider(1, "test", "test", "test", "test", "4000-007");
        ServiceProvider spRes = this.specifyDailyAvailabilityController.getServiceProvider();
        assertEquals(exp, spRes);
    }

    @Test
    public void testAddDisponibility() {
        System.out.println("addDisponibility");
        boolean expTrue = true;
        boolean expFalse = false;

        Disponibility disp = new Disponibility(LocalDateTime.now(), LocalDateTime.now());
        boolean res = this.specifyDailyAvailabilityController.addDisponibility(testsp, disp.getDateTimeInit(), disp.getDateTimeEnd());
        testsp.addDisponibility(disp);
        boolean res2 = this.specifyDailyAvailabilityController.addDisponibility(testsp, disp.getDateTimeInit(), disp.getDateTimeEnd());
        assertEquals(expTrue, res);
        assertEquals(expFalse,res2);
    }
}
