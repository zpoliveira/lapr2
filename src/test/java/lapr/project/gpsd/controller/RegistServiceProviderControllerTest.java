/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ExternalService;
import lapr.project.gpsd.model.GeographicalArea;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.registers.ApplicationRegister;
import lapr.project.gpsd.model.registers.CategoryRegister;
import lapr.project.gpsd.model.registers.GeographicalAreaRegister;
import lapr.project.gpsd.model.registers.ServiceProviderRegister;
import lapr.project.gpsd.utils.Constants;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class RegistServiceProviderControllerTest {
    private Company company;
    private CategoryRegister catRegister;
    private GeographicalAreaRegister geoRegister;
    private String completName, shortName, email, vatNum, spPcode;
    private List<GeographicalArea> geoAreaList;
    private List<Category> catList;
    private ServiceProvider sp1;
    private int numMec;
    
    public RegistServiceProviderControllerTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");
        this.catRegister = this.company.getCategoryRegister();
        this.geoRegister = this.company.getGeographicalAreaRegister();
        this.catRegister.registerCategory(new Category("1", "cat1"));
        this.numMec = 20;
        this.completName = "sp1";
        this.shortName = "sp1";
        this.email = "sp1@sp1.com";
        this.vatNum = "123546789";
        this.spPcode = "3333";
        this.geoAreaList = new ArrayList<>();
        ExternalService externalService = new ExternalService();
        GeographicalArea geoA = new GeographicalArea("des", 20, 30, "3333", externalService);
        this.geoAreaList.add(geoA);
        this.catList =  new ArrayList<>();
        this.catList.add(this.catRegister.getCategorieById("1"));
        this.sp1 = new ServiceProvider(numMec, completName, shortName, email, vatNum, spPcode);
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getCompany method, of class RegistServiceProviderController.
     */
    @Test
    public void testGetCompany() {
        System.out.println("getCompany");
        RegistServiceProviderController instance = new RegistServiceProviderController();
        Company expResult = this.company;
        Company result = instance.getCompany();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCategoryRegister method, of class RegistServiceProviderController.
     */
    @Test
    public void testGetCategoryRegister() {
        System.out.println("getCategoryRegister");
        RegistServiceProviderController instance = new RegistServiceProviderController();
        CategoryRegister expResult = this.catRegister;
        CategoryRegister result = instance.getCategoryRegister();
        assertEquals(expResult, result);
    }

    /**
     * Test of getGeoAreaRegister method, of class RegistServiceProviderController.
     */
    @Test
    public void testGetGeoAreaRegister() {
        System.out.println("getGeoAreaRegister");
        RegistServiceProviderController instance = new RegistServiceProviderController();
        GeographicalAreaRegister expResult = this.geoRegister;
        GeographicalAreaRegister result = instance.getGeoAreaRegister();
        assertEquals(expResult, result);
    }

    /**
     * Test of getApplicationRegister method, of class RegistServiceProviderController.
     */
    @Test
    public void testGetApplicationRegister() {
        System.out.println("getApplicationRegister");
        RegistServiceProviderController instance = new RegistServiceProviderController();
        ApplicationRegister expResult = this.company.getApplicationRegister();
        ApplicationRegister result = instance.getApplicationRegister();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSpRegister method, of class RegistServiceProviderController.
     */
    @Test
    public void testGetSpRegister() {
        System.out.println("getSpRegister");
        RegistServiceProviderController instance = new RegistServiceProviderController();
        ServiceProviderRegister expResult = this.company.getServiceProviderRegister();
        ServiceProviderRegister result = instance.getSpRegister();
        assertEquals(expResult, result);
    }

    /**
     * Test of newServiceProvider method, of class RegistServiceProviderController.
     */
    @Test
    public void testNewServiceProvider() {
        System.out.println("newServiceProvider");

        RegistServiceProviderController instance = new RegistServiceProviderController();
        boolean expResult = this.company.getServiceProviderRegister().registServiceProvider(sp1);
        boolean result = instance.newServiceProvider(21, completName, shortName, "ee@rr.com", "987654321", "3333", geoAreaList, catList);
        boolean falseResult = instance.newServiceProvider(21, completName, shortName, email, vatNum,spPcode, geoAreaList, catList);
        assertEquals(expResult, result);
        Assertions.assertNotEquals(expResult, falseResult);
    }

    /**
     * Test of registServiceProvider method, of class RegistServiceProviderController.
     */
    @Test
    public void testRegistServiceProvider() {
        System.out.println("registServiceProvider");
        RegistServiceProviderController instance = new RegistServiceProviderController();
        boolean expResult = true;
        instance.newServiceProvider(30, completName, shortName, "outro", "456789123","3333", geoAreaList, catList);
        boolean result = instance.registServiceProvider();
        assertEquals(expResult, result);
    }

    /**
     * Test of createAuthForServicerovider method, of class RegistServiceProviderController.
     */
    @Test
    public void testCreateAuthForServicerovider() {
        System.out.println("createAuthForServicerovider");
        String name = "ola";
        String email = "ola@ola.com";
        String password = "1234";
        String role = Constants.ROLE_HR;
        RegistServiceProviderController instance = new RegistServiceProviderController();
        instance.createAuthForServicerovider(name, email, password, role);
        assertEquals(true, this.company.getAuthFacade().userExists(email));
        Assertions.assertFalse(this.company.getAuthFacade().userExists("bdbbd@mmgf.com"));
        Assertions.assertNotEquals(true, this.company.getAuthFacade().userExists("bdbbd@mmgf.com"));
    }

    /**
     * Test of getApplicationByVat method, of class RegistServiceProviderController.
     */
    @Test
    public void testGetApplicationByVat() {
        System.out.println("getApplicationByVat");
        RegistServiceProviderController instance = new RegistServiceProviderController();
        PostalAddress postalA = new PostalAddress("rua","3333" ,"place" );
        Application app1 = new Application("app1","123456789" , "911111111", "app1@app1", postalA);
        this.company.getApplicationRegister().registerApplication(app1);
        Application expResult = app1;
        Application result = instance.getApplicationByVat("123456789");
        assertEquals(expResult, result);
    }
    
}
