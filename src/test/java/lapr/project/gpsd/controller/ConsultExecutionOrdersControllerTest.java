/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ExecutionOrder;
import lapr.project.gpsd.model.LimitatedService;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceRequest;
import lapr.project.gpsd.model.ServiceRequestDescription;
import lapr.project.gpsd.model.ServiceType;
import lapr.project.gpsd.utils.Constants;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class ConsultExecutionOrdersControllerTest {
    
    private Company company;
   private  ConsultExecutionOrdersController instance;
    
    public ConsultExecutionOrdersControllerTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");
        ServiceProvider sp = this.company.getServiceProviderRegister().newServiceProvider(7, "sp3", "sp3", "test", "258147963", "4000-007");
        Client cli =this.company.getClientRegister().newClient("nome", "852741963", "921325467", "jj@jj.com");
        this.company.getServiceProviderRegister().registServiceProvider(sp);
        this.company.getClientRegister().addClient(cli);
        Category cat = new Category("2", "cattest");
        LimitatedService limitatedService = new LimitatedService("8", "ola", "ola2", 50, cat);
        PostalAddress pA = new PostalAddress("rua", "4000-008", "Porto");
        ServiceRequest sr = new ServiceRequest("jj@jj.com", pA);
        sr.addDescription(limitatedService, "a descr", 2);
        ServiceRequestDescription desc = sr.getServiceRequestDescriptionByService(limitatedService);
        desc.setServiceProvider(sp);
        this.company.getServiceRequestRegister().registerServiceRequest(sr);
        
        ExecutionOrder exO = new ExecutionOrder(4, sr.getID(), sp, limitatedService, 30, "jj@jj.com");
        Constants.SERVICE_TYPES.forEach((className, descriptor)->{
            ServiceType serType =  this.company.getServiceTypeRegister().newServiceType(className, descriptor);
            this.company.getServiceTypeRegister().registServiceType(serType);
        });
        
        this.company.getExecutionOrdersRegister().addExecutionOrder(exO);
        this.instance = new ConsultExecutionOrdersController();
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getCompany method, of class ConsultExecutionOrdersController.
     */
    @Test
    public void testGetCompany() {
        System.out.println("getCompany");
        Company expResult = this.company;
        Company result = this.instance.getCompany();
        assertEquals(expResult, result);
    }
    
}
