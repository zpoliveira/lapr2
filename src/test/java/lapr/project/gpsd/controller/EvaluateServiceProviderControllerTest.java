/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ServiceProvider;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class EvaluateServiceProviderControllerTest {

    private EvaluateServiceProviderController evaluateServiceProviderController;
    private Company company;
    private ServiceProvider testsp2;

    public EvaluateServiceProviderControllerTest() {

    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {

        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");
        testsp2 = new ServiceProvider(1, "test", "test", "test", "test", "4000-017");
        this.company.getServiceProviderRegister().registServiceProvider(testsp2);
        
        this.evaluateServiceProviderController = new EvaluateServiceProviderController();

    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testGetServiceProviderList() {
        System.out.println("getServiceProviderList");
        List<ServiceProvider> sps = this.company.getServiceProviderRegister().getServiceProviders();
        
        List<ServiceProvider> res = this.evaluateServiceProviderController.getServiceProviderList();
        
        assertEquals(sps, res);
        
    }
    
    @Test
    public void testGetServiceProvider(){
        System.out.println("getServiceProvider");
        
        ServiceProvider res = this.evaluateServiceProviderController.getServiceProvider("test");
        
        assertEquals(testsp2,res);
        
    }

}
