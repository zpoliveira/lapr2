/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ExecutionOrder;
import lapr.project.gpsd.model.FixedService;
import lapr.project.gpsd.model.PostalAddress;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceRequest;
import lapr.project.gpsd.model.registers.ExecutionOrdersRegister;
import lapr.project.gpsd.model.registers.ServiceRequestRegister;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class RateServicesControllerTest {
    
    private RateServicesController rateServicesController;
    private Company company;
    private ExecutionOrdersRegister execOrderregister;
    private ServiceRequestRegister serviceRequestRegister;
    private ServiceRequest serviceRequest;
    private ServiceProvider sp;
    private Service ser;
    private Category cat;
    private ExecutionOrder execOrder;
    
    public RateServicesControllerTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");
        this.serviceRequestRegister = new ServiceRequestRegister();
        this.rateServicesController = new RateServicesController();
        this.serviceRequestRegister = new ServiceRequestRegister();
        this.execOrderregister = this.company.getExecutionOrdersRegister();
        this.sp = new ServiceProvider(1, "ZePedro", "ZP","zp@zp.pt", "123456789", "3333");
        this.cat = new Category("1", "Food");
        this.ser = new FixedService("1", "Cook", "Restaurant", 30.0, cat);
        this.execOrder = new ExecutionOrder(1, 1, sp, ser, 2, "test");
        this.serviceRequest = new ServiceRequest("test", new PostalAddress("QQ", "4000-117", "QQ"));
        this.serviceRequestRegister.registerServiceRequest(serviceRequest);
        
        
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getClient method, of class RateServicesController.
     */
    @Test
    public void testGetClient() {
        System.out.println("getClient");
        String expResult = "test";
        String result = this.rateServicesController.getClient().getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getExecutionOrdersByClient method, of class RateServicesController.
     */
    @Test
    public void testGetExecutionOrdersByClient() {
        System.out.println("testGetExecutionOrdersByClient");
        Client client = this.rateServicesController.getClient();
        this.execOrderregister.addExecutionOrder(execOrder);
        int expResult = 0;
        int result = this.rateServicesController.getExecutionOrdersByClient(client).size();
        assertEquals(expResult, result);
    }

    /**
     * Test of rateService method, of class RateServicesController.
     */
    @Test
    public void testRateService() {
        System.out.println("rateService");
        int rating = 3;
        boolean expResult = true;
        boolean result = this.rateServicesController.rateService(execOrder, rating);
        assertEquals(expResult, result);
    }

    /**
     * Test of getServiceRequestByID method, of class RateServicesController.
     */
    @Test
    public void testGetServiceRequestByID() {
        System.out.println("getServiceRequestByID");
        int id = 1;
        String expResult = this.serviceRequest.getClientEmail();
        String result = this.rateServicesController.getServiceRequestByID(id).getClientEmail();
        Assertions.assertNotEquals(expResult, result);
    }

}
