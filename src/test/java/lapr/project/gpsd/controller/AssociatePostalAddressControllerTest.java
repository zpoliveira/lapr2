/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalAddress;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class AssociatePostalAddressControllerTest {
    
    private AssociatePostalAddressController associatePostalAddressController;
    private Company company;
    
    public AssociatePostalAddressControllerTest() {
        
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        this.company = ApplicationGPSD.getInstance().getCompany();
        this.company.getAuthFacade().doLogin("test", "test");
        this.associatePostalAddressController = new AssociatePostalAddressController();
        this.associatePostalAddressController.newAddress("Rua QQ", "4000-00", "Porto");
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getPostalAddress method, of class AssociatePostalAddressController.
     */
    @Test
    public void testGetPostalAddress() {
        System.out.println("getPostalAddress");
        PostalAddress expResult = new PostalAddress("Rua QQ", "4000-00", "Porto");
        PostalAddress result = this.associatePostalAddressController.getPostalAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of getClient method, of class AssociatePostalAddressController.
     */
    @Test
    public void testGetClient() {
        System.out.println("getClient");
        String expResult = "test";
        String result = this.associatePostalAddressController.getClient().getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of newAddress method, of class AssociatePostalAddressController.
     */
    @Test
    public void testNewAddress() {
        System.out.println("newAddress");
        String address = "Other";
        String postalCode = "1000-000";
        String local = "Other";
        PostalAddress expResult = new PostalAddress(address, postalCode, local);
        PostalAddress result = this.associatePostalAddressController.newAddress(address, postalCode, local);
        assertEquals(expResult, result);
    }

    /**
     * Test of addAddress method, of class AssociatePostalAddressController.
     */
    @Test
    public void testAddAddress() {
        System.out.println("addAddress");
        PostalAddress postalAddress = new PostalAddress("otherStreet", "5000-000", "Maia");
        Boolean expResult = true;
        Boolean result = this.associatePostalAddressController.addAddress(postalAddress);
        assertEquals(expResult, result);
    }
    
}
