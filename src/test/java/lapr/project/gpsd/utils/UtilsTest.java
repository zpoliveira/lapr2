/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import org.junit.Rule;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.rules.ExpectedException;

/**
 *
 * @author 1181846 Miguel Pereira
 * @author 1161335 Jose Oliveira
 * @author 1040321 José Pedro Barbosa
 * @author 1080510 Miguel Silva
 * @author 1161241 Diogo Nogueira
 */
public class UtilsTest {
    
    public UtilsTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of readPostalCodesFromFile method, of class Utils.
     */
    @Test
    public void testReadPostalCodesFromFile() {
        System.out.println("readPostalCodesFromFile");
        String filePath = "codigopostal_alt_long.csv";

        HashMap expResult = null;
        HashMap result = Utils.readPostalCodesFromFile(filePath);
        int expSize = 31587;
        int size = Utils.readPostalCodesFromFile(filePath).size();
        Assertions.assertNotEquals(expResult, result);
        assertEquals(expSize, size);
    }
    
    @Rule
    public final ExpectedException exception = ExpectedException.none();
    @Test
    public void testReadPostalCodesFromFileExcep() {
        System.out.println("readPostalCodesFromFileExep");
        String filePath = "codi";
        exception.expect(Exception.class);
        Utils.readPostalCodesFromFile(filePath);
    }


    /**
     * Test of getCalendar method, of class Utils.
     */
    @Test
    public void testGetCalendar_Date_Date() throws ParseException {
        System.out.println("getCalendar");
        String string1 = "1986-04-08 14:30";
        String string2 = "2000-09-10 12:30";
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

         Date date = format.parse(string1);
        Date time = format.parse(string2);
        //DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyy-MM-ddTHH:mm");
        String str = "1986-04-08 12:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        LocalDateTime expResult = LocalDateTime.parse(str, formatter);
        LocalDateTime result = Utils.getCalendar(date, time);
        assertEquals(expResult, result);
    }
    
}
