/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.auth.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author zpoliveira
 */
public class UserSessionTest {
    
    private UserSession userSession;
    private User user;
    
    public UserSessionTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        
        this.user = new User("ZP", "test", "test");
        this.userSession = new UserSession(user);
        this.user.addRole(new UserRole("1", "desc"));
        
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of doLogout method, of class UserSession.
     */
    @Test
    public void testDoLogout() {
        System.out.println("doLogout");
        this.userSession.doLogout();
        assertEquals(false, this.userSession.isLoggedIn());
    }

    /**
     * Test of isLoggedIn method, of class UserSession.
     */
    @Test
    public void testIsLoggedIn() {
        System.out.println("isLoggedIn");
        boolean expResult = true;
        boolean result = this.userSession.isLoggedIn();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isLoggedIn method, of class UserSession.
     */
    @Test
    public void testIsLoggedInfalse() {
        System.out.println("isLoggedIn");
        boolean expResult = false;
        this.userSession.doLogout();
        boolean result = this.userSession.isLoggedIn();
        assertEquals(expResult, result);
    }

    /**
     * Test of isLoggedInWithRole method, of class UserSession.
     */
    @Test
    public void testIsLoggedInWithRole() {
        System.out.println("isLoggedInWithRole");
        String role = "5";
        boolean expResult = false;
        boolean result = this.userSession.isLoggedInWithRole(role);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isLoggedInWithRole method, of class UserSession.
     */
    @Test
    public void testIsLoggedInWithRoleTrue() {
        System.out.println("isLoggedInWithRole");
        String role = "1";
        boolean expResult = true;
        boolean result = this.userSession.isLoggedInWithRole(role);
        assertEquals(expResult, result);
    }

    /**
     * Test of getUserName method, of class UserSession.
     */
    @Test
    public void testGetUserName() {
        System.out.println("getUserName");
        String expResult = "ZP";
        String result = this.userSession.getUserName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUserId method, of class UserSession.
     */
    @Test
    public void testGetUserId() {
        System.out.println("getUserId");
        String expResult = "test";
        String result = this.userSession.getUserId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUserEmail method, of class UserSession.
     */
    @Test
    public void testGetUserEmail() {
        System.out.println("getUserEmail");
        String expResult = "test";
        String result = this.userSession.getUserEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUserRoles method, of class UserSession.
     */
    @Test
    public void testGetUserRoles() {
        System.out.println("getUserRoles");
        int expResult = 1;
        int result = this.userSession.getUserRoles().size();
        assertEquals(expResult, result);
    }
    
}
