/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.auth.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author zpoliveira
 */
public class UserRoleRegisterTest {
    
    private UserRoleRegister userRoleRegister;
    private UserRole userRole;
    private UserRole userRole2;
    
    public UserRoleRegisterTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        
        userRoleRegister = new UserRoleRegister();
        userRole = new UserRole("1");
        userRole2 = new UserRole("2","desc");
        
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of newUserRole method, of class UserRoleRegister.
     */
    @Test
    public void testNewUserRole_String() {
        System.out.println("newUserRole");
        String role = "1";
        UserRole expResult = this.userRole;
        UserRole result = this.userRoleRegister.newUserRole(role);
        assertEquals(expResult, result);
    }

    /**
     * Test of newUserRole method, of class UserRoleRegister.
     */
    @Test
    public void testNewUserRole_String_String() {
        System.out.println("newUserRole");
        String role = "2";
        String description = "desc";
        UserRole expResult = this.userRole2;
        UserRole result = this.userRoleRegister.newUserRole(role, description);
        assertEquals(expResult, result);
    }

    /**
     * Test of addRole method, of class UserRoleRegister.
     */
    @Test
    public void testAddRole() {
        System.out.println("addRole");
        UserRole userRole = this.userRole;
        boolean expResult = true;
        boolean result = this.userRoleRegister.addRole(userRole);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of addRole method, of class UserRoleRegister.
     */
    @Test
    public void testAddRoleFalse() {
        System.out.println("addRole");
        UserRole userRole = null;
        boolean expResult = false;
        boolean result = this.userRoleRegister.addRole(userRole);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeRole method, of class UserRoleRegister.
     */
    @Test
    public void testRemoveRole() {
        System.out.println("removeRole");
        this.userRoleRegister.addRole(userRole);
        boolean expResult = true;
        boolean result = this.userRoleRegister.removeRole(userRole);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of removeRole method, of class UserRoleRegister.
     */
    @Test
    public void testRemoveRoleFalse() {
        System.out.println("removeRoleFalse");
        this.userRoleRegister.addRole(userRole);
        boolean expResult = false;
        boolean result = this.userRoleRegister.removeRole(userRole2);
        assertEquals(expResult, result);
    }

    /**
     * Test of findRole method, of class UserRoleRegister.
     */
    @Test
    public void testFindRole() {
        System.out.println("findRole");
        this.userRoleRegister.addRole(userRole);
        String role = "1";
        UserRole expResult = this.userRole;
        UserRole result = this.userRoleRegister.findRole(role);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasRole method, of class UserRoleRegister.
     */
    @Test
    public void testHasRole_String() {
        System.out.println("hasRole");
        this.userRoleRegister.addRole(userRole);
        String role = "1";
        boolean expResult = true;
        boolean result = this.userRoleRegister.hasRole(role);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hasRole method, of class UserRoleRegister.
     */
    @Test
    public void testHasRole_StringFalse() {
        System.out.println("hasRole");
        this.userRoleRegister.addRole(userRole);
        String role = "2";
        boolean expResult = false;
        boolean result = this.userRoleRegister.hasRole(role);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasRole method, of class UserRoleRegister.
     */
    @Test
    public void testHasRole_UserRole() {
        System.out.println("hasRole");
        this.userRoleRegister.addRole(userRole);
        boolean expResult = true;
        boolean result = this.userRoleRegister.hasRole(userRole);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hasRole method, of class UserRoleRegister.
     */
    @Test
    public void testHasRole_UserRoleFalse() {
        System.out.println("hasRole");
        this.userRoleRegister.addRole(userRole);
        boolean expResult = false;
        boolean result = this.userRoleRegister.hasRole(userRole2);
        assertEquals(expResult, result);
    }
    
}
