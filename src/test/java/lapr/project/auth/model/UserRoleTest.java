/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.auth.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author zpoliveira
 */
public class UserRoleTest {
    
    private UserRole userRole;
    
    public UserRoleTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        
        userRole = new UserRole("1", "desc");
        
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getRole method, of class UserRole.
     */
    @Test
    public void testGetRole() {
        System.out.println("getRole");
        String expResult = "1";
        String result = this.userRole.getRole();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDescription method, of class UserRole.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        String expResult = "desc";
        String result = this.userRole.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of hasId method, of class UserRole.
     */
    @Test
    public void testHasId() {
        System.out.println("hasId");
        String strId = "1";
        boolean expResult = true;
        boolean result = this.userRole.hasId(strId);
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class UserRole.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = 210;
        int result = this.userRole.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class UserRole.
     */
    @Test
    public void testEqualsNull() {
        System.out.println("equals");
        Object o = null;
        boolean expResult = false;
        boolean result = this.userRole.equals(o);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class UserRole.
     */
    @Test
    public void testEqualsTrue() {
        System.out.println("equals");
        boolean expResult = true;
        boolean result = this.userRole.equals(this.userRole);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class UserRole.
     */
    @Test
    public void testEqualsClass() {
        System.out.println("equals");
        boolean expResult = false;
        boolean result = this.userRole.equals(new User("ZP", "test", "test"));
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class UserRole.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "1 - desc";
        String result = this.userRole.toString();
        assertEquals(expResult, result);
    }
    
}
