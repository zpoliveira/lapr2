/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.auth.model;

import java.util.HashSet;
import java.util.Set;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author zpoliveira
 */
public class UserTest {
    
    private User user;
    
    public UserTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        this.user = new User("ZP", "test", "test");
        this.user.addRole(new UserRole("QQ", "desc"));
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getId method, of class User.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        String expResult = "test";
        String result = this.user.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class User.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String expResult = "ZP";
        String result = this.user.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmail method, of class User.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        String expResult = "test";
        String result = this.user.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class User.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Other";
        this.user.setName(name);
        String result = this.user.getName();
        
        assertEquals(name, result);
    }

    /**
     * Test of setEmail method, of class User.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "email";
        this.user.setEmail(email);
        String result = this.user.getEmail();
        
        assertEquals(email, result);
    }

    /**
     * Test of setPassword method, of class User.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String password = "other";
        this.user.setPassword(password);
        boolean result = this.user.hasPassword(password);
        
        Assert.assertTrue(result);
    }

    /**
     * Test of setUserRoles method, of class User.
     */
    @Test
    public void testSetUserRoles() {
        System.out.println("setUserRoles");
        Set<UserRole> userRoles = new HashSet<UserRole>();
        userRoles.add(new UserRole("1"));
        userRoles.add(new UserRole("2"));
        
        this.user.setUserRoles(userRoles);
        int expresult = 2;
        int result = this.user.getRoles().size();
        
        assertEquals(expresult, result);
        
    }

    /**
     * Test of hasId method, of class User.
     */
    @Test
    public void testHasIdtrue() {
        System.out.println("hasId");
        String strId = "test";
        boolean expResult = true;
        boolean result = this.user.hasId(strId);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hasId method, of class User.
     */
    @Test
    public void testHasIdFalse() {
        System.out.println("hasId");
        String strId = "2";
        boolean expResult = false;
        boolean result = this.user.hasId(strId);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasPassword method, of class User.
     */
    @Test
    public void testHasPasswordTrue() {
        System.out.println("hasPassword");
        String strPwd = "test";
        boolean expResult = true;
        boolean result = this.user.hasPassword(strPwd);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hasPassword method, of class User.
     */
    @Test
    public void testHasPasswordFalse() {
        System.out.println("hasPassword");
        String strPwd = "not";
        boolean expResult = false;
        boolean result = this.user.hasPassword(strPwd);
        assertEquals(expResult, result);
    }

    /**
     * Test of addRole method, of class User.
     */
    @Test
    public void testAddRole() {
        System.out.println("addRole");
        UserRole role = new UserRole("3");
        boolean expResult = true;
        boolean result = this.user.addRole(role);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeRole method, of class User.
     */
    @Test
    public void testRemoveRoleTrue() {
        System.out.println("removeRole");
        UserRole role = new UserRole("QQ", "desc");
        boolean expResult = true;
        boolean result = this.user.removeRole(role);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of removeRole method, of class User.
     */
    @Test
    public void testRemoveRoleFalse() {
        System.out.println("removeRole");
        UserRole role = new UserRole("4", "4");
        boolean expResult = false;
        boolean result = this.user.removeRole(role);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasRole method, of class User.
     */
    @Test
    public void testHasRole_UserRoleTrue() {
        System.out.println("hasRole");
        UserRole role = new UserRole("QQ", "desc");
        boolean expResult = true;
        boolean result = this.user.hasRole(role);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hasRole method, of class User.
     */
    @Test
    public void testHasRole_UserRoleFalse() {
        System.out.println("hasRole");
        UserRole role = new UserRole("6", "9");
        boolean expResult = false;
        boolean result = this.user.hasRole(role);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasRole method, of class User.
     */
    @Test
    public void testHasRole_StringTrue() {
        System.out.println("hasRole");
        String strRole = "QQ";
        boolean expResult = true;
        boolean result = this.user.hasRole(strRole);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hasRole method, of class User.
     */
    @Test
    public void testHasRole_StringFalse() {
        System.out.println("hasRole");
        String strRole = "x";
        boolean expResult = false;
        boolean result = this.user.hasRole(strRole);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRoles method, of class User.
     */
    @Test
    public void testGetRoles() {
        System.out.println("getRoles");
        int expResult = 1;
        int result = this.user.getRoles().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class User.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = 3556659;
        int result = this.user.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class User.
     */
    @Test
    public void testEqualsNull() {
        System.out.println("equals");
        Object o = null;
        boolean expResult = false;
        boolean result = this.user.equals(o);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class User.
     */
    @Test
    public void testEqualstrue() {
        System.out.println("equals");
        boolean expResult = true;
        boolean result = this.user.equals(this.user);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class User.
     */
    @Test
    public void testEqualsClass() {
        System.out.println("equals");
        boolean expResult = false;
        boolean result = this.user.equals(new UserRole("2"));
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class User.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "ZP - test";
        String result = this.user.toString();
        assertEquals(expResult, result);
    }
    
}
